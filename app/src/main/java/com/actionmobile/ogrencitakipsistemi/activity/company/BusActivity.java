package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BusActivity extends AppCompatActivity {
    private static final String TAG = "BusActivity";

    private List<Bus> busList = new ArrayList<>();
    Bus bus;
    TextView tvOkul;
    TextView tvPlaka;
    TextView tvKapasite;
    TextView tvSofor;
    TextView tvRehberAdi;
    TextView tvRehberMail;
    TextView tvRehberTel;
    TextView tvTitle;
    private FirebaseUser userCompany;
    ImageView backBt;
    CircleImageView rehberResim;
    ImageView deleteBt;
    Button routesBt;
    Context context;
    TextView asd;
    String school_id;
    ImageView editBus;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_bus);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        asd = findViewById(R.id.asd);
        editBus = findViewById(R.id.img_edit_bus);
        rehberResim = findViewById(R.id.img_guide);
        routesBt = findViewById(R.id.btn_routes);
        backBt = findViewById(R.id.img_back_bus);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();
        deleteBt = findViewById(R.id.img_delete_bus);
        tvOkul = findViewById(R.id.tv_okul);
        tvPlaka = findViewById(R.id.tv_plaka);
        tvKapasite = findViewById(R.id.tv_kapasite);
        tvTitle = findViewById(R.id.tv_title);
        tvSofor = findViewById(R.id.tv_sofor);
        tvRehberAdi = findViewById(R.id.tv_rehberAdi);
        tvRehberMail = findViewById(R.id.tv_rehberMail);
        tvRehberTel = findViewById(R.id.tv_rehberTel);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bus = (Bus) bundle.getSerializable(Constance.KEY_ACTIVITY);
            school_id = (String) bundle.getSerializable("school");
        }
        if (school_id != null)
            editBus.setVisibility(View.GONE);
        else {
            routesBt.setVisibility(View.GONE);
            asd.setVisibility(View.GONE);
        }

        if (bus.id != null) {
            if (bus.id.equals(bus.rehber.yetkiKodu)) {
                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("busList")
                        .document(bus.rehber.yetkiKodu)
                        .addSnapshotListener((snapshot, e) -> {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }
                            bus = snapshot.toObject(Bus.class);
                            setViews();
                        });
            } else {
                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("busList")
                        .document(bus.id)
                        .addSnapshotListener((snapshot, e) -> {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }
                            bus = snapshot.toObject(Bus.class);
                            setViews();
                        });
            }
        }


        editBus.setOnClickListener(v -> editBus());
        setViews();

        backBt.setOnClickListener(v -> onBackPressed());
        deleteBt.setOnClickListener(view -> {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Servis Sil");
            alert.setMessage("Silmek istediğinize emin misiniz?");
            alert.setPositiveButton("Evet", (dialog, which) -> {
                // continue with delete
                if (school_id != null) {
                    if (bus.rehber.id != null) {
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("schoolList")
                                .document(school_id)
                                .collection("busList")
                                .document(bus.rehber.id)
                                .delete();
                        bus.okulId = null;
                        bus.okulAdi = "(Henüz Okul Atanmadı)";
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("busList")
                                .document(bus.rehber.id)
                                .set(bus, SetOptions.merge());
                    } else {
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("schoolList")
                                .document(school_id)
                                .collection("busList")
                                .document(bus.rehber.yetkiKodu)
                                .delete();

                        bus.okulId = null;
                        bus.okulAdi = "(Henüz Okul Atanmadı)";
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("busList")
                                .document(bus.rehber.yetkiKodu)
                                .set(bus, SetOptions.merge());
                    }
                } else {
                    if (bus.rehber.id != null) {
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("busList")
                                .document(bus.rehber.id)
                                .delete()
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("schoolList")
                                .document(bus.okulId)
                                .collection("busList")
                                .document(bus.rehber.id)
                                .delete()
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                        db.collection("users")
                                .document("guide")
                                .collection("guideList")
                                .document(bus.rehber.id)
                                .delete()
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                    } else {
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("busList")
                                .document(bus.rehber.yetkiKodu)
                                .delete()
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                        db.collection("users")
                                .document("guide")
                                .collection("guideList")
                                .document(bus.rehber.yetkiKodu)
                                .delete()
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                        if (bus.okulId != null) {
                            db.collection("users")
                                    .document("company")
                                    .collection("companyList")
                                    .document(userCompany.getUid())
                                    .collection("schoolList")
                                    .document(bus.okulId)
                                    .collection("busList")
                                    .document(bus.rehber.yetkiKodu)
                                    .delete()
                                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                    .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                        }
                    }


                }

                dialog.dismiss();
                onBackPressed();
            });
            alert.setNegativeButton("Hayır", (dialog, which) -> {
                // close dialog
                dialog.dismiss();
            });
            alert.show();

        });
        routesBt.setOnClickListener(v -> {
            if (bus.rehber.id != null) {
                Intent intent = new Intent(context, RoutesActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY, bus);
                intent.putExtra("school", school_id);
                context.startActivity(intent);
            } else
                Toast.makeText(getApplicationContext(), "Rehber kaydı tamamlanmamış.", Toast.LENGTH_LONG).show();
        });

    }

    private void setViews() {
        tvOkul.setText(bus.okulAdi);
        tvPlaka.setText(bus.plaka);
        tvKapasite.setText(bus.kapasite);
        tvSofor.setText(bus.soforAdi);
        tvRehberAdi.setText(bus.rehber.ad);
        tvRehberMail.setText(bus.rehber.email);
        tvRehberTel.setText(bus.rehber.telefon);
        tvTitle.setText(bus.plaka);

        String imageUrl = bus.rehber.imageUrl;
        if (imageUrl != null) {
            Glide.with(BusActivity.this)
                    .load(imageUrl)
                    .into(rehberResim);
        } else {
            rehberResim.setImageResource(R.drawable.ic_child);
        }
    }

    private void editBus() {
        Intent intent = new Intent(context, AddOrEditBusActivity.class);
        intent.putExtra("bus", bus);
        intent.putExtra("tip", "BusActivityEdit");
        context.startActivity(intent);
    }


}
