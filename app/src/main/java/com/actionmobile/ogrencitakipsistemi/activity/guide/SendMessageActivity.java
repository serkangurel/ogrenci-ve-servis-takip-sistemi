package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.BaseActivity;
import com.actionmobile.ogrencitakipsistemi.fragments.guide.ParentsFragment;
import com.actionmobile.ogrencitakipsistemi.otto.BusProvider;
import com.actionmobile.ogrencitakipsistemi.otto.CbItemsSelected;

public class SendMessageActivity extends BaseActivity {

    private ImageView imgBack;
    private CheckBox cbStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        imgBack = findViewById(R.id.img_back);
        cbStatus = findViewById(R.id.cb_status);

        imgBack.setOnClickListener(view -> onBackPressed());
        cbStatus.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if(isChecked) {
                BusProvider.getInstance().post(new CbItemsSelected(true));
            }else {
                BusProvider.getInstance().post(new CbItemsSelected(false));
            }
        });

        ParentsFragment parentsFragment = ParentsFragment.newInstance(true, "");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, parentsFragment);
        fragmentTransaction.commit();
    }

}