package com.actionmobile.ogrencitakipsistemi.activity.admin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.model.admin.TempUser;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.github.javafaker.Faker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import static com.actionmobile.ogrencitakipsistemi.model.admin.Company.Yetkili;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.COMPANY;

public class AddOrEditCompanyActivity extends AppCompatActivity {

    private static final String TAG = "AddOrEditCompany";

    private FirebaseFirestore db;

    private Company company;
    private TextView tvAddOrEditCompany;
    private ImageView imgBack;

    private EditText etFirmaAdi;
    private EditText etYetkiliEmail;
    private EditText etAdres;
    private EditText etYetkiliAd;
    private EditText etYetkiliTelefon;
    private TempUser tempUser = new TempUser();
    private FirebaseUser adminUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_company);

        etFirmaAdi = findViewById(R.id.input_firmaAdi);
        etYetkiliAd = findViewById(R.id.input_yetkiliAdi);
        etYetkiliEmail = findViewById(R.id.input_email);
        etYetkiliTelefon = findViewById(R.id.input_telefon);
        etAdres = findViewById(R.id.input_adres);
        tvAddOrEditCompany = findViewById(R.id.tv_add_or_edit_company);
        imgBack = findViewById(R.id.img_back);
        Button btnSave = findViewById(R.id.btn_save);

        db = FirebaseFirestore.getInstance();
        imgBack.setOnClickListener(view -> onBackPressed());
        adminUser = FirebaseAuth.getInstance().getCurrentUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            company = (Company) bundle.getSerializable(Constance.KEY_ACTIVITY);
            tvAddOrEditCompany.setText("Firma Düzenle");

            etFirmaAdi.setText(company.firmaIsmi);
            etYetkiliAd.setText(company.yetkili.ad);
            etYetkiliEmail.setText(company.yetkili.email);
            etYetkiliTelefon.setText(company.yetkili.telefon);
            etAdres.setText(company.adres);
        } else {
            tvAddOrEditCompany.setText("Firma Ekle");

            Faker faker = new Faker(new Locale("tr", "TR"));
            etFirmaAdi.setText(faker.job().keySkills());
            etYetkiliAd.setText(faker.name().fullName());
            etYetkiliEmail.setText(getSaltString() + "@gmail.com");
            etAdres.setText(faker.address().fullAddress());

        }

        btnSave.setOnClickListener(view -> onSaveClick());
    }

    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    private void onSaveClick() {

        String firmaAdi = etFirmaAdi.getText().toString();
        String yetkiliAdi = etYetkiliAd.getText().toString();
        String yetkiliEmail = etYetkiliEmail.getText().toString();
        String yetkiliTelefon = etYetkiliTelefon.getText().toString();
        String adres = etAdres.getText().toString();

        if (!validate(firmaAdi, yetkiliAdi, yetkiliEmail, yetkiliTelefon, adres)) {
            onSaveFailed();
            return;
        }

        addTempUser(yetkiliEmail);

        if (company == null) {
            Company company = new Company();
            company.yetkili = new Yetkili();
            company.id = UUID.randomUUID().toString();
            company.firmaIsmi = firmaAdi;
            company.adres = adres;
            company.yetkili.yetkiKodu = tempUser.authorityCode;
            company.yetkili.id = null;
            company.yetkili.ad = yetkiliAdi;
            company.yetkili.email = yetkiliEmail;
            company.yetkili.telefon = yetkiliTelefon;

            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(company.yetkili.yetkiKodu)
                    .set(company)
                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                    .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
        } else {
            company.yetkili = new Yetkili();
            company.id = UUID.randomUUID().toString();
            company.firmaIsmi = firmaAdi;
            company.adres = adres;
            company.yetkili.yetkiKodu = tempUser.authorityCode;
            company.yetkili.id = null;
            company.yetkili.ad = yetkiliAdi;
            company.yetkili.email = yetkiliEmail;
            company.yetkili.telefon = yetkiliTelefon;

            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(company.yetkili.yetkiKodu)
                    .set(company, SetOptions.merge())
                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                    .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
        }

        sendEmail(tempUser);
    }

    private void addTempUser(String yetkiliEmail) {
        tempUser.authorityCode = UUID.randomUUID().toString().substring(0, 8);
        tempUser.email = yetkiliEmail;
        tempUser.role = COMPANY.toString();
        tempUser.adderId = adminUser.getUid();

        //Add a new document with a authorityCode
        db.collection("tempUser")
                .document(tempUser.authorityCode)
                .set(tempUser)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

    }

    private void sendEmail(TempUser tempUser) {

        String email = tempUser.email;
        String subject = "Öğrenci ve Servis Takip Sistemi";
        String message = "Yetki Kodu : " + tempUser.authorityCode;


        String yourgmail = "ogrenciveservistakipsistemi@gmail.com";
        String yourPassword = "ogrencitakibi469";

        BackgroundMail.newBuilder(this)
                .withUsername(yourgmail)
                .withPassword(yourPassword)
                .withMailto(email)
                .withType(BackgroundMail.TYPE_PLAIN)
                .withSubject(subject)
                .withBody(message)
                .withSendingMessage("Mail Gönderiliyor")
                .withSendingMessageError("Mail Gönderilemedi")
                .withSendingMessageSuccess("Mail Gönderildi")
                .withOnSuccessCallback(this::onBackPressed)
                .withOnFailCallback(this::onBackPressed)
                .send();

    }

    private boolean validate(String firmaAdi, String yetkiliAdi, String yetkiliEmail, String yetkiliTelefon, String adres) {
        boolean valid = true;

        if (firmaAdi.isEmpty() || firmaAdi.length() < 3) {
            etFirmaAdi.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etFirmaAdi.setError(null);
        }
        if (yetkiliAdi.isEmpty() || yetkiliAdi.length() < 3) {
            etYetkiliAd.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etYetkiliAd.setError(null);
        }


        if (yetkiliEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(yetkiliEmail).matches()) {
            etYetkiliEmail.setError("Geçersiz mail adresi");
            valid = false;
        } else {
            etYetkiliEmail.setError(null);
        }

        if (yetkiliTelefon.isEmpty() || !isPhoneValid(yetkiliTelefon)) {
            etYetkiliTelefon.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            etYetkiliTelefon.setError(null);
        }

        if (adres.isEmpty() || adres.length() < 3) {
            etAdres.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etAdres.setError(null);
        }

        return valid;
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız", Toast.LENGTH_SHORT).show();
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }

}
