package com.actionmobile.ogrencitakipsistemi.activity.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.model.admin.TempUser;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.COMPANY;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.PARENT;

public class SignupActivity extends AppCompatActivity {

    private TextView linkLogin;
    private static final String TAG = "SignupActivity";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private ArrayList<TempUser> tempUserList = new ArrayList<>();
    private Company company;
    EditText nameText;
    EditText emailText;
    EditText phoneText;
    EditText passwordText;
    EditText reEnterPasswordText;
    EditText authorityCodeText;
    Button signupButton;
    TextView loginLink;
    ProgressDialog progressDialog;
    String authorityCode, name, email, phone, password, reEnterPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        signupButton = findViewById(R.id.btn_signup);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        authorityCodeText = findViewById(R.id.input_authorityCode);
        nameText = findViewById(R.id.input_name);
        emailText = findViewById(R.id.input_email);
        phoneText = findViewById(R.id.input_phone);
        passwordText = findViewById(R.id.input_password);
        reEnterPasswordText = findViewById(R.id.input_reEnterPassword);


        signupButton.setOnClickListener(view -> {
            authorityCode = authorityCodeText.getText().toString();
            name = nameText.getText().toString();
            email = emailText.getText().toString();
            phone = phoneText.getText().toString();
            password = passwordText.getText().toString();
            reEnterPassword = reEnterPasswordText.getText().toString();

            if (authorityCode.isEmpty()) {
                onSignupFailed();
                return;
            }

            db.collection("tempUser")
                    .document(authorityCode)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {//Başarılı bir şekide alındıysa
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                TempUser tempUser = document.toObject(TempUser.class);

                                if (!validate(tempUser)) {
                                    onSignupFailed();
                                    return;
                                }
                                signuptoFirebase(tempUser);

                            } else {
                                onSignupFailed();
                            }
                        }
                    });

        });

        linkLogin = findViewById(R.id.link_login);
        linkLogin.setOnClickListener(view -> startLoginActivity());
    }

    private void signuptoFirebase(TempUser tempUser) {
        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(tempUser.role)
                                .build();
                        user.updateProfile(profileUpdates);
                        onSignupSuccess(tempUser);
                        startLoginActivity();

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.e(TAG, "createUserWithEmail:failure", task.getException());
                        onSignupFailed();
                    }
                    //progressDialog.hide();
                });
    }

    private void onSignupSuccess(TempUser tempUser) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (tempUser.role.equals(COMPANY.toString())) {
            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(tempUser.authorityCode)
                    .get()
                    .addOnCompleteListener(task -> {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            Company company = document.toObject(Company.class);
                            company.yetkili.id = currentUser.getUid();
                            company.yetkili.ad = name;
                            company.yetkili.telefon = phone;
                            updateCompany(tempUser, company);
                        }
                    });
        } else if(tempUser.role.equals(GUIDE.toString())) {

           db.collection("users")
                    .document("guide")
                    .collection("guideList")
                    .document(tempUser.authorityCode)
                    .get()
                    .addOnCompleteListener(task -> {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            Guide guide = document.toObject(Guide.class);
                            guide.id = currentUser.getUid();
                            guide.ad = name;
                            guide.telefon = phone;
                            updateGuide(tempUser, guide);
                        }
                    });

            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(tempUser.adderId)
                    .collection("busList")
                    .document(tempUser.authorityCode)
                    .get()
                    .addOnCompleteListener(task -> {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            Bus bus = document.toObject(Bus.class);
                            bus.rehber = new Guide();
                            bus.rehber.id = currentUser.getUid();
                            bus.rehber.ad = name;
                            bus.rehber.telefon = phone;
                            bus.rehber.email = email;
                            updateBusList(tempUser, bus);
                        }
                    });
        }

        else if(tempUser.role.equals(PARENT.toString())) {

            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(tempUser.authorityCode)
                    .get()
                    .addOnCompleteListener(task -> {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            Parents parents = document.toObject(Parents.class);
                            parents.id = currentUser.getUid();
                            parents.name = name;
                            parents.phoneNumber = phone;
                            parents.email = email;
                            updateParentList(tempUser, parents);
                        }
                    });
        }
    }

    private void updateParentList(TempUser tempUser, Parents parents) {
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parents.id)
                .set(parents)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(tempUser.authorityCode)
                .delete()
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        deleteTempUser(tempUser);

    }


    private void updateBusList(TempUser tempUser, Bus bus) {


        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(tempUser.adderId)
                .collection("busList")
                .document(bus.rehber.id)
                .set(bus)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(tempUser.adderId)
                .collection("busList")
                .document(tempUser.authorityCode)
                .delete()
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        deleteTempUser(tempUser);

    }

    private void updateGuide(TempUser tempUser, Guide guide) {

        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(guide.id)
                .set(guide)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(tempUser.authorityCode)
                .delete()
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        deleteTempUser(tempUser);

    }

    private void updateCompany(TempUser tempUser, Company company) {
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(company.yetkili.id)
                .set(company)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(tempUser.authorityCode)
                .delete()
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

        deleteTempUser(tempUser);
    }

    private void deleteTempUser(TempUser tempUser) {
        db.collection("tempUser")
                .document(tempUser.authorityCode)
                .delete()
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private void onSignupFailed() {
        Toast.makeText(this, "Kayıt Başarısız", Toast.LENGTH_SHORT).show();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public boolean validate(TempUser tempUser) {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 3) {
            nameText.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            nameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() || !tempUser.email.equals(email)) {
            emailText.setError("Geçersiz mail adresi");
            valid = false;
        } else {
            emailText.setError(null);
        }
        if (phone.isEmpty() || !isPhoneValid(phone)) {
            phoneText.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            phoneText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            passwordText.setError("6 ile 10 arası sayıda karakter giriniz");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            reEnterPasswordText.setError("Şifre eşleşmiyor");
            valid = false;
        } else {
            reEnterPasswordText.setError(null);
        }

        return valid;
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }

}

