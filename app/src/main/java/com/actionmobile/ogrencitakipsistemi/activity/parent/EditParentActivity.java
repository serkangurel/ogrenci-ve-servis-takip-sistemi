package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class EditParentActivity extends AppCompatActivity {
    private static final String TAG = "EditParentActivity";
    private EditText etMail, etName, etPhone;
    private ImageView imgBack;
    private TextView tvAddImage;
    private FirebaseFirestore db;
    private Parents parent;
    private CircleImageView imgProfile;
    private Bitmap compressedBitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_parent);

        imgProfile = findViewById(R.id.img_profile);
        etName = findViewById(R.id.input_name);
        etMail = findViewById(R.id.input_mail);
        etPhone = findViewById(R.id.input_phone);
        imgBack = findViewById(R.id.img_back);
        tvAddImage = findViewById(R.id.tv_add_image);
        db = FirebaseFirestore.getInstance();
        Button btnSave = findViewById(R.id.btn_save);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parent = (Parents) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }

        String imageUrl = parent.imageUrl;
        if (imageUrl != null) {
            Glide.with(EditParentActivity.this)
                    .load(imageUrl)
                    .into(imgProfile);
        } else {
            imgProfile.setImageResource(R.drawable.ic_child);
        }

        etName.setText(parent.name);
        etPhone.setText(parent.phoneNumber);
        etMail.setText(parent.email);
        etMail.setEnabled(false);

        tvAddImage.setOnClickListener(view -> onAddImageClick());
        imgBack.setOnClickListener(view -> onBackPressed());
        imgProfile.setOnClickListener(view -> onAddImageClick());
        btnSave.setOnClickListener(view -> onSaveClick());
    }

    private void onAddImageClick() {
        new ImagePicker.Builder(EditParentActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void onSaveClick() {
        String name = etName.getText().toString();
        String email = etMail.getText().toString();
        String phone = etPhone.getText().toString();

        if (!validate(name, email, phone)) {
            onSaveFailed();
            return;
        }
        parent.name = name;
        parent.phoneNumber = phone;

        if (compressedBitmap != null) {
            uploadFile(compressedBitmap);
        }
        updateParentDB();
        onBackPressed();
    }

    private void updateParentDB() {
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.id)
                .set(parent, SetOptions.merge())
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
    }

    private void uploadFile(Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://ogrencitakipsistemi-25fa3.appspot.com/");
        StorageReference mountainImagesRef = storageRef.child("parents/" + parent.id + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        // Handle unsuccessful uploads
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            mountainImagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                db.collection("users")
                        .document("parent")
                        .collection("parentList")
                        .document(parent.id)
                        .update("imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
            }).addOnFailureListener(Throwable::printStackTrace);
        }).addOnFailureListener(Throwable::printStackTrace);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT", "OK");
            if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE) {
                String picturePath = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH).get(0);
                File imageFile = new File(picturePath);
                try {
                    compressedBitmap = new Compressor(EditParentActivity.this)
                            .setMaxHeight(150)
                            .setMaxWidth(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(this)
                            .load(compressedBitmap)
                            .into(imgProfile);
                }
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT", "CANCELLED");
        }
    }

    private boolean validate(String name, String email, String phone) {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 3) {
            etName.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (phone.isEmpty() || !isPhoneValid(phone)) {
            etPhone.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            etPhone.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etMail.setError("Geçersiz Mail Adresi!");
            valid = false;
        } else {
            etMail.setError(null);
        }

        return valid;
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_SHORT).show();
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }


}
