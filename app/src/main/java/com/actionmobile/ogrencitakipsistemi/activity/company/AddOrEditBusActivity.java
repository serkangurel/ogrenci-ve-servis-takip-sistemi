package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.admin.TempUser;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.School;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.github.javafaker.Faker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

import static com.actionmobile.ogrencitakipsistemi.model.company.School.OkulBean;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;

public class AddOrEditBusActivity extends AppCompatActivity {
    private static final String TAG = "AddOrEditBusActivity";
    private List<OkulBean> schoolList = new ArrayList<>();
    private List<Bus> busList = new ArrayList<>();
    private List<Bus> busList2 = new ArrayList<>();
    private String url;
    private Spinner spinnerServisler;
    private EditText etPlaka;
    private Bitmap compressedBitmap = null;
    ArrayAdapter<String> adapterKapasite;
    ArrayAdapter<String> adapterServisler;
    private EditText etSoforAdi;
    private EditText etRehberAdi;
    private EditText etrehberMail;
    private EditText etrehberTel;
    private Spinner spinnerKapasite;
    private Guide rehber;
    private ImageView imgBack;
    private TextView tvTitle;
    private Button btnSave;
    private CircleImageView imgRehber;
    private TextView addImage;
    private String str;
    private School.OkulBean okul;
    private Bus bus;
    private FirebaseUser userCompany;
    private TempUser tempUser = new TempUser();
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_bus);
        etRehberAdi = findViewById(R.id.et_rehberAdi);
        etrehberTel = findViewById(R.id.et_rehberTel);
        spinnerServisler = findViewById(R.id.spinner_servisler);
        etPlaka = findViewById(R.id.et_plaka);
        imgRehber = findViewById(R.id.img_guide);
        addImage = findViewById(R.id.tv_add_image);
        imgRehber.setOnClickListener(v -> addImage());
        etSoforAdi = findViewById(R.id.et_soforAdi);
        etrehberMail = findViewById(R.id.et_rehberMail);
        spinnerKapasite = findViewById(R.id.spinner_kapasite);
        imgBack = findViewById(R.id.img_back);
        tvTitle = findViewById(R.id.tv_add_or_edit_bus);
        btnSave = findViewById(R.id.btn_save);

        imgBack.setOnClickListener(view -> onBackPressed());
        btnSave.setOnClickListener(view -> onSaveClick());

        db = FirebaseFirestore.getInstance();
        userCompany = FirebaseAuth.getInstance().getCurrentUser();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            str = (String) bundle.getSerializable("tip");
            if (str.equals("BusFragment")) {
                tvTitle.setText("Servis Ekle");

                Faker faker = new Faker(new Locale("tr", "TR"));

                spinnerServisler.setVisibility(View.GONE);
                etPlaka.setText(faker.number().digits(10));
                etSoforAdi.setText(faker.name().fullName());
                etRehberAdi.setText(faker.name().fullName());
                etrehberTel.setText(faker.phoneNumber().phoneNumber());
                etrehberMail.setText(getSaltString() + "@gmail.com");
                setSpinnerKapasite();
            }

            if (str.equals("SchoolActivity")) {
                tvTitle.setText("Servis Ekle");

                Faker faker = new Faker(new Locale("tr", "TR"));
                etRehberAdi.setVisibility(View.GONE);
                etrehberTel.setVisibility(View.GONE);
                etPlaka.setVisibility(View.GONE);
                etSoforAdi.setVisibility(View.GONE);
                etrehberMail.setVisibility(View.GONE);
                addImage.setVisibility(View.GONE);
                imgRehber.setVisibility(View.GONE);
                spinnerKapasite.setVisibility(View.GONE);
                okul = (School.OkulBean) bundle.getSerializable(Constance.KEY_ACTIVITY);

                getBusList();
            }

            if (str.equals("BusActivityEdit")) {
                spinnerServisler.setVisibility(View.GONE);
                imgRehber.setVisibility(View.GONE);
                addImage.setVisibility(View.GONE);
                etRehberAdi.setVisibility(View.GONE);
                etrehberMail.setVisibility(View.GONE);
                etrehberTel.setVisibility(View.GONE);
                tvTitle.setText("Servis Düzenle");
                bus = (Bus) bundle.getSerializable("bus");
                etPlaka.setText(bus.plaka);
                etSoforAdi.setText(bus.soforAdi);
                setSpinnerKapasite();
                int posKapasite = adapterKapasite.getPosition(bus.kapasite);
                spinnerKapasite.setSelection(posKapasite);
                btnSave.setText("Değiştir");

            }

            if (str.equals("")) {
                bus = (Bus) bundle.getSerializable(Constance.KEY_ACTIVITY);
                tvTitle.setText("Servis Düzenle");
                etPlaka.setText(bus.plaka);
                etSoforAdi.setText(bus.soforAdi);
                etRehberAdi.setText(bus.rehber.ad);
                etrehberMail.setText(bus.rehber.email);
                etrehberTel.setText(bus.rehber.telefon);
            }

        }
    }

    private void addImage() {
        new ImagePicker.Builder(AddOrEditBusActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void uploadFile(Bus bus, Guide guide, Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://ogrencitakipsistemi-25fa3.appspot.com/");
        StorageReference mountainImagesRef;
        if (bus.rehber.id != null)
            mountainImagesRef = storageRef.child("guides/" + bus.rehber.id + ".jpg");
        else
            mountainImagesRef = storageRef.child("guides/" + bus.rehber.yetkiKodu + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);

        // Handle unsuccessful uploads
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            mountainImagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                getBusDocumentReference(bus)
                        .update("rehber.imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                getGuideDocumentReference(guide)
                        .update("imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
            }).addOnFailureListener(Throwable::printStackTrace);
        }).addOnFailureListener(Throwable::printStackTrace);

    }


    private DocumentReference getBusDocumentReference(Bus bus) {
        if (bus.rehber.id == null) {
            return db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(userCompany.getUid())
                    .collection("busList")
                    .document(bus.rehber.yetkiKodu);
        } else {
            return db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(userCompany.getUid())
                    .collection("busList")
                    .document(bus.rehber.id);
        }
    }

    private DocumentReference getGuideDocumentReference(Guide guide) {
        if (guide.id != null) {
            return db.collection("users")
                    .document("guide")
                    .collection("guideList")
                    .document(guide.id);
        }
        if (guide.id == null) {
            return db.collection("users")
                    .document("guide")
                    .collection("guideList")
                    .document(guide.yetkiKodu);
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String picturePath = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH).get(0);
        File imageFile = new File(picturePath);
        try {
            compressedBitmap = new Compressor(AddOrEditBusActivity.this)
                    .setMaxHeight(150)
                    .setMaxWidth(150)
                    .compressToBitmap(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (compressedBitmap != null) {
            Glide.with(this)
                    .load(compressedBitmap)
                    .into(imgRehber);
        }
    }


    private void onSaveClick() {
        if (str.equals("SchoolActivity")) {
            String servis = spinnerServisler.getSelectedItem().toString();
            Bus busTmp = null;
            for (int i = 0; i < busList.size(); i++) {
                if (busList.get(i).plaka.equals(servis)) {
                    busTmp = busList.get(i);
                }
            }
            if (!validate(servis)) {
                onSaveFailed();
                return;
            }

            addTempUser(busTmp.rehber.email);


            if (bus == null) {
                //servisin veritabanına kaydedilmesi
                if (busTmp.rehber.id == null) {
                    Bus bus = new Bus();
                    bus.plaka = busTmp.plaka;
                    bus.okulId = okul.id;
                    bus.okulAdi = okul.okul;
                    bus.kapasite = busTmp.kapasite;
                    bus.soforAdi = busTmp.soforAdi;

                    bus.rehber = busTmp.rehber;

                    //Servisin okula atanması
                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(bus.okulId)
                            .collection("busList")
                            .document(bus.rehber.yetkiKodu)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("busList")
                            .document(bus.rehber.yetkiKodu)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


                } else {
                    Bus bus = new Bus();
                    bus.plaka = busTmp.plaka;
                    bus.okulId = okul.id;
                    bus.okulAdi = okul.okul;
                    bus.kapasite = busTmp.kapasite;
                    bus.soforAdi = busTmp.soforAdi;
                    bus.rehber = busTmp.rehber;

                    //Servisin okula atanması
                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(bus.okulId)
                            .collection("busList")
                            .document(bus.rehber.id)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("busList")
                            .document(bus.rehber.id)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


                }

            }
        }

        if (str.equals("BusFragment")) {
            String plaka = etPlaka.getText().toString();
            String soforAdi = etSoforAdi.getText().toString();
            String rehberMail = etrehberMail.getText().toString();
            String rehberAd = etRehberAdi.getText().toString();
            String rehberTel = etrehberTel.getText().toString();
            String kapasite = spinnerKapasite.getSelectedItem().toString();

            if (!validate(rehberAd, rehberTel, plaka, soforAdi, rehberMail, kapasite)) {
                onSaveFailed();
                return;
            }

            addTempUser(rehberMail);


            if (bus == null) {
                //servisin veritabanına kaydedilmesi
                Bus bus = new Bus();
                bus.plaka = plaka;
                bus.okulId = null;
                bus.okulAdi = "(Henüz Okul Atanmadı)";
                bus.kapasite = kapasite;
                bus.soforAdi = soforAdi;

                Guide rehber = new Guide();
                rehber.ad = rehberAd;
                rehber.email = rehberMail;
                rehber.id = null;
                rehber.telefon = rehberTel;
                rehber.yetkiKodu = tempUser.authorityCode;
                bus.rehber = rehber;
                bus.id=rehber.yetkiKodu;
                if (compressedBitmap != null) {
                    uploadFile(bus, rehber, compressedBitmap);
                } else
                    rehber.imageUrl = null;

                //Servisin okula atanması
                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("busList")
                        .document(bus.rehber.yetkiKodu)
                        .set(bus)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

                db.collection("users")
                        .document("guide")
                        .collection("guideList")
                        .document(rehber.yetkiKodu)
                        .set(rehber)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));


                //sendEmail(tempUser);
            }
        }


        if (str.equals("BusActivityEdit")) {
            String plaka = etPlaka.getText().toString();
            String soforAdi = etSoforAdi.getText().toString();
            String kapasite = spinnerKapasite.getSelectedItem().toString();

            if (!validate(plaka, soforAdi, kapasite)) {
                onSaveFailed();
                return;
            }


            bus.plaka = plaka;
            bus.kapasite = kapasite;
            bus.soforAdi = soforAdi;

            if (bus.rehber.id != null) {
                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("busList")
                        .document(bus.rehber.id)
                        .set(bus)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

                if (bus.okulId != null) {
                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(bus.okulId)
                            .collection("busList")
                            .document(bus.rehber.id)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
                }
            } else {

                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("busList")
                        .document(bus.rehber.yetkiKodu)
                        .set(bus)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
                if (bus.okulId != null) {
                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(bus.okulId)
                            .collection("busList")
                            .document(bus.rehber.yetkiKodu)
                            .set(bus)
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
                }
            }


        }
        onBackPressed();

    }


    private void sendEmail(TempUser tempUser) {

        String email = tempUser.email;
        String subject = "Öğrenci ve Servis Takip Sistemi";
        String message = "Yetki Kodu : " + tempUser.authorityCode;


        String yourgmail = "ogrenciveservistakipsistemi@gmail.com";
        String yourPassword = "ogrencitakibi469";

        BackgroundMail.newBuilder(this)
                .withUsername(yourgmail)
                .withPassword(yourPassword)
                .withMailto(email)
                .withType(BackgroundMail.TYPE_PLAIN)
                .withSubject(subject)
                .withBody(message)
                .withSendingMessage("Mail Gönderiliyor")
                .withSendingMessageError("Mail Gönderilemedi")
                .withSendingMessageSuccess("Mail Gönderildi")
                .withOnSuccessCallback(AddOrEditBusActivity.this::onBackPressed)
                .withOnFailCallback(AddOrEditBusActivity.this::onBackPressed)
                .send();

    }

    private void addTempUser(String yetkiliEmail) {
        tempUser.authorityCode = UUID.randomUUID().toString().substring(0, 8);
        tempUser.email = yetkiliEmail;
        tempUser.adderId = userCompany.getUid();
        tempUser.role = GUIDE.toString();

        //Add a new document with a authorityCode
        db.collection("tempUser")
                .document(tempUser.authorityCode)
                .set(tempUser)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

    }

    private boolean validate(String plaka, String soforAdi,
                             String kapasite) {
        boolean valid = true;


        if (plaka.isEmpty() || plaka.length() < 3) {
            etPlaka.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etPlaka.setError(null);
        }

        if (soforAdi.isEmpty() || soforAdi.length() < 3) {
            etSoforAdi.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etSoforAdi.setError(null);
        }


        //!Patterns.PHONE.matcher(rehberTel).matches()


        if (kapasite.equals("Kapasite Seçiniz")) {
            valid = false;
        } else {
        }

        return valid;
    }

    private boolean validate(String ad, String tel, String plaka, String soforAdi,
                             String rehberMail, String kapasite) {
        boolean valid = true;

        if (tel.isEmpty() || !isPhoneValid(tel)) {
            etrehberTel.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            etrehberTel.setError(null);
        }
        if (ad.isEmpty() || ad.length() < 3) {
            etRehberAdi.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etRehberAdi.setError(null);
        }
        if (plaka.isEmpty() || plaka.length() < 3) {
            etPlaka.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etPlaka.setError(null);
        }

        if (soforAdi.isEmpty() || soforAdi.length() < 3) {
            etSoforAdi.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etSoforAdi.setError(null);
        }


        if (rehberMail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(rehberMail).matches()) {
            etrehberMail.setError("Geçersiz mail adresi");
            valid = false;
        } else {
            etrehberMail.setError(null);
        }

        //!Patterns.PHONE.matcher(rehberTel).matches()


        if (kapasite.equals("Kapasite Seçiniz")) {
            valid = false;
        } else {
        }

        return valid;
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }


    private boolean validate(String servis) {
        boolean valid = true;

        if (servis.equals("Servis Seçiniz")) {
            valid = false;
        }


        return valid;
    }


    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }


    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız", Toast.LENGTH_SHORT).show();
    }


    private void getBusList() {
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("busList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        busList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        for (DocumentSnapshot documentSnapshot : documentList) {
                            Bus bus = documentSnapshot.toObject(Bus.class);
                            if (bus != null && bus.okulId == null) {
                                busList.add(bus);
                            }
                        }

                        setSpinnerServisler();
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });


    }

    private void setSpinnerServisler() {
        ArrayList<String> itemsServisler = new ArrayList<>();
        itemsServisler.add("Servis Seçiniz");
        for (int i = 0; i < busList.size(); i++) {
            itemsServisler.add(busList.get(i).plaka);
        }
        adapterServisler =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsServisler);
        spinnerServisler.setAdapter(adapterServisler);

    }

    private void setSpinnerKapasite() {
        ArrayList<String> itemsKapasite = new ArrayList<>();
        itemsKapasite.add("14");
        itemsKapasite.add("17");
        itemsKapasite.add("18");
        itemsKapasite.add("27");
        adapterKapasite =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsKapasite);
        spinnerKapasite.setAdapter(adapterKapasite);

    }

}
