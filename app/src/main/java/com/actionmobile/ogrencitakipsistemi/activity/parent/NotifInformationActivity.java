package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;
import com.google.firebase.firestore.FirebaseFirestore;

public class NotifInformationActivity extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    MyNotification notif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_information);
        TextView tvBaslik=findViewById(R.id.tv_baslik);
        TextView tvTitle=findViewById(R.id.tv_title);
        TextView tvMesaj=findViewById(R.id.tv_mesaj);
        TextView tvTarih=findViewById(R.id.tv_tarih);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
           notif  = (MyNotification) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        long milis=Long.valueOf(notif.dateInMilis);
        tvTarih.setText(Tools.getInstance().getDateFromMillisecond(milis));

        tvMesaj.setText(notif.message);
        tvBaslik.setText(notif.title);
        tvTitle.setText(notif.title);
    }

    public void back(View view) {
        onBackPressed();
    }
}
