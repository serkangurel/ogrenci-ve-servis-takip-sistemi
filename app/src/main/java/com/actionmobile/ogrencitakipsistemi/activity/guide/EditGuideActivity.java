package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class EditGuideActivity extends AppCompatActivity {

    private Guide guide;
    private FirebaseFirestore db;
    private CircleImageView imgGuide;
    private EditText etGuideName, etPhone;
    private Button btnSave;
    private Bus bus;
    private TextView tvAddImage;
    private ImageView imgBack;
    private Bitmap compressedBitmap = null;
    private static final String TAG = "EditGuideActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_guide);

        imgGuide = findViewById(R.id.img_guide);
        etGuideName = findViewById(R.id.input_guideName);
        etPhone = findViewById(R.id.input_phone);
        btnSave = findViewById(R.id.btn_save);
        tvAddImage = findViewById(R.id.tv_add_image);
        imgBack = findViewById(R.id.img_back);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            guide = (Guide) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        db = FirebaseFirestore.getInstance();
        setViews();

        tvAddImage.setOnClickListener(view -> onAddImageClick());
        imgGuide.setOnClickListener(view -> onAddImageClick());
        btnSave.setOnClickListener(view -> onSaveClick());
        imgBack.setOnClickListener(view -> onBackPressed());

    }

    private void setViews() {
        if (guide.imageUrl != null) {
            Glide.with(EditGuideActivity.this)
                    .load(guide.imageUrl)
                    .into(imgGuide);
        } else {
            imgGuide.setImageResource(R.drawable.ic_child);
        }
        etGuideName.setText(guide.ad);
        etPhone.setText(guide.telefon);
    }

    private void onAddImageClick() {
        new ImagePicker.Builder(EditGuideActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private void onSaveClick() {
        String name = etGuideName.getText().toString();
        String phone = etPhone.getText().toString();

        if (!validate(name, phone)) {
            onSaveFailed();
            return;
        }
        guide.ad = name;
        guide.telefon = phone;


        updateParentDB();
        onBackPressed();
    }

    private void updateParentDB() {
        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(guide.id)
                .set(guide, SetOptions.merge())
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));


        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(guide.firmaId)
                .collection("busList")
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
            for (DocumentSnapshot documentSnapshot : snapshotList) {
                Bus bus = documentSnapshot.toObject(Bus.class);
                if (bus.rehber.id != null && bus.rehber.id.equals(guide.id)) {
                    bus.rehber.ad = guide.ad;
                    bus.rehber.telefon = guide.telefon;
                    documentSnapshot.getReference()
                            .set(bus, SetOptions.merge());
                    if (bus.okulId != null) {
                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(guide.firmaId)
                                .collection("schoolList")
                                .document(bus.okulId)
                                .collection("busList")
                                .document(guide.id)
                                .set(bus, SetOptions.merge());
                    }


                    if (compressedBitmap != null) {
                        uploadFile(bus, compressedBitmap);
                    }
                }
            }
        });


    }

    private void uploadFile(Bus bus, Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://ogrencitakipsistemi-25fa3.appspot.com/");
        StorageReference mountainImagesRef = storageRef.child("guide/" + guide.id + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        // Handle unsuccessful uploads
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            mountainImagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                db.collection("users")
                        .document("guide")
                        .collection("guideList")
                        .document(guide.id)
                        .update("imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(guide.firmaId)
                        .collection("busList")
                        .document(guide.id)
                        .update("rehber.imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                if (bus.okulId != null) {
                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(guide.firmaId)
                            .collection("schoolList")
                            .document(bus.okulId)
                            .collection("busList")
                            .document(guide.id)
                            .update("rehber.imageUrl", uri.toString())
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                }

            }).addOnFailureListener(Throwable::printStackTrace);
        }).addOnFailureListener(Throwable::printStackTrace);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT", "OK");
            if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE) {
                String picturePath = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH).get(0);
                File imageFile = new File(picturePath);
                try {
                    compressedBitmap = new Compressor(EditGuideActivity.this)
                            .setMaxHeight(150)
                            .setMaxWidth(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(this)
                            .load(compressedBitmap)
                            .into(imgGuide);
                }
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT", "CANCELLED");
        }
    }

    private boolean validate(String name, String phone) {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 3) {
            etGuideName.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etGuideName.setError(null);
        }

        if (phone.isEmpty() || !isPhoneValid(phone)) {
            etPhone.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            etPhone.setError(null);
        }


        return valid;
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_SHORT).show();
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }

}
