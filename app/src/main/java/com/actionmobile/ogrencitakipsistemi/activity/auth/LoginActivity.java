package com.actionmobile.ogrencitakipsistemi.activity.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.admin.AdminActivity;
import com.actionmobile.ogrencitakipsistemi.activity.company.CompanyActivity;
import com.actionmobile.ogrencitakipsistemi.activity.guide.GuideActivity;
import com.actionmobile.ogrencitakipsistemi.activity.parent.ParentActivity;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.model.auth.Remember;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.orhanobut.hawk.Hawk;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.ADMIN;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.COMPANY;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.PARENT;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private ProgressDialog progressDialog;

    private EditText emailText;
    private EditText passwordText;
    private Button loginButton;
    private TextView linkSignup;
    private AppCompatCheckBox cbRemember;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailText = findViewById(R.id.input_email);
        passwordText = findViewById(R.id.input_password);
        loginButton = findViewById(R.id.btn_login);
        linkSignup = findViewById(R.id.link_signup);
        cbRemember = findViewById(R.id.cb_remember);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        loginButton.setOnClickListener(v -> login());
        linkSignup.setOnClickListener(view -> startSignupActivity());

        Remember remember = Hawk.get(Constance.KEY_REMEMBER, null);
        if (remember != null) {
            emailText.setText(remember.email);
            passwordText.setText(remember.password);
            cbRemember.setChecked(true);
        }

    }

    private void startSignupActivity() {
        Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeProgressDialog();
    }


    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        loginButton.setEnabled(false);

        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Giriş yapılıyor...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        onLoginSuccess();
                    } else {
                        // If sign in fails, display a message to the user.
                        onLoginFailed();
                    }

                });
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the CompanyActivity
        moveTaskToBack(true);
    }

    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null && currentUser.getDisplayName() != null) {
            startCorrectActivity(currentUser);
        } else {
            onLoginFailed();
        }
    }

    private void startCorrectActivity(FirebaseUser user) {
        if (user.getDisplayName().equals(ADMIN.toString())) {
            closeProgressDialog();
            updateRememberState();
            Hawk.put(Constance.HAWK_ISLOGGEDIN, true);
            Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
            startActivity(intent);
            finish();
        } else if (user.getDisplayName().equals(COMPANY.toString())) {
            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(user.getUid())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            closeProgressDialog();
                            updateRememberState();
                            Company company = task.getResult().toObject(Company.class);
                            if (company != null) {
                                Hawk.put(Constance.HAWK_ROLEOBJECT, company);
                                Hawk.put(Constance.HAWK_ISLOGGEDIN, true);
                                Intent intent = new Intent(LoginActivity.this, CompanyActivity.class);
                                intent.putExtra(Constance.KEY_ACTIVITY, company);
                                startActivity(intent);
                                finish();
                            } else {
                                onLoginFailed();
                            }

                        } else {
                            onLoginFailed();
                        }
                    });
        } else if (user.getDisplayName().equals(GUIDE.toString())) {
            db.collection("users")
                    .document("guide")
                    .collection("guideList")
                    .document(user.getUid())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            closeProgressDialog();
                            updateRememberState();
                            Guide guide = task.getResult().toObject(Guide.class);
                            if (guide != null) {
                                Hawk.put(Constance.HAWK_ROLEOBJECT, guide);
                                Hawk.put(Constance.HAWK_ISLOGGEDIN, true);
                                Intent intent = new Intent(LoginActivity.this, GuideActivity.class);
                                intent.putExtra(Constance.KEY_ACTIVITY, guide);
                                startActivity(intent);
                                finish();
                            } else {
                                onLoginFailed();
                            }

                        } else {
                            onLoginFailed();
                        }
                    });
        } else if (user.getDisplayName().equals(PARENT.toString())) {
            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(user.getUid())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            updateRememberState();
                            Parents parent = task.getResult().toObject(Parents.class);
                            if (parent != null) {
                                Hawk.put(Constance.HAWK_ROLEOBJECT, parent);
                                Hawk.put(Constance.HAWK_ISLOGGEDIN, true);

                                db.collection("users")
                                        .document("guide")
                                        .collection("guideList")
                                        .document(parent.guideId)
                                        .get()
                                        .addOnCompleteListener(task1 -> {
                                            if (task1.isSuccessful()) {
                                                closeProgressDialog();
                                                Guide guide = task1.getResult().toObject(Guide.class);
                                                Hawk.put(Constance.HAWK_GUIDEINPARENT, guide);
                                                Intent intent = new Intent(LoginActivity.this, ParentActivity.class);
                                                intent.putExtra(Constance.KEY_ACTIVITY, parent);
                                                intent.putExtra(Constance.KEY_GUIDE, guide);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                onLoginFailed();
                                            }
                                        });
                            } else {
                                onLoginFailed();
                            }

                        } else {
                            onLoginFailed();
                        }
                    });
        }
    }

    private void updateRememberState() {
        if (cbRemember.isChecked()) {
            Remember remember = new Remember();
            remember.email = emailText.getText().toString();
            remember.password = passwordText.getText().toString();
            Hawk.put(Constance.KEY_REMEMBER, remember);
        } else {
            Hawk.delete(Constance.KEY_REMEMBER);
        }
    }

    public void onLoginFailed() {
        closeProgressDialog();
        loginButton.setEnabled(true);
        Toast.makeText(getBaseContext(), "Giriş başarısız!", Toast.LENGTH_LONG).show();
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Geçersiz mail adresi!");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("Lütfen 4 ile 10 karakter arasında bir şifre giriniz.");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }
}
