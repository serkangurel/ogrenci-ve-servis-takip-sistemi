package com.actionmobile.ogrencitakipsistemi.activity.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.orhanobut.hawk.Hawk;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.yaml.snakeyaml.error.Mark;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.*;
import static com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

public class MapActivity extends AppCompatActivity implements OnConnectionFailedListener {

    private static final String TAG = "MapActivity";
    private static final float DEFAULT_ZOOM = 15f;

    private int lastFocusIndex = 0;
    private boolean isPermissionsGranted = false;
    private boolean isMarkerAdded = false;
    private GoogleMap mMap;
    private DatabaseReference myRef;
    private Handler handler = new Handler();

    private MyLocation myLocation = new MyLocation();
    private MyLocation busLocation;
    private Marker markerBus;
    private Runnable locationRunnable;

    private ImageView imgGps;
    private ImageView imgStudentLocations;
    private List<Child> childList = new ArrayList<>();
    private String userRole;
    private FirebaseFirestore db;
    private Guide guide;
    private String which;
    private Routes route;
    private LatLng target;
    private Child childInParent;
    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        imgGps = findViewById(R.id.img_gps);
        imgStudentLocations = findViewById(R.id.img_student_location);

        final RxPermissions rxPermissions = new RxPermissions(this);
        getLocationPermissions(rxPermissions);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("location");
        db = FirebaseFirestore.getInstance();

        imgGps.setOnClickListener(view -> onImageGpsClick());
        imgStudentLocations.setOnClickListener(view -> onImageStudentsClick());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userRole = bundle.getString(Constance.KEY_ACTIVITY);
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
            childList = (List<Child>) bundle.getSerializable(Constance.KEY_CHILD_LIST);
            which = bundle.getString("which");
            route = (Routes) bundle.getSerializable(Constance.KEY_ROUTE);
            childInParent = (Child) bundle.getSerializable(Constance.KEY_CHILD);
            busLocation = (MyLocation) bundle.getSerializable(Constance.KEY_LOCATION);

            if (which != null && which.equals("1")) { //gidiş
                if (route != null)
                    target = new LatLng(route.gidisGuzergahi.destination.latitude, route.gidisGuzergahi.destination.longitude);
            } else { //dönüş
                if (route != null)
                    target = new LatLng(route.gidisGuzergahi.destination.latitude, route.gidisGuzergahi.destination.longitude);
            }

            if (userRole != null && userRole.equals(GUIDE.toString()) && isPermissionsGranted) {
                Log.d(TAG, "onCreate: ");
                SmartLocation.with(MapActivity.this).location().start(locationListener);
            } else if (userRole != null && userRole.equals(PARENT.toString()) && isPermissionsGranted) {
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        busLocation = dataSnapshot.getValue(MyLocation.class);
                        addMarkerandAnimate();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: " + databaseError.getMessage());
                    }
                });
            }
        }
        locationRunnable = () -> SmartLocation.with(MapActivity.this).location().start(locationListener);
    }

    private void addChildAndTargetMarker() {
        MarkerOptions options = new MarkerOptions();
        options.position(new LatLng(childInParent.location.latitude, childInParent.location.longitude));
        options.title(childInParent.name);
        mMap.addMarker(options);

        //TODO:Marker activity pause/resume yapıldığında yanlış ekleniyor
        options.position(new LatLng(busLocation.latitude, busLocation.longitude));
        options.title("Bus");
        mMap.addMarker(options);
        markerBus = mMap.addMarker(options);
        markerBus.setTag("BusMarker");
        moveCamera(new LatLng(busLocation.latitude, busLocation.longitude), DEFAULT_ZOOM, "Bus Location");

        if (target != null) {
            options.position(new LatLng(target.latitude, target.longitude));
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            options.title("Hedef Okul");
            mMap.addMarker(options);
        }
    }


    private void sortChildList() {
        Collections.sort(childList, (child1, child2) -> {
            float distance1 = getDistanceBetweenTwoPoints(new LatLng(myLocation.latitude, myLocation.longitude)
                    , new LatLng(child1.location.latitude, child1.location.longitude));

            float distance2 = getDistanceBetweenTwoPoints(new LatLng(myLocation.latitude, myLocation.longitude)
                    , new LatLng(child2.location.latitude, child2.location.longitude));
            return Float.compare(distance1, distance2);
        });
    }

    private void onImageStudentsClick() {
        if (mMap != null && userRole != null && userRole.equals(GUIDE.toString()) && isPermissionsGranted) { // rehber
            focusStudent();
        } else { //veli
            if (mMap != null) {
                if (lastFocusIndex % 2 == 0) {
                    moveCamera(new LatLng(childInParent.location.latitude, childInParent.location.longitude), DEFAULT_ZOOM, "Child Location");
                    lastFocusIndex++;
                } else {
                    moveCamera(new LatLng(target.latitude, target.longitude), DEFAULT_ZOOM, "Target Location");
                    lastFocusIndex++;
                }
            }
        }
    }

    private void focusStudent() {
        Child child = null;
        //LatLng target = new LatLng(40.74192927095924, 30.332806706428524);   //sakarya üniversitesi
        if (lastFocusIndex < childList.size()) {
            child = childList.get(lastFocusIndex);
        }
        if (lastFocusIndex == childList.size()) {
            moveCamera(new LatLng(target.latitude, target.longitude), DEFAULT_ZOOM, "Target Location");
            lastFocusIndex = 0;
        } else {
            if (child != null) {
                moveCamera(new LatLng(child.location.latitude, child.location.longitude), DEFAULT_ZOOM, "Child Location");
                lastFocusIndex++;
            }
        }
    }

    private void onImageGpsClick() {
        if (userRole != null && userRole.equals(GUIDE.toString()) && isPermissionsGranted) { // rehber
            moveCamera(new LatLng(myLocation.latitude, myLocation.longitude), DEFAULT_ZOOM, "My Location");
        } else { //veli
            if (busLocation != null) {
                moveCamera(new LatLng(busLocation.latitude, busLocation.longitude), DEFAULT_ZOOM, "Bus Location");
            }
        }
    }

    OnLocationUpdatedListener locationListener = location -> {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        for (Child child : childList) {
            //TODO: Gidiş rotasının sonunda "gidis" + child.id keyini false a çek
            //TODO: Dönüş rotasının sonunda "donus" + child.id keyini false a çek

            float distance = getDistanceBetweenTwoPoints(new LatLng(lat, lng)
                    , new LatLng(child.location.latitude, child.location.longitude));

            if (which.equals("1")) { //Gidiş
                boolean isSend = Hawk.get("gidis" + child.id, false);
                if (distance < 500 && !isSend) {
                    Hawk.put("gidis" + child.id, true);
                    MyNotification notification = new MyNotification();
                    notification.id = UUID.randomUUID().toString();
                    notification.dateInMilis = String.valueOf(System.currentTimeMillis());
                    notification.title = "Servisiniz yaklaşmaktadır";
                    notification.message = "servisiniz " + child.name + " isimli çocuğunuzu almak üzere yaklaşmaktadır.";
                    notification.recipentId = child.parentId;
                    sendToFirestoreDB(child, notification);
                }
            } else { //Dönüş
                boolean isSend = Hawk.get("donus" + child.id, false);
                if (distance < 500 && !isSend) {
                    Hawk.put("donus" + child.id, true);
                    MyNotification notification = new MyNotification();
                    notification.id = UUID.randomUUID().toString();
                    notification.dateInMilis = String.valueOf(System.currentTimeMillis());
                    notification.title = "Servisiniz yaklaşmaktadır";
                    notification.message = "servisiniz " + child.name + " isimli çocuğunuzu bırakmak üzere yaklaşmaktadır.";
                    notification.recipentId = child.parentId;
                    sendToFirestoreDB(child, notification);
                }
            }
        }
        if (mMap != null) {
            myLocation.latitude = lat;
            myLocation.longitude = lng;
            myRef.setValue(myLocation);
            Toast.makeText(MapActivity.this, "latitude : " + myLocation.latitude + " longitude : " + myLocation.longitude, Toast.LENGTH_SHORT).show();
        }
        handler.postDelayed(locationRunnable, 5000);
    };

    private void sendToFirestoreDB(Child child, MyNotification notification) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(child.parentId)
                .collection("notificationList")
                .document(notification.id)
                .set(notification)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    //    @Override
//    public void onStop() {
//        if (userRole != null && userRole.equals(GUIDE.toString()) && isPermissionsGranted) {
//            SmartLocation.with(MapActivity.this).location().stop();
//            handler.removeCallbacks(locationRunnable);
//        }
//        super.onStop();
//    }
//
//    @Override
//    protected void onResume() {
//        if (userRole != null && userRole.equals(GUIDE.toString()) && isPermissionsGranted) {
//            SmartLocation.with(MapActivity.this).location().start(locationListener);
//        }
//        super.onResume();
//    }

    private void addMarkerandAnimate() {
        if (mMap != null && busLocation != null) {
            LatLng latLng = new LatLng(busLocation.latitude, busLocation.longitude);
//            if (!isMarkerAdded) {
//                markerBus = mMap.addMarker(new MarkerOptions()
//                        .position(latLng)
//                        .title("lat: " + latLng.latitude + " lng: " + latLng.longitude));
////                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car));
//                moveCamera(new LatLng(latLng.latitude, latLng.longitude), DEFAULT_ZOOM, "Bus Location");
//                isMarkerAdded = true;
//            }
            if (mMap != null && markerBus != null) {
                animateMarker(markerBus, latLng, false);
            }
        }
    }

    public void animateMarker(final Marker marker, final LatLng toPosition, final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                double lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude;
                double lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude;

                LatLng endLatLng = new LatLng(lat, lng);
                marker.setPosition(endLatLng);
                moveCamera(endLatLng, DEFAULT_ZOOM, "My myLocation");

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> {
            mMap = googleMap;
            if (userRole != null && userRole.equals(PARENT.toString())) { // veli
                mMap.setMyLocationEnabled(false);
                mMap.clear();
                addChildAndTargetMarker();
            } else {
                mMap.setMyLocationEnabled(true);
                getDeviceLocation();
            }
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        });
    }

    private float getDistanceBetweenTwoPoints(LatLng startPoint, LatLng endPoint) {
        float[] results = new float[1];
        Location.distanceBetween(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude, results);
        return results[0];
    }

    private void setMarkers() {
        //LatLng target = new LatLng(40.74192927095924, 30.332806706428524);   //sakarya üniversitesi
        MarkerOptions options = new MarkerOptions();
        for (Child child : childList) {
            options.position(new LatLng(child.location.latitude, child.location.longitude));
            options.title(child.name);
            mMap.addMarker(options);
        }
        if (target != null) {
            options.position(new LatLng(target.latitude, target.longitude));
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            options.title("Hedef Okul");
            mMap.addMarker(options);
        }

    }


    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        FusedLocationProviderClient flpClient;
        flpClient = LocationServices.getFusedLocationProviderClient(MapActivity.this);
        flpClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    if (location != null) {
                        myLocation.latitude = location.getLatitude();
                        myLocation.longitude = location.getLongitude();
                        moveCamera(new LatLng(location.getLatitude()
                                , location.getLongitude()), DEFAULT_ZOOM, "My Location");
                        setMarkers();
                        sortChildList();
                    } else {
                        Log.d(TAG, "getDeviceLocation: current myLocation null");
                        Toast.makeText(this, "Konum alınamamaktadır", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void moveCamera(LatLng latLng, float zoom, String title) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        hideKeyboard();
    }

    @SuppressLint({"CheckResult", "RxLeakedSubscription", "RxSubscribeOnError"})
    private void getLocationPermissions(RxPermissions rxPermissions) {
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) {
                        // All requested permissions are granted
                        isPermissionsGranted = true;
                        initMap();
                    } else {
                        // At least one permission is denied
                        isPermissionsGranted = false;
                    }
                });
    }

    private void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onBackPressed() {
        if (userRole != null && userRole.equals(GUIDE.toString())) {
            finish();
        } else {
            System.exit(0);
        }

        super.onBackPressed();
    }
}