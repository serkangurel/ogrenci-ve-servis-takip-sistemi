package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.auth.LoginActivity;
import com.actionmobile.ogrencitakipsistemi.fragments.guide.BusInfoFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.guide.GuideFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.guide.ParentsFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ChildFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.GuideInfoFragment;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class GuideActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "GuideActivity";
    private TextView tvTitle;
    private Guide guide;
    private ImageView imgSendMessage, imgEditGuide;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvTitle = findViewById(R.id.tv_title);
        imgSendMessage = findViewById(R.id.img_send_message);
        imgEditGuide = findViewById(R.id.img_edit_guide);
        mAuth = FirebaseAuth.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            guide = (Guide) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }

        db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(guide.id)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    guide = documentSnapshot.toObject(Guide.class);
                });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.route);

        tvTitle.setText("Güzergahlar");

        GuideFragment guideFragment = GuideFragment.newInstance(guide, "");
        setTransaction(guideFragment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            mAuth.signOut();
            Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTransaction(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, fragment);
        fragmentTransaction.commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.route) {
            imgSendMessage.setVisibility(View.GONE);
            imgEditGuide.setVisibility(View.GONE);
            tvTitle.setText("Güzergahlar");
            GuideFragment guideFragment = GuideFragment.newInstance(guide, "");
            setTransaction(guideFragment);
        } else if (id == R.id.bus_info) {
            imgSendMessage.setVisibility(View.GONE);
            imgEditGuide.setVisibility(View.GONE);
            tvTitle.setText("Servis Bilgileri");
            BusInfoFragment busInfoFragment = BusInfoFragment.newInstance(guide, "");
            setTransaction(busInfoFragment);
        } else if (id == R.id.parents) {
            imgSendMessage.setVisibility(View.VISIBLE);
            imgEditGuide.setVisibility(View.GONE);
            imgSendMessage.setOnClickListener(view -> startSendMessageActivity());
            tvTitle.setText("Veliler");
            ParentsFragment parentsFragment = ParentsFragment.newInstance(false, "");
            setTransaction(parentsFragment);
        } else if (id == R.id.students) {
            imgSendMessage.setVisibility(View.GONE);
            imgEditGuide.setVisibility(View.GONE);
            tvTitle.setText("Öğrenciler");
            ChildFragment childFragment = ChildFragment.newInstance("Guide", false, guide, null, null);
            setTransaction(childFragment);
        } else if (id == R.id.guide_info) {
            imgEditGuide.setVisibility(View.VISIBLE);
            imgEditGuide.setOnClickListener(view -> onEditClick());
            tvTitle.setText(guide.ad);
            GuideInfoFragment guideInfoFragment = GuideInfoFragment.newInstance(guide, "");
            setTransaction(guideInfoFragment);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onEditClick() {
        Intent intent = new Intent(GuideActivity.this, EditGuideActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY,guide);
        startActivity(intent);
    }

    private void startSendMessageActivity() {
        Intent intent = new Intent(GuideActivity.this, SendMessageActivity.class);
        startActivity(intent);
    }
}