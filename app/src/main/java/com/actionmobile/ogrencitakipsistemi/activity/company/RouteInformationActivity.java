package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class RouteInformationActivity extends AppCompatActivity {

    TextView tvRouteName;
    TextView tvRouteStart;
    TextView tvRouteArrival;
    TextView tvOgrenciSayisi;
    private static final String TAG = "RouteInfActivity";
    private FirebaseFirestore db;
    private FirebaseUser userCompany;
    Routes rota;
    private Bus bus;
    private String school_id;
    private String which;
    private int adet = 0;
    private TextView tvTitle;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_information);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            which = (String) bundle.getSerializable("asd");
            rota = (Routes) bundle.getSerializable(Constance.KEY_ACTIVITY);
            bus = (Bus) bundle.getSerializable("bus");
            school_id = (String) bundle.getSerializable("school");
        }

        ImageView editBt = findViewById(R.id.img_edit_route);
        ImageView geriBt = findViewById(R.id.img_back_bus);
        tvTitle = findViewById(R.id.tv_title);
        tvOgrenciSayisi = findViewById(R.id.tv_ogrenciSayisi);
        tvRouteName = findViewById(R.id.tv_routeName);
        tvRouteStart = findViewById(R.id.tv_gidis);
        tvRouteArrival = findViewById(R.id.tv_donus);

        setViews();
        DocumentReference routeRef = FirebaseFirestore.getInstance().document(rota.routeRefPath);
        routeRef.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w(TAG, "Listen failed.", e);
                return;
            }
            rota = snapshot.toObject(Routes.class);
            setViews();
        });

        geriBt.setOnClickListener(view -> onBackPressed());
        editBt.setOnClickListener(view -> {
            Intent intent = new Intent(this, AddRouteActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, bus);
            intent.putExtra("rota", rota);
            intent.putExtra("school", school_id);
            intent.putExtra("which", which);
            intent.putExtra("edit", 1);
            context.startActivity(intent);
        });
        if (rota != null && bus != null && school_id != null) {
            db = FirebaseFirestore.getInstance();
            DocumentReference routeRef2 = FirebaseFirestore.getInstance().document(rota.routeRefPath);
            routeRef2.collection("routeAddedChildList").addSnapshotListener((queryDocumentSnapshots, e) -> {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                String source = queryDocumentSnapshots != null
                        && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";

                if (queryDocumentSnapshots != null) {
                    List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                    adet = documentList.size();
                    setViews();
                } else {
                    Log.d(TAG, source + " data: null");
                }
            });

        }

    }

    private void setViews() {
        tvTitle.setText(rota.ad);
        if (which.equals("1")) {
            tvRouteName.setText(rota.ad + " (Gidiş)");
            tvRouteStart.setText("Adres: " + rota.gidisGuzergahi.startPoint.address + "\n\n" + "Saati: " + rota.gidisGuzergahi.starTime);
            tvRouteArrival.setText("Adres: " + rota.gidisGuzergahi.destination.address + "\n\n" + "Saati: " + rota.gidisGuzergahi.endTime);
        }

        if (which.equals("2")) {
            tvRouteName.setText(rota.ad + " (Dönüş)");
            tvRouteStart.setText("Adres: " + rota.donusGuzergahi.startPoint.address + "\n\n" + "Saati: " + rota.donusGuzergahi.starTime);
            tvRouteArrival.setText("Adres: " + rota.donusGuzergahi.destination.address + "\n\n" + "Saati: " + rota.donusGuzergahi.endTime);

        }

        if (adet == 0)
            tvOgrenciSayisi.setText("Henüz öğrenci kaydı bulunmamaktadır");
        else
            tvOgrenciSayisi.setText(adet + " adet kayıtlı öğrenci bulunmaktadır");
    }


}
