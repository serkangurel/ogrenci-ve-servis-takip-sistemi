package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.map.MapActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RouteActivity extends AppCompatActivity {

    private static final String TAG = "RouteActivity";
    private Guide guide;
    private Routes route;
    private String school_id;
    private String which;
    private FirebaseFirestore db;
    private List<Child> childList = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        ImageView imgBack = findViewById(R.id.img_back);
        TextView tv_route = findViewById(R.id.tv_route);
        TextView tvRouteName = findViewById(R.id.tv_routeName);
        TextView tvRouteTime = findViewById(R.id.tv_routeTime);
        Button btnRoute = findViewById(R.id.btn_route);
        Button btnInspection = findViewById(R.id.btn_inspection);

        db = FirebaseFirestore.getInstance();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            route = (Routes) bundle.getSerializable(Constance.KEY_ACTIVITY);
            guide = (Guide) bundle.getSerializable("guide");
            school_id = (String) bundle.getSerializable("school");
            which = (String) bundle.getSerializable("asd");
        }
        if (which.equals("1")) {
            tv_route.setText(route.ad + " - Gidiş");
            tvRouteName.setText(route.ad + " - Gidiş");
            tvRouteTime.setText(route.gidisGuzergahi.starTime + " - " + route.gidisGuzergahi.endTime);
        } else {
            tv_route.setText(route.ad + " - Dönüş");
            tvRouteName.setText(route.ad + " - Dönüş");
            tvRouteTime.setText(route.donusGuzergahi.starTime + " - " + route.donusGuzergahi.endTime);
        }

        btnRoute.setOnClickListener(view -> startMapActivity());
        btnInspection.setOnClickListener(view -> startInspectionActivity());
        imgBack.setOnClickListener(view -> onBackPressed());

    }

    public boolean checkTimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(RouteActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Harita yükleniyor...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void startMapActivity() {
        startProgressDialog();
        DocumentReference routeRef = FirebaseFirestore.getInstance().document(route.routeRefPath);
        routeRef.collection("routeAddedChildList").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                QuerySnapshot queryDocumentSnapshots = task.getResult();
                if (queryDocumentSnapshots != null) {
                    childList.clear();
                    List<DocumentSnapshot> childDocumentList = queryDocumentSnapshots.getDocuments();
                    if (!childDocumentList.isEmpty()) {
                        for (int i = 0; i < childDocumentList.size(); i++) {
                            int index = i;
                            DocumentSnapshot documentSnapshot = childDocumentList.get(i);
                            String childRefPath = (String) documentSnapshot.get("childRefPath");
                            DocumentReference childRef = FirebaseFirestore.getInstance().document(childRefPath);
                            Task<DocumentSnapshot> taskSnap = childRef.get().addOnCompleteListener(task1 -> {
                                if (task1.isSuccessful()) {
                                    Child child = task1.getResult().toObject(Child.class);
                                    if (child != null && !child.isInBus) {
                                        childList.add(child);
                                    }
                                    if (index == childDocumentList.size() - 1) {
                                        closeProgressDialog();
                                        Intent intent = new Intent(RouteActivity.this, MapActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(Constance.KEY_ACTIVITY, Tools.UserRoles.GUIDE.toString());
                                        bundle.putSerializable(Constance.KEY_GUIDE, guide);
                                        bundle.putString("which", which);
                                        bundle.putSerializable(Constance.KEY_ROUTE, route);
                                        bundle.putSerializable(Constance.KEY_CHILD_LIST, (Serializable) childList);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                }
                            });
                            while (!taskSnap.isComplete()) { //task tamamlanana kadar bekle
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        closeProgressDialog();
                        Toast.makeText(RouteActivity.this, "Rotada öğrenci bulunmamaktadır.", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                closeProgressDialog();
                Toast.makeText(RouteActivity.this, "Rota gösterilememektedir.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void startInspectionActivity() {
        Intent intent = new Intent(RouteActivity.this, InspectionActivity.class);
        intent.putExtra(Constance.KEY_GUIDE, guide);
        intent.putExtra(Constance.KEY_ROUTE, route);
        intent.putExtra("school", school_id);
        startActivity(intent);



    }

}