package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.schibstedspain.leku.LocationPickerActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;

public class AddOrEditChildActivity extends AppCompatActivity {

    private Child child;
    private EditText etChildName, etSchoolNo, etDate;
    private TextView tvAddress;
    private CircleImageView imgProfile;
    private FirebaseFirestore db;
    private FirebaseUser parent;
    private boolean isPermissionsGranted = false;
    private MyLocation location;
    private Bundle bundle;
    private Bitmap compressedBitmap = null;

    private static final int MAP_BUTTON_REQUEST_CODE = 1;
    private static final String TAG = "AddOrEditChildActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);

        ImageView imgBack = findViewById(R.id.img_back);
        TextView tvAddChild = findViewById(R.id.tv_add_child);
        TextView tvAddImage = findViewById(R.id.tv_add_image);
        Button btnSave = findViewById(R.id.btn_save);
        Button btnMap = findViewById(R.id.btn_map);

        etChildName = findViewById(R.id.input_childName);
        etDate = findViewById(R.id.et_date);
        etSchoolNo = findViewById(R.id.input_schoolNo);
        tvAddress = findViewById(R.id.tv_address);
        imgProfile = findViewById(R.id.img_profile);

        db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        parent = mAuth.getCurrentUser();
        RxPermissions rxPermissions = new RxPermissions(this);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            tvAddChild.setText("Çocuk Düzenle");
            child = (Child) bundle.getSerializable(Constance.KEY_ACTIVITY);
            String imageUrl = child.imageUrl;
            if (imageUrl != null) {
                Glide.with(AddOrEditChildActivity.this)
                        .load(imageUrl)
                        .into(imgProfile);
            } else {
                imgProfile.setImageResource(R.drawable.ic_child);
            }
            etChildName.setText(child.name);
            etDate.setText(child.birthDate);
            etSchoolNo.setText(child.schoolNo);
            tvAddress.setText(child.location.address);
            location = child.location;
        } else {
            tvAddChild.setText("Çocuk Ekle");
            imgProfile.setImageResource(R.drawable.ic_child);
        }


        etDate.setOnClickListener(dateClickListener);
        imgBack.setOnClickListener(view -> onBackPressed());
        btnSave.setOnClickListener(view -> onSaveClick());
        btnMap.setOnClickListener(view -> getLocationPermissions(rxPermissions));
        imgProfile.setOnClickListener(view -> addImage());
        tvAddImage.setOnClickListener(view -> addImage());

    }

    private void addImage() {
        new ImagePicker.Builder(AddOrEditChildActivity.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    private String getAge(String dateAsString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("tr"));
        Date dateAsObj = null;
        try {
            dateAsObj = sdf.parse(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(dateAsObj);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return String.valueOf(age);
    }

    View.OnClickListener dateClickListener = view -> {
        // Şimdiki zaman bilgilerini alıyoruz. güncel yıl, güncel ay, güncel gün.
        final Calendar takvim = Calendar.getInstance();
        int yil = takvim.get(Calendar.YEAR);
        int ay = takvim.get(Calendar.MONTH);
        int gun = takvim.get(Calendar.DAY_OF_MONTH);

        @SuppressLint("SetTextI18n")
        DatePickerDialog dpd = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog,
                (view1, year, month, dayOfMonth) -> {
                    String dayOfMonthh;
                    String monthh;
                    String yearr;
                    // ay değeri 0 dan başladığı için (Ocak=0, Şubat=1,..,Aralık=11)
                    // değeri 1 artırarak gösteriyoruz.
                    month += 1;
                    if (dayOfMonth < 10) {
                        dayOfMonthh = "0" + String.valueOf(dayOfMonth);
                    } else {
                        dayOfMonthh = String.valueOf(dayOfMonth);
                    }

                    if (month < 10) {
                        monthh = "0" + String.valueOf(month);
                    } else {
                        monthh = String.valueOf(month);
                    }
                    yearr = String.valueOf(year);
                    // year, month ve dayOfMonth değerleri seçilen tarihin değerleridir.
                    // Edittextte bu değerleri gösteriyoruz.
                    etDate.setText(dayOfMonthh + "/" + monthh + "/" + yearr);
                }, yil, ay, gun);
        // datepicker açıldığında set edilecek değerleri buraya yazıyoruz.
        // şimdiki zamanı göstermesi için yukarda tanmladığımz değşkenleri kullanyoruz.
        // showDialog penceresinin button bilgilerini ayarlıyoruz ve ekranda gösteriyoruz.
        dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", dpd);
        dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", dpd);
        dpd.setTitle("Doğum Tarihini Seçiniz");
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.show();
    };


    @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
    private void getLocationPermissions(RxPermissions rxPermissions) {
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    // All requested permissions are granted
                    if (isServicesOK() && granted) {
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(41.4036299, 2.1743558)
                                .withGeolocApiKey("AIzaSyBvJ_npi54cEf96z-GF1tvZ7T0CnITbdeQ")
                                .shouldReturnOkOnBackPressed()
                                .withGooglePlacesEnabled()
                                .build(AddOrEditChildActivity.this);

                        startActivityForResult(locationPickerIntent, MAP_BUTTON_REQUEST_CODE);
                    } else {
                        Toast.makeText(this,
                                getString(R.string.media_picker_some_permission_is_denied), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(AddOrEditChildActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
//            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(context, available, ERROR_DIALOG_REQUEST);
//            dialog.show();
        } else {
            Toast.makeText(AddOrEditChildActivity.this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT", "OK");
            if (requestCode == MAP_BUTTON_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                if (latitude != 0 && longitude != 0 && !address.isEmpty() && !postalcode.isEmpty()) {
                    tvAddress.setText(address);
                    tvAddress.setError(null);

                    location = new MyLocation();
                    location.latitude = latitude;
                    location.longitude = longitude;
                    location.address = address;
                    location.postalcode = postalcode;
                }
            } else if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE) {
                String picturePath = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH).get(0);
                File imageFile = new File(picturePath);
                try {
                    compressedBitmap = new Compressor(AddOrEditChildActivity.this)
                            .setMaxHeight(150)
                            .setMaxWidth(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(this)
                            .load(compressedBitmap)
                            .into(imgProfile);
                }
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT", "CANCELLED");
        }
    }

    private void uploadFile(Child child, Bitmap bitmap) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://ogrencitakipsistemi-25fa3.appspot.com/");
        StorageReference mountainImagesRef = storageRef.child("childs/" + child.id + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = mountainImagesRef.putBytes(data);
        // Handle unsuccessful uploads
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            mountainImagesRef.getDownloadUrl().addOnSuccessListener(uri -> {
                getDocumentReference(child)
                        .update("imageUrl", uri.toString())
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
            }).addOnFailureListener(Throwable::printStackTrace);
        }).addOnFailureListener(Throwable::printStackTrace);
    }

    private void onSaveClick() {
        String childName = etChildName.getText().toString();
        String schoolNo = etSchoolNo.getText().toString();
        String date = etDate.getText().toString();

        if (!validate(childName, schoolNo, date)) {
            onSaveFailed();
            return;
        }

        if (bundle != null) { //Edit child
            child.name = childName;
            child.schoolNo = schoolNo;
            child.birthDate = date;
            child.location = location;
            if (compressedBitmap != null) {
                uploadFile(child, compressedBitmap);
            }
            updateChildToFirestore(child);
        } else { //Add child
            Child child = new Child();
            child.id = UUID.randomUUID().toString();
            child.name = childName;
            child.schoolNo = schoolNo;
            child.isInBus = false;
            child.birthDate = date;
            child.age = getAge(date);
            child.routeId = null;
            child.childRefPath = getDocumentReference(child).getPath();
            if (parent != null) {
                child.parentId = parent.getUid();
            }
            child.location = location;

            if (compressedBitmap != null) {
                uploadFile(child, compressedBitmap);
            }
            addChildToFirestore(child);
        }

        onBackPressed();
    }


    private void addChildToFirestore(Child child) {
        getDocumentReference(child)
                .set(child)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private void updateChildToFirestore(Child child) {
        getDocumentReference(child)
                .set(child, SetOptions.merge())
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private DocumentReference getDocumentReference(Child child) {
        return db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.getUid())
                .collection("childs")
                .document(child.id);
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_SHORT).show();
    }

    private boolean validate(String childName, String schoolNo, String date) {
        boolean valid = true;

        if (childName.isEmpty() || childName.length() < 3) {
            etChildName.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etChildName.setError(null);
        }

        if (schoolNo.isEmpty() || schoolNo.length() < 3) {
            etSchoolNo.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etSchoolNo.setError(null);
        }

        if (date.isEmpty()) {
            etDate.setError("Doğum tarihini seciniz");
            valid = false;
        } else {
            etDate.setError(null);
        }

        if (location == null) {
            tvAddress.setError("Lütfen konum belirleyiniz");
            valid = false;
        } else {
            tvAddress.setError(null);
        }

        return valid;
    }


}