package com.actionmobile.ogrencitakipsistemi.activity.admin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.auth.LoginActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.admin.CompanyAdapter;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class AdminActivity extends AppCompatActivity {

    private static final String TAG = "AdminActivity";
    private FirebaseAuth mAuth;
    private DatabaseReference myRef;
    private List<Company> companyList = new ArrayList<>();
    private TextView tvInstruction;
    private RecyclerView rvCompany;
    private CompanyAdapter adapter;
    private CircularProgressBar circleProgress;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        fab = findViewById(R.id.fab);
        tvInstruction = findViewById(R.id.tv_instruction);
        rvCompany = findViewById(R.id.rv_company);
        circleProgress = findViewById(R.id.circle_progress);

        circleProgress.setVisibility(View.VISIBLE);
        db.collection("users")
                .document("company")
                .collection("companyList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        companyList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                companyList.add(documentSnapshot.toObject(Company.class));
                            }
                        }
                        adapter.setList(companyList);
                        updateUI();
                    } else {
                        Log.d(TAG, source + " data: null");
                    }

                });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AdminActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCompany.setLayoutManager(linearLayoutManager);

        adapter = new CompanyAdapter(AdminActivity.this);
        rvCompany.setAdapter(adapter);
        adapter.setList(companyList);

    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(view -> startAddOrEditCompanyActivity());
        if (companyList.size() == 0) {
            rvCompany.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvCompany.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }


    private void startAddOrEditCompanyActivity() {
        Intent intent = new Intent(AdminActivity.this, AddOrEditCompanyActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            mAuth.signOut();
            Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}