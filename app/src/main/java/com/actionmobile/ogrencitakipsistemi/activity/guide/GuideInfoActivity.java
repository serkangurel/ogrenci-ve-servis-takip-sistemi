package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.AddOrEditBusActivity;
import com.actionmobile.ogrencitakipsistemi.activity.company.BusActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import de.hdodenhof.circleimageview.CircleImageView;

public class GuideInfoActivity extends AppCompatActivity {


    FirebaseFirestore db = FirebaseFirestore.getInstance();
    TextView ad;
    TextView tel;
    TextView mail;
    Context context;
    Guide guide;
    private FirebaseUser userGuide;
    ImageView backBt;
    ImageView editGuide;
    CircleImageView rehberResim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_info);
        context=this;
        ad=findViewById(R.id.tv_static_name);
        tel=findViewById(R.id.tv_static_phone);
        mail=findViewById(R.id.tv_static_email);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        editGuide=findViewById(R.id.img_edit_guide);
        backBt=findViewById(R.id.img_back);
        userGuide = mAuth.getCurrentUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            guide = (Guide) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        String imageUrl = guide.imageUrl;
        if (imageUrl != null) {
            Glide.with(GuideInfoActivity.this)
                    .load(imageUrl)
                    .into(rehberResim);
        } else {
            rehberResim.setImageResource(R.drawable.ic_child);
        }

        ad.setText(guide.ad);
        tel.setText(guide.telefon);
        mail.setText(guide.email);

        editGuide.setOnClickListener(v ->startEditGuide());


    }


    public void startEditGuide(){


        Intent intent = new Intent(context, EditGuideActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, guide);
        context.startActivity(intent);
    }
}
