package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.auth.LoginActivity;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ChildFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.GuideInfoFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ParentInfoFragment;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.notificationFragment;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbruyelle.rxpermissions2.RxPermissions;

public class ParentActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "ParentActivity";
    private TextView tvTitle;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth1;
    private Parents parent;
    private Guide guide;
    private ImageView imgEdit, imgSendMail, imgCallPhone;
    private RxPermissions rxPermissions;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imgSendMail = findViewById(R.id.img_send_mail);
        imgCallPhone = findViewById(R.id.img_call_phone);
        imgEdit = findViewById(R.id.imgEdit);
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText("Çocuklar");

        mAuth = FirebaseAuth.getInstance();
        rxPermissions = new RxPermissions(this);
        db = FirebaseFirestore.getInstance();
        mAuth1 = FirebaseAuth.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parent = (Parents) bundle.getSerializable(Constance.KEY_ACTIVITY);
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.childs);

        getFirebaseToken();

        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.id)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    parent = documentSnapshot.toObject(Parents.class);
                });


        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(parent.guideId)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    guide = documentSnapshot.toObject(Guide.class);
                });


        ChildFragment childFragment = ChildFragment.newInstance("Parent", false, guide, null, null);
        setTransaction(childFragment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            mAuth.signOut();
            Intent intent = new Intent(ParentActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    sendRegistrationToServer(token);

                });

    }

    private void sendRegistrationToServer(String token) {
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.id)
                .update("messageToken", token)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                .addOnFailureListener(error -> Log.w(TAG, "Error updating document", error));
    }

    private void setTransaction(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.childs) {
            tvTitle.setText("Çocuklar");
            imgEdit.setVisibility(View.GONE);
            imgSendMail.setVisibility(View.GONE);
            imgCallPhone.setVisibility(View.GONE);

            ChildFragment childFragment = ChildFragment.newInstance("Parent", false, guide, null, null);
            setTransaction(childFragment);

        } else if (id == R.id.notifications) {
            tvTitle.setText("Bildirimler");
            imgEdit.setVisibility(View.GONE);
            imgSendMail.setVisibility(View.GONE);
            imgCallPhone.setVisibility(View.GONE);
            notificationFragment childFragment = notificationFragment.newInstance("", "");
            setTransaction(childFragment);

        } else if (id == R.id.guide) {
            tvTitle.setText("Rehber");
            imgEdit.setVisibility(View.GONE);
            imgSendMail.setVisibility(View.VISIBLE);
            imgCallPhone.setVisibility(View.VISIBLE);

            imgSendMail.setOnClickListener(view -> onSendMailClick(guide.email));
            imgCallPhone.setOnClickListener(view -> getCallPhonePermissions(guide.telefon));
            GuideInfoFragment guideInfoFragment = GuideInfoFragment.newInstance(guide, "");
            setTransaction(guideInfoFragment);
        } else if (id == R.id.information) {
            tvTitle.setText("Bilgilerim");
            imgSendMail.setVisibility(View.GONE);
            imgCallPhone.setVisibility(View.GONE);
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setOnClickListener(view -> onEditClick());
            ParentInfoFragment parentInfoFragment = ParentInfoFragment.newInstance(parent, "");
            setTransaction(parentInfoFragment);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onEditClick() {
        Intent intent = new Intent(ParentActivity.this, EditParentActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, parent);
        startActivity(intent);
    }

    private void onSendMailClick(String email) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        try {
            startActivity(Intent.createChooser(i, "Mail Gönder..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ParentActivity.this, "Yüklü e-posta istemcisi bulunamadı", Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult", "MissingPermission"})
    private void getCallPhonePermissions(String telefon) {
        rxPermissions.request(Manifest.permission.CALL_PHONE)
                .subscribe(granted -> {
                    // All requested permissions are granted
                    if (granted) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setTitle("Numarayı Ara");
                        alert.setMessage(telefon + " numarasını aramak istediğinize emin misiniz?");
                        alert.setPositiveButton("Evet", (dialog, which) -> {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + telefon));
                            startActivity(intent);
                        });
                        alert.setNegativeButton("Hayır", (dialog, which) -> {
                            // close dialog
                            dialog.dismiss();
                        });
                        alert.show();

                    } else {
                        Toast.makeText(this, "Lütfen izinleri onaylayınız!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}