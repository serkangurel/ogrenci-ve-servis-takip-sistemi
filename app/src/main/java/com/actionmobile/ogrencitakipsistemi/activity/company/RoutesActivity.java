package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.guide.RouteActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.company.RouteAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class RoutesActivity extends AppCompatActivity {
    private static final String TAG = "RoutesActivity";
    private Bus bus;
    private List<Routes> routeList = new ArrayList<>();
    private TextView tvPlaka, tvInstruction;
    private ImageView imgBackBus;
    private RecyclerView rvRoutes;
    private FirebaseFirestore db;
    private RouteAdapter adapter;
    private FloatingActionButton fab;
    private Context context;
    private String school_id;
    private FirebaseUser userCompany;
    private CircularProgressBar circleProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        // FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        setContentView(R.layout.activity_routes);
        tvPlaka = findViewById(R.id.tv_plaka);
        tvInstruction = findViewById(R.id.tv_instruction);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();
        imgBackBus = findViewById(R.id.img_back_bus);
        fab = findViewById(R.id.fab);
        circleProgress = findViewById(R.id.circle_progress);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            bus = (Bus) bundle.getSerializable(Constance.KEY_ACTIVITY);
            school_id = (String) bundle.getSerializable("school");
        }

        rvRoutes = findViewById(R.id.rv_route);
        rvRoutes.setLayoutManager(new LinearLayoutManager(RoutesActivity.this));
        adapter = new RouteAdapter(RoutesActivity.this, null, 0, school_id, bus);
        rvRoutes.setAdapter(adapter);
        imgBackBus.setOnClickListener(view -> onBackPressed());
        setViews();

        db = FirebaseFirestore.getInstance();

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .document(school_id)
                .collection("busList")
                .document(bus.rehber.id)
                .collection("routeList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        Toast.makeText(RoutesActivity.this, "Güzergahlara erişilememektedir", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        routeList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                Routes route = documentSnapshot.toObject(Routes.class);
                                routeList.add(route);
                            }
                            for (int i = 0; i < routeList.size() - 1; i++) {
                                for (int j = 0; j < routeList.size() - i - 1; j++) {
                                    if (!checkTimings(routeList.get(j).donusGuzergahi.endTime, routeList.get(j + 1).donusGuzergahi.endTime)) {
                                        Routes route = routeList.get(j);
                                        routeList.set(j, routeList.get(j + 1));
                                        routeList.set(j + 1, route);
                                    }
                                }
                            }
                            adapter.setList(routeList);
                            updateUI();
                        }
                        setViews();
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

    }

    public boolean checkTimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void startAddRouteActivity() {
        Intent intent = new Intent(context, AddRouteActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, bus);
        intent.putExtra("school", school_id);
        intent.putExtra("edit", 0);
        startActivity(intent);
    }

    public void back(View v) {
        onBackPressed();
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(view -> startAddRouteActivity());
        if (routeList.size() == 0) {
            rvRoutes.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvRoutes.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    private void setViews() {
        tvPlaka.setText(bus.plaka);
    }
}
