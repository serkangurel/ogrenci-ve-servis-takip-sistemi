package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.guide.AddStudentToBusActivity;
import com.actionmobile.ogrencitakipsistemi.activity.guide.RouteActivity;
import com.actionmobile.ogrencitakipsistemi.activity.map.MapActivity;
import com.actionmobile.ogrencitakipsistemi.activity.parent.ChooseChairActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;

public class RouteCardsActivity extends AppCompatActivity {
    private Bus bus;
    private String school_id;
    private Routes route;
    private Guide guide;
    private Context context = this;
    private Button btnAddStudents, btnShowChairs;
    private FirebaseFirestore db;
    private DatabaseReference myRef;
    private String callPlace;
    private Child child;
    private ProgressDialog progressDialog;
    private ImageView imgDeleteRoute;

    private String which1 = "1";
    private String which2 = "2";

    private TextView tvGuzergahAdi;
    private TextView tvGuzergahAdi2;
    private TextView tvKalkisSaati;
    private TextView tvKalkisSaati2;
    private TextView baslik;
    private TextView tvVarisSaati;
    private TextView tvVarisSaati2;

    private static final String TAG = "RouteCardsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_cards);

        db = FirebaseFirestore.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference("location");
        tvGuzergahAdi = findViewById(R.id.tv_guzergahAdi);
        tvGuzergahAdi2 = findViewById(R.id.tv_guzergahAdi2);
        tvKalkisSaati = findViewById(R.id.tv_kalkisSaati);
        tvKalkisSaati2 = findViewById(R.id.tv_kalkisSaati2);
        baslik = findViewById(R.id.tv_route);
        tvVarisSaati = findViewById(R.id.tv_varisSaati);
        tvVarisSaati2 = findViewById(R.id.tv_varisSaati2);
        imgDeleteRoute = findViewById(R.id.img_delete_route);
        ImageView geriBt = findViewById(R.id.img_back);

        btnAddStudents = findViewById(R.id.btn_addStudents);
        btnShowChairs = findViewById(R.id.btn_show_chair);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            route = (Routes) bundle.getSerializable(Constance.KEY_ACTIVITY);
            bus = (Bus) bundle.getSerializable("bus");
            school_id = bundle.getString("school");
            guide = (Guide) bundle.getSerializable("guide");
            callPlace = bundle.getString(Constance.KEY_CALLPLACE);
            child = (Child) bundle.getSerializable(Constance.KEY_CHILD);
        }

        DocumentReference routeRef = FirebaseFirestore.getInstance().document(route.routeRefPath);
        routeRef.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.w(TAG, "Listen failed.", e);
                return;
            }
            route = snapshot.toObject(Routes.class);
            setViews();
        });

        setViews();
        geriBt.setOnClickListener(view -> onBackPressed());


        if (guide != null) {
            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(guide.firmaId)
                    .collection("busList")
                    .document(guide.id)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Bus bus = task.getResult().toObject(Bus.class);
                            btnShowChairs.setOnClickListener(view -> startChooseChairActivity(bus));
                        }
                    });
            btnAddStudents.setOnClickListener(view -> startAddStudentToBusActivity());
        } else {
            imgDeleteRoute.setVisibility(View.VISIBLE);
            imgDeleteRoute.setOnClickListener(view -> onRouteDeleteClick());
            btnAddStudents.setVisibility(View.GONE);
            btnShowChairs.setVisibility(View.GONE);
        }
    }

    private void setViews() {
        baslik.setText(route.ad);
        tvGuzergahAdi.setText(route.ad + " - Gidiş");
        tvGuzergahAdi2.setText(route.ad + " - Dönüş");
        tvKalkisSaati.setText(route.gidisGuzergahi.starTime);
        tvKalkisSaati2.setText(route.donusGuzergahi.starTime);
        tvVarisSaati.setText(route.gidisGuzergahi.endTime);
        tvVarisSaati2.setText(route.donusGuzergahi.endTime);
    }

    private void onRouteDeleteClick() {
        DocumentReference routeRef = FirebaseFirestore.getInstance().document(route.routeRefPath);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Rota Sil");
        alert.setMessage("Silmek istediğinize emin misiniz?");
        alert.setPositiveButton("Evet", (dialog, which) -> {
            // continue with delete
            routeRef.delete();
            dialog.dismiss();
            onBackPressed();
        });
        alert.setNegativeButton("Hayır", (dialog, which) -> {
            // close dialog
            dialog.dismiss();
        });
        alert.show();
    }

    private void startChooseChairActivity(Bus bus) {
        Intent intent = new Intent(getApplicationContext(), ChooseChairActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, bus.kapasite);
        intent.putExtra(Constance.KEY_GUIDE, guide);
        intent.putExtra(Constance.KEY_CALLPLACE, GUIDE.toString());
        intent.putExtra("bus", bus);
        intent.putExtra("schoolId", school_id);
        intent.putExtra(Constance.KEY_ROUTE, route);
        context.startActivity(intent);
    }

    public boolean checkTimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void route1(View view) {
        //Date currentTime = Calendar.getInstance().getTime();
        //String time = Tools.getInstance().getTimeFromMillisecond(currentTime.getTime());

        //TODO: gerçek time kullan
        long millisecond = Tools.getInstance().getMillisecondFromTime(route.gidisGuzergahi.starTime);
        millisecond += 60000;
        String time = Tools.getInstance().getTimeFromMillisecond(millisecond);
        if (checkTimings(route.gidisGuzergahi.starTime, time) && checkTimings(time, route.gidisGuzergahi.endTime)) {
            if (guide == null) {
                if (callPlace != null && callPlace.equals("ChildInfo")) {
                    startMapActivity(which1);
                } else {
                    Intent intent = new Intent(this, RouteInformationActivity.class);
                    intent.putExtra(Constance.KEY_ACTIVITY, route);
                    intent.putExtra("bus", bus);
                    intent.putExtra("school", school_id);
                    intent.putExtra("asd", which1);
                    context.startActivity(intent);
                }

            } else {
                Intent intent = new Intent(this, RouteActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY, route);
                intent.putExtra("school", school_id);
                intent.putExtra("asd", which1);
                intent.putExtra("guide", guide);
                context.startActivity(intent);
            }

        } else {
            Toast.makeText(context, "Güzergah saatleri arasında görüntülenebilir.", Toast.LENGTH_SHORT).show();
        }
    }

    public void route2(View view) {
//        Date currentTime = Calendar.getInstance().getTime();
//        String time = Tools.getInstance().getTimeFromMillisecond(currentTime.getTime());

        //TODO: gerçek time kullan
        long millisecond = Tools.getInstance().getMillisecondFromTime(route.donusGuzergahi.starTime);
        millisecond += 60000;
        String time = Tools.getInstance().getTimeFromMillisecond(millisecond);
        if (checkTimings(route.donusGuzergahi.starTime, time) && checkTimings(time, route.donusGuzergahi.endTime)) {
            if (guide == null) {
                if (callPlace != null && callPlace.equals("ChildInfo")) {
                    startMapActivity(which2);
                } else {
                    Intent intent = new Intent(this, RouteInformationActivity.class);
                    intent.putExtra(Constance.KEY_ACTIVITY, route);
                    intent.putExtra("bus", bus);
                    intent.putExtra("school", school_id);
                    intent.putExtra("asd", which2);
                    context.startActivity(intent);
                }
            } else {
                Intent intent = new Intent(this, RouteActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY, route);
                intent.putExtra("school", school_id);
                intent.putExtra("asd", which2);
                intent.putExtra("guide", guide);
                context.startActivity(intent);
            }
        } else {
            Toast.makeText(context, "Güzergah saatleri arasında görüntülenebilir.", Toast.LENGTH_SHORT).show();
        }

    }

    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(RouteCardsActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Harita yükleniyor...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }


    private void startMapActivity(String which) {
        startProgressDialog();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                closeProgressDialog();
                MyLocation busLocation = dataSnapshot.getValue(MyLocation.class);
                Intent intent = new Intent(RouteCardsActivity.this, MapActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constance.KEY_ACTIVITY, Tools.UserRoles.PARENT.toString());
                bundle.putString("which", which);
                bundle.putSerializable(Constance.KEY_ROUTE, route);
                bundle.putSerializable(Constance.KEY_CHILD, child);
                bundle.putSerializable(Constance.KEY_LOCATION, busLocation);
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });

    }

    private void startAddStudentToBusActivity() {
        Intent intent = new Intent(RouteCardsActivity.this, AddStudentToBusActivity.class);
        intent.putExtra(Constance.KEY_GUIDE, guide);
        intent.putExtra(Constance.KEY_ROUTE, route);
        startActivity(intent);
    }
}
