package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.admin.TempUser;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Random;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.PARENT;

public class AddParentActivity extends AppCompatActivity {

    private static final String TAG = "AddParentActivity";
    private EditText etMail, etName, etPhone;
    private FirebaseFirestore db;
    private TempUser tempUser = new TempUser();
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parent);
        CircleImageView imgProfile = findViewById(R.id.img_profile);
        etName = findViewById(R.id.input_name);
        etMail = findViewById(R.id.input_mail);
        etPhone = findViewById(R.id.input_phone);
        imgBack = findViewById(R.id.img_back);
        db = FirebaseFirestore.getInstance();
        Button btnSave = findViewById(R.id.btn_save);

        etMail.setText(getSaltString() + "@gmail.com");
        imgProfile.setImageResource(R.drawable.ic_child);
        imgBack.setOnClickListener(view -> onBackPressed());
        btnSave.setOnClickListener(view -> onSaveClick());
    }

    private void onSaveClick() {
        String name = etName.getText().toString();
        String email = etMail.getText().toString();
        String phone = etPhone.getText().toString();

        if (!validate(name, email, phone)) {
            onSaveFailed();
            return;
        }

        addTempUser(email);

        Parents parent = new Parents();
        parent.id = null;
        parent.email = email;
        parent.guideId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        parent.authorityCode = tempUser.authorityCode;
        parent.imageUrl = null;
        parent.name = name;
        parent.phoneNumber = phone;
        parent.role = PARENT.toString();

        addParentToFirestore(parent);
        onBackPressed();
    }

    private void addParentToFirestore(Parents parent) {
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(tempUser.authorityCode)
                .set(parent)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private void addTempUser(String yetkiliEmail) {
        tempUser.authorityCode = UUID.randomUUID().toString().substring(0, 8);
        tempUser.email = yetkiliEmail;
        tempUser.adderId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        tempUser.role = PARENT.toString();

        //Add a new document with a authorityCode
        db.collection("tempUser")
                .document(tempUser.authorityCode)
                .set(tempUser)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    private boolean validate(String name, String email, String phone) {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 3) {
            etName.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (phone.isEmpty() || !isPhoneValid(phone)) {
            etPhone.setError("Geçersiz telefon numarası");
            valid = false;
        } else {
            etPhone.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etMail.setError("Geçersiz Mail Adresi!");
            valid = false;
        } else {
            etMail.setError(null);
        }

        return valid;
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_SHORT).show();
    }

    private boolean isPhoneValid(String phone) {
        String countryCode = "90";
        String phoneNumber = phone.trim();
        if (isValidPhoneNumber(phoneNumber)) {
            return validateUsing_libphonenumber(countryCode, phoneNumber);
        } else {
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }
        return phoneNumberUtil.isValidNumber(phoneNumber);
    }

}
