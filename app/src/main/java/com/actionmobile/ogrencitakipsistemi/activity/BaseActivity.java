package com.actionmobile.ogrencitakipsistemi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.actionmobile.ogrencitakipsistemi.otto.BusProvider;

public class BaseActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }
}
