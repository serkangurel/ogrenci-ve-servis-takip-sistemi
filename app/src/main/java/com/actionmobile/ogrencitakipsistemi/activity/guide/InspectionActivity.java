package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.BaseActivity;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ChildFragment;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.otto.BusProvider;
import com.actionmobile.ogrencitakipsistemi.otto.CbItemsSelected;
import com.actionmobile.ogrencitakipsistemi.otto.Deliver;
import com.actionmobile.ogrencitakipsistemi.otto.WaitUntilLoading;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.squareup.otto.Subscribe;

public class InspectionActivity extends BaseActivity {

    private Guide guide;
    private Routes route;
    private String school_id;
    private Button btnDeliver;
    private int deliverIndex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection);

        ImageView imgBack = findViewById(R.id.img_back);
        btnDeliver = findViewById(R.id.btn_deliver);

        imgBack.setOnClickListener(view -> onBackPressed());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
            school_id = (String) bundle.getSerializable("school");
            route = (Routes) bundle.getSerializable(Constance.KEY_ROUTE);
        }

        ChildFragment childFragment = ChildFragment.newInstance("Inspection", true, guide, route, school_id);
        setTransaction(childFragment);
    }

    @Subscribe
    public void WaitUntilLoading(WaitUntilLoading event) {
        btnDeliver.setOnClickListener(view -> onButtonDeliverClick());
    }

    private void onButtonDeliverClick() {
        if (deliverIndex % 2 == 0) {
            BusProvider.getInstance().post(new Deliver(true));
            btnDeliver.setBackground(getResources().getDrawable(R.drawable.shape_button_green));
        } else {
            BusProvider.getInstance().post(new Deliver(false));
            btnDeliver.setBackground(getResources().getDrawable(R.drawable.shape_button_red));
        }
        deliverIndex++;
    }

    private void setTransaction(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, fragment);
        fragmentTransaction.commit();
    }
}