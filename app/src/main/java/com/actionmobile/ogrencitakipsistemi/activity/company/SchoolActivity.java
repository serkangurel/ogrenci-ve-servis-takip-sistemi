package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.adapter.company.BusAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.School.OkulBean;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class SchoolActivity extends AppCompatActivity {
    private static final String TAG = "SchoolActivity";
    private List<Bus> busList = new ArrayList<>();
    private OkulBean school;
    private TextView tvSchoolName;
    private TextView tvInstruction;
    private RecyclerView rvSchool;
    private FirebaseFirestore db;
    private BusAdapter adapter;
    private FirebaseUser userCompany;
    private CircularProgressBar circleProgress;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);

        circleProgress = findViewById(R.id.circle_progress);
        ImageView imgBack = findViewById(R.id.img_back_school);
        tvSchoolName = findViewById(R.id.tv_school_name);
        tvInstruction = findViewById(R.id.tv_instruction);
        ImageView imgEditSchool = findViewById(R.id.img_edit_school);
        ImageView imgDeleteSchool = findViewById(R.id.img_delete_school);
        fab = findViewById(R.id.fab);
        rvSchool = findViewById(R.id.rv_school);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSchool.setLayoutManager(layoutManager);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            school = (OkulBean) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        setViews();

        adapter = new BusAdapter(SchoolActivity.this, school.id);
        rvSchool.setAdapter(adapter);

        db = FirebaseFirestore.getInstance();

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .document(school.id)
                .collection("busList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        busList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                Bus bus = documentSnapshot.toObject(Bus.class);
                                busList.add(bus);
                            }

                        }
                        adapter.setList(busList);
                        updateUI();
                        setViews();
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        imgEditSchool.setOnClickListener(view -> startEditSchoolActivity());
        imgBack.setOnClickListener(view -> onBackPressed());

        imgDeleteSchool.setOnClickListener(view -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Okul Sil");
            alert.setMessage("Silmek istediğinize emin misiniz?");
            alert.setPositiveButton("Evet", (dialog, which) -> {
                // continue with delete
                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("schoolList")
                        .document(school.id)
                        .delete()
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                dialog.dismiss();
                onBackPressed();
            });
            alert.setNegativeButton("Hayır", (dialog, which) -> {
                // close dialog
                dialog.dismiss();
            });
            alert.show();
        });

    }

    private void setViews() {
        tvSchoolName.setText(school.okul);
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(view -> startAddBusActivity());
        if (busList.size() == 0) {
            rvSchool.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvSchool.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    private void startEditSchoolActivity() {
        Intent intent = new Intent(SchoolActivity.this, AddOrEditSchoolActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, school);
        startActivity(intent);
    }

    public void startAddBusActivity() {
        Intent intent = new Intent(SchoolActivity.this, AddOrEditBusActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, school);
        String tip = "SchoolActivity";
        intent.putExtra("tip", tip);
        startActivity(intent);
    }
}
