package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.company.School;
import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;
import com.schibstedspain.leku.LocationPickerActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;

public class AddRouteActivity extends AppCompatActivity
        implements RangeTimePickerDialog.ISelectedTime {

    private MyLocation location;
    private Routes route;
    private Button btnMap;
    private static final String TAG = "AddRouteActivity";
    private EditText etKalkisYeri;
    private EditText etVarisYeri;
    private School.OkulBean okul;
    private static final int MAP_BUTTON_REQUEST_CODE = 1;
    private TextView tvStartTimeGidis;
    private TextView tvArrivalTimeGidis;
    private TextView tvStartTimeDonus;
    private TextView tvArrivalTimeDonus;
    private TextView tvAddress;
    private TextView tvRouteName;
    private FirebaseFirestore db;
    private FirebaseUser userCompany;
    private Bus bus;
    private int x = 0;
    private String school_id, which;
    int edit;
    private List<Routes> routeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_route);
        ImageView imgBack = findViewById(R.id.img_back);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        TextView tvDonus = findViewById(R.id.tv_donus);
        TextView dk = findViewById(R.id.dk);
        TextView dv = findViewById(R.id.dv);
        TextView tvGidis = findViewById(R.id.tv_gidis);
        TextView gk = findViewById(R.id.gk);
        TextView gv = findViewById(R.id.gv);
        TextView baslik = findViewById(R.id.tv_add_route);
        userCompany = mAuth.getCurrentUser();
        btnMap = findViewById(R.id.btn_map);
        tvRouteName = findViewById(R.id.et_guzergah);
        tvStartTimeGidis = findViewById(R.id.tv_gidisStartTime);
        tvArrivalTimeGidis = findViewById(R.id.tv_gidisArrivalTime);
        tvStartTimeDonus = findViewById(R.id.tv_donusStartTime);
        tvArrivalTimeDonus = findViewById(R.id.tv_donusArrivalTime);
        tvAddress = findViewById(R.id.tv_address);
        Button btnSave = findViewById(R.id.btn_save);
        Button btnTimeRangeGidis = findViewById(R.id.btn_timeRangeGidis);
        Button btnTimeRangeDonus = findViewById(R.id.btn_timeRangeDonus);

        RxPermissions rxPermissions = new RxPermissions(this);
        btnMap.setOnClickListener(v -> getLocationPermissions(rxPermissions));
        imgBack.setOnClickListener(view -> onBackPressed());
        btnSave.setOnClickListener(view -> onSaveClick());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bus = (Bus) bundle.getSerializable(Constance.KEY_ACTIVITY);
            school_id = (String) bundle.getSerializable("school");
            which = bundle.getString("which");
            edit = bundle.getInt("edit");
            route = (Routes) bundle.getSerializable("rota");
        }
        if (edit == 1) {
            baslik.setText("Güzergah Düzenle");
            if (which.equals("1")) {

                tvDonus.setVisibility(View.GONE);
                dv.setVisibility(View.GONE);
                dk.setVisibility(View.GONE);
                tvStartTimeDonus.setVisibility(View.GONE);
                tvArrivalTimeDonus.setVisibility(View.GONE);
                btnTimeRangeDonus.setVisibility(View.GONE);
                tvRouteName.setText(route.ad);
                tvStartTimeGidis.setText(route.gidisGuzergahi.starTime);
                tvArrivalTimeGidis.setText(route.gidisGuzergahi.endTime);
                tvAddress.setText(route.gidisGuzergahi.startPoint.address);
            }

            if (which.equals("2")) {
                tvGidis.setVisibility(View.GONE);
                gv.setVisibility(View.GONE);
                gk.setVisibility(View.GONE);
                tvStartTimeGidis.setVisibility(View.GONE);
                tvArrivalTimeGidis.setVisibility(View.GONE);
                btnTimeRangeGidis.setVisibility(View.GONE);
                tvRouteName.setText(route.ad);
                tvStartTimeDonus.setText(route.donusGuzergahi.starTime);
                tvArrivalTimeDonus.setText(route.donusGuzergahi.endTime);
                tvAddress.setText(route.donusGuzergahi.startPoint.address);
            }
        }
        db = FirebaseFirestore.getInstance();

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.e(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        List<DocumentSnapshot> schoolDocumentList = queryDocumentSnapshots.getDocuments();
                        if (schoolDocumentList != null) {
                            for (DocumentSnapshot documentSnapshot : schoolDocumentList) {
                                School.OkulBean school = documentSnapshot.toObject(School.OkulBean.class);
                                if (school.id.equals(school_id))
                                    okul = school;

                            }

                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .document(school_id)
                .collection("busList")
                .document(bus.rehber.id)
                .collection("routeList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        routeList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                Routes route = documentSnapshot.toObject(Routes.class);
                                if (route != null) {
                                    routeList.add(route);
                                }
                            }
                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        btnTimeRangeGidis.setOnClickListener(view -> {
            // Create an instance of the dialog fragment and show it
            x = 0;
            RangeTimePickerDialog dialog = new RangeTimePickerDialog();
            dialog.newInstance();
            dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
            dialog.setIs24HourView(true); // Indicates if the format should be 24 hours
            dialog.setColorBackgroundHeader(R.color.colorPrimary); // Set Color of Background header dialog
            dialog.setColorTextButton(R.color.colorPrimaryDark); // Set Text color of button
            dialog.setValidateRange(true);
            FragmentManager fragmentManager = getFragmentManager();
            dialog.show(fragmentManager, "");
        });

        btnTimeRangeDonus.setOnClickListener(view -> {
            // Create an instance of the dialog fragment and show it
            x = 1;
            RangeTimePickerDialog dialog = new RangeTimePickerDialog();
            dialog.newInstance();
            dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
            dialog.setIs24HourView(true); // Indicates if the format should be 24 hours
            dialog.setColorBackgroundHeader(R.color.colorPrimary); // Set Color of Background header dialog
            dialog.setColorTextButton(R.color.colorPrimaryDark); // Set Text color of button
            dialog.setValidateRange(true);
            FragmentManager fragmentManager = getFragmentManager();
            dialog.show(fragmentManager, "");
        });

    }


    @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
    private void getLocationPermissions(RxPermissions rxPermissions) {
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    // All requested permissions are granted
                    if (isServicesOK() && granted) {
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(41.4036299, 2.1743558)
                                .withGeolocApiKey("https://www.googleapis.com/geolocation/v1/geolocate?key=" + getString(R.string.google_maps_API_KEY))
                                .shouldReturnOkOnBackPressed()
                                .withGooglePlacesEnabled()
                                .build(AddRouteActivity.this);

                        startActivityForResult(locationPickerIntent, MAP_BUTTON_REQUEST_CODE);
                    } else {
                        Toast.makeText(this,
                                getString(R.string.media_picker_some_permission_is_denied), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT", "OK");
            if (requestCode == MAP_BUTTON_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                if (latitude != 0 && longitude != 0 && !address.isEmpty() && !postalcode.isEmpty()) {
                    tvAddress.setText(address);
                    tvAddress.setError(null);

                    location = new MyLocation();
                    location.latitude = latitude;
                    location.longitude = longitude;
                    location.address = address;
                    location.postalcode = postalcode;
                }
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT", "CANCELLED");
        }
    }

    private boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(AddRouteActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
//            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(context, available, ERROR_DIALOG_REQUEST);
//            dialog.show();
        } else {
            Toast.makeText(AddRouteActivity.this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSelectedTime(int hourStart, int minuteStart, int hourEnd, int minuteEnd) {

        String startHour;
        String startMinute;
        String endHour;
        String endMinute;

        if (hourStart < 10) {
            startHour = "0" + String.valueOf(hourStart);
        } else {
            startHour = String.valueOf(hourStart);
        }
        if (minuteStart < 10) {
            startMinute = "0" + String.valueOf(minuteStart);
        } else {
            startMinute = String.valueOf(minuteStart);
        }
        if (hourEnd < 10) {
            endHour = "0" + String.valueOf(hourEnd);
        } else {
            endHour = String.valueOf(hourEnd);
        }
        if (minuteEnd < 10) {
            endMinute = "0" + String.valueOf(minuteEnd);
        } else {
            endMinute = String.valueOf(minuteEnd);
        }

        if (x == 0) {
            tvStartTimeGidis.setText(startHour + ":" + startMinute);
            tvArrivalTimeGidis.setText(endHour + ":" + endMinute);

            tvStartTimeGidis.setError(null);
            tvArrivalTimeGidis.setError(null);
        }

        if (x == 1) {
            tvStartTimeDonus.setText(startHour + ":" + startMinute);
            tvArrivalTimeDonus.setText(endHour + ":" + endMinute);

            tvStartTimeDonus.setError(null);
            tvArrivalTimeDonus.setError(null);
        }
    }

    private void onSaveClick() {
        String kalkisSaatiGidis = tvStartTimeGidis.getText().toString();
        String varisSaatiGidis = tvArrivalTimeGidis.getText().toString();
        String kalkisSaatiDonus = tvStartTimeDonus.getText().toString();
        String varisSaatiDonus = tvArrivalTimeDonus.getText().toString();
        String guzergahAdi = tvRouteName.getText().toString();
        if (edit == 1) {
            for (int i = 0; i < routeList.size(); i++) {
                if (routeList.get(i).id.equals(route.id))
                    routeList.remove(i);
            }
            if (which.equals("1")) {
                if (location == null)
                    location = route.gidisGuzergahi.startPoint;
                kalkisSaatiDonus = route.donusGuzergahi.starTime;
                varisSaatiDonus = route.donusGuzergahi.endTime;

                if (validate(guzergahAdi, kalkisSaatiGidis, varisSaatiGidis, kalkisSaatiDonus, varisSaatiDonus) && validate2(location, okul.location)) {
                    route.ad = guzergahAdi;
                    route.gidisGuzergahi.starTime = kalkisSaatiGidis;
                    route.gidisGuzergahi.endTime = varisSaatiGidis;
                    route.gidisGuzergahi.startPoint = location;
                    route.gidisGuzergahi.destination = okul.location;


                    route.donusGuzergahi.starTime = kalkisSaatiDonus;
                    route.donusGuzergahi.endTime = varisSaatiDonus;
                    route.donusGuzergahi.startPoint = okul.location;
                    route.donusGuzergahi.destination = location;

                    routeList.add(route);

                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(school_id)
                            .collection("busList")
                            .document(bus.rehber.id)
                            .collection("routeList")
                            .document(route.id)
                            .set(route, SetOptions.merge())
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                    onBackPressed();
                } else {
                    Toast.makeText(this, "Düzenleme başarısız!", Toast.LENGTH_SHORT).show();
                }
            }
            if (which.equals("2")) {
                if (location == null)
                    location = route.donusGuzergahi.destination;
                kalkisSaatiGidis = route.gidisGuzergahi.starTime;
                varisSaatiGidis = route.gidisGuzergahi.endTime;
                routeList.remove(route);
                if (validate(guzergahAdi, kalkisSaatiGidis, varisSaatiGidis, kalkisSaatiDonus, varisSaatiDonus) && validate2(location, okul.location)) {
                    route.ad = guzergahAdi;
                    route.gidisGuzergahi.starTime = kalkisSaatiGidis;
                    route.gidisGuzergahi.endTime = varisSaatiGidis;
                    route.gidisGuzergahi.startPoint = location;
                    route.gidisGuzergahi.destination = okul.location;


                    route.donusGuzergahi.starTime = kalkisSaatiDonus;
                    route.donusGuzergahi.endTime = varisSaatiDonus;
                    route.donusGuzergahi.startPoint = okul.location;
                    route.donusGuzergahi.destination = location;

                    routeList.add(route);

                    db.collection("users")
                            .document("company")
                            .collection("companyList")
                            .document(userCompany.getUid())
                            .collection("schoolList")
                            .document(school_id)
                            .collection("busList")
                            .document(bus.rehber.id)
                            .collection("routeList")
                            .document(route.id)
                            .set(route, SetOptions.merge())
                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                            .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

                    onBackPressed();
                } else {
                    Toast.makeText(this, "Düzenleme başarısız!", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            if (validate(guzergahAdi, kalkisSaatiGidis, varisSaatiGidis, kalkisSaatiDonus, varisSaatiDonus) && validate2(location, okul.location)) {
                Routes route = new Routes();
                route.id = UUID.randomUUID().toString();
                route.gidisGuzergahi = new Routes.RouteItem();
                route.donusGuzergahi = new Routes.RouteItem();

                route.ad = guzergahAdi;
                route.gidisGuzergahi.starTime = kalkisSaatiGidis;
                route.gidisGuzergahi.endTime = varisSaatiGidis;
                route.gidisGuzergahi.startPoint = location;
                route.gidisGuzergahi.destination = okul.location;


                route.donusGuzergahi.starTime = kalkisSaatiDonus;
                route.donusGuzergahi.endTime = varisSaatiDonus;
                route.donusGuzergahi.startPoint = okul.location;
                route.donusGuzergahi.destination = location;
                routeList.add(route);

                DocumentReference routeRef = db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("schoolList")
                        .document(school_id)
                        .collection("busList")
                        .document(bus.rehber.id)
                        .collection("routeList")
                        .document(route.id);
                route.routeRefPath = routeRef.getPath();
                routeRef.set(route)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));


                onBackPressed();
            } else {
                Toast.makeText(this, "Kayıt başarısız!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate2(MyLocation location, MyLocation location1) {
        boolean valid = true;

        if (location != null && location1 != null) {
            valid = true;
        } else {
            valid = false;
            tvAddress.setError("Kalkış Adresi Giriniz!");
        }
        return valid;
    }

    public boolean checkTimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean validate(String guzergahAdi, String kalkisSaatiGidis, String varisSaatiGidis,
                             String kalkisSaatiDonus, String varisSaatiDonus) {
        boolean valid = true;

        if (kalkisSaatiGidis.equals("Seçilmedi") || varisSaatiGidis.equals("Seçilmedi") || kalkisSaatiDonus.equals("Seçilmedi") || varisSaatiDonus.equals("Seçilmedi")) {
            tvStartTimeGidis.setError("Zaman aralığı seçiniz!");
            tvArrivalTimeGidis.setError("Zaman aralığı seçiniz!");
            tvStartTimeDonus.setError("Zaman aralığı seçiniz!");
            tvArrivalTimeDonus.setError("Zaman aralığı seçiniz!");
            valid = false;
        }

        if (guzergahAdi.equals("Güzergah Adı")) {
            tvRouteName.setError("Güzergah adı belirleyiniz");
            valid = false;
        } else if (checkTimings(kalkisSaatiDonus, varisSaatiGidis)) {
            tvStartTimeGidis.setError("Gidiş saati dönüş saatinden sonra olamaz!");
            tvArrivalTimeGidis.setError("Gidiş saati dönüş saatinden sonra olamaz!");
            tvStartTimeDonus.setError("Gidiş saati dönüş saatinden sonra olamaz!");
            tvArrivalTimeDonus.setError("Gidiş saati dönüş saatinden sonra olamaz!");
            valid = false;
        } else {
            tvStartTimeGidis.setError(null);
            tvArrivalTimeGidis.setError(null);
            tvStartTimeDonus.setError(null);
            tvArrivalTimeDonus.setError(null);
        }

        for (int i = 0; i < routeList.size(); i++) {

            if (checkTimings(kalkisSaatiGidis, routeList.get(i).gidisGuzergahi.endTime) && checkTimings(routeList.get(i).gidisGuzergahi.starTime, kalkisSaatiGidis)) {
                tvStartTimeGidis.setError("Bu zaman aralığı dolu!");
                valid = false;
            }

            if (checkTimings(kalkisSaatiGidis, routeList.get(i).donusGuzergahi.endTime) && checkTimings(routeList.get(i).donusGuzergahi.starTime, kalkisSaatiGidis)) {
                tvStartTimeGidis.setError("Bu zaman aralığı dolu!");
                valid = false;
            }
            if (checkTimings(kalkisSaatiDonus, routeList.get(i).gidisGuzergahi.endTime) && checkTimings(routeList.get(i).gidisGuzergahi.starTime, kalkisSaatiDonus)) {
                tvStartTimeDonus.setError("Bu zaman aralığı dolu!");
                valid = false;
            }
            if (checkTimings(kalkisSaatiDonus, routeList.get(i).donusGuzergahi.endTime) && checkTimings(routeList.get(i).donusGuzergahi.starTime, kalkisSaatiDonus)) {
                tvStartTimeDonus.setError("Bu zaman aralığı dolu!");
                valid = false;
            }
            if (checkTimings(varisSaatiGidis, routeList.get(i).gidisGuzergahi.endTime) && checkTimings(routeList.get(i).gidisGuzergahi.starTime, varisSaatiGidis)) {
                tvArrivalTimeGidis.setError("Bu zaman aralığı dolu!");
                valid = false;
            }
            if (checkTimings(varisSaatiGidis, routeList.get(i).donusGuzergahi.endTime) && checkTimings(routeList.get(i).donusGuzergahi.starTime, varisSaatiGidis)) {
                tvArrivalTimeGidis.setError("Bu zaman aralığı dolu!");
                valid = false;
            }

            if (checkTimings(varisSaatiDonus, routeList.get(i).gidisGuzergahi.endTime) && checkTimings(routeList.get(i).gidisGuzergahi.starTime, varisSaatiDonus)) {
                tvStartTimeDonus.setError("Bu zaman aralığı dolu!");
                valid = false;
            }

            if (checkTimings(varisSaatiDonus, routeList.get(i).donusGuzergahi.endTime) && checkTimings(routeList.get(i).donusGuzergahi.starTime, varisSaatiDonus)) {
                tvStartTimeDonus.setError("Bu zaman aralığı dolu!");
                valid = false;
            }

        }

        return valid;
    }


}