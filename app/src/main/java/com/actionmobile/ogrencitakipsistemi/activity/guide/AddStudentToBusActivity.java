package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ChildFragment;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;

public class AddStudentToBusActivity extends AppCompatActivity {

    private ImageView imgBack;
    private TextView tvKapasite;
    private Guide guide;
    private Routes route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_to_bus);

        imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(view -> onBackPressed());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
            route = (Routes) bundle.getSerializable(Constance.KEY_ROUTE);
        }

        ChildFragment childFragment = ChildFragment.newInstance("Guide", true, guide, route, null);
        setTransaction(childFragment);
    }

    private void setTransaction(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, fragment);
        fragmentTransaction.commit();
    }
}
