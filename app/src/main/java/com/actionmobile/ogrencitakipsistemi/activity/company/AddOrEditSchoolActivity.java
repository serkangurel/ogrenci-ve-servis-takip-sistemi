package com.actionmobile.ogrencitakipsistemi.activity.company;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.School.OkulBean;
import com.actionmobile.ogrencitakipsistemi.model.company.Schools.OkullarBean;
import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.schibstedspain.leku.LocationPickerActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;

public class AddOrEditSchoolActivity extends AppCompatActivity {

    private OkulBean school;
    private List<String> illerList = new ArrayList<>();
    private List<OkulBean> schoolList = new ArrayList<>();
    private HashMap<String,OkullarBean> schoolHashMap = new HashMap<>();
    private ArrayList<String> itemsIller;
    private ArrayList<String> itemsIlceler;
    private ArrayList<String> itemsOkullar;
    private ArrayAdapter<String> adapterIller;
    private ArrayAdapter<String> adapterIlceler;
    private ArrayAdapter<String> adapterOkullar;
    private String seciliIl = "";
    private String seciliIlce = "";
    private String seciliOkul = "";
    private boolean ilStatus = false;
    private boolean ilceStatus = false;

    private Button btnMap;

    private FirebaseFirestore db;
    private FirebaseUser userCompany;

    private boolean isPermissionsGranted = false;
    private static final int MAP_BUTTON_REQUEST_CODE = 1;
    private static final String TAG = "AddOrEditSchoolActivity";
    private MyLocation location;
    private TextView tvAddress;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_school);

        TextView tvAddOrEditSchool = findViewById(R.id.tv_add_or_edit_school);
        ImageView imgBack = findViewById(R.id.img_back);
        Spinner spinnerIller = findViewById(R.id.spinner1);
        Spinner spinnerIlceler = findViewById(R.id.spinner2);
        Spinner spinnerOkullar = findViewById(R.id.spinner3);
        Button btnSave = findViewById(R.id.btn_save);
        tvAddress = findViewById(R.id.tv_address);
        btnMap = findViewById(R.id.btn_map);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();

        RxPermissions rxPermissions = new RxPermissions(this);
        btnMap.setOnClickListener(v -> getLocationPermissions(rxPermissions));

        init();
        db = FirebaseFirestore.getInstance();

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        schoolList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                OkulBean school = documentSnapshot.toObject(OkulBean.class);
                                if (school != null) {
                                    schoolList.add(school);
                                }
                            }
                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });


        bundle = getIntent().getExtras();
        if (bundle != null) {
            school = (OkulBean) bundle.getSerializable(Constance.KEY_ACTIVITY);
            tvAddOrEditSchool.setText("Okul Düzenle");
            seciliIl = school.il;
            seciliIlce = school.ilce;
            seciliOkul = school.okul;
            location = school.location;
            tvAddress.setText(location.address);
        } else {
            tvAddOrEditSchool.setText("Okul Ekle");
        }

        btnSave.setOnClickListener(view -> {
            String il = spinnerIller.getSelectedItem().toString();
            String ilce = spinnerIlceler.getSelectedItem().toString();
            String okul = spinnerOkullar.getSelectedItem().toString();

            if (!validate(il, ilce, okul)) {
                onSaveFailed();
                return;
            }

            if (school == null) {   //scholl null ise okul ekle
                OkulBean okulBean = new OkulBean();
                okulBean.id = UUID.randomUUID().toString();
                okulBean.il = il;
                okulBean.ilce = ilce;
                okulBean.okul = okul;
                okulBean.location = location;
                schoolList.add(okulBean);

                db.collection("users")
                        .document("company")
                        .collection("companyList")
                        .document(userCompany.getUid())
                        .collection("schoolList")
                        .document(okulBean.id)
                        .set(okulBean)
                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));

            } else {   //scholl null değil ise okulu güncelle
                for (OkulBean okulBean : schoolList) {
                    if (okulBean.id.equals(school.id)) {
                        okulBean.il = il;
                        okulBean.ilce = ilce;
                        okulBean.okul = okul;
                        okulBean.location = location;

                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("schoolList")
                                .document(okulBean.id)
                                .set(okulBean, SetOptions.merge())
                                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));


                        db.collection("users")
                                .document("company")
                                .collection("companyList")
                                .document(userCompany.getUid())
                                .collection("schoolList")
                                .document(okulBean.id)
                                .collection("busList")
                                .get().addOnSuccessListener(queryDocumentSnapshots -> {
                            List<DocumentSnapshot> snapshotList = queryDocumentSnapshots.getDocuments();
                            for (DocumentSnapshot documentSnapshot : snapshotList) {
                                Bus bus = documentSnapshot.toObject(Bus.class);
                                bus.okulAdi = okulBean.okul;
                                db.collection("users")
                                        .document("company")
                                        .collection("companyList")
                                        .document(userCompany.getUid())
                                        .collection("schoolList")
                                        .document(okulBean.id)
                                        .collection("busList")
                                        .document(documentSnapshot.getId())
                                        .set(bus)
                                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                                        .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
                            }
                        });


                    }
                }

            }

            onBackPressed();
        });

        imgBack.setOnClickListener(view -> onBackPressed());

        itemsIller = setSpinnerIller(spinnerIller);
        itemsIlceler = setSpinnerIlceler(spinnerIlceler);
        itemsOkullar = setSpinnerOkullar(spinnerOkullar);

        if (school != null) {
            int posIl = adapterIller.getPosition(school.il);
            spinnerIller.setSelection(posIl);
            int posIlce = adapterIlceler.getPosition(school.ilce);
            spinnerIlceler.setSelection(posIlce);
            int posOkul = adapterOkullar.getPosition(school.okul);
            spinnerOkullar.setSelection(posOkul);
        }

        spinnerIller.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != 0) {
                    seciliIl = itemsIller.get(position);
                    spinnerIlceler.setEnabled(true);
                } else {
                    spinnerIlceler.setEnabled(false);
                    spinnerOkullar.setEnabled(false);
                }
                if (ilStatus) {
                    itemsIlceler = setSpinnerIlceler(spinnerIlceler);
                } else {
                    ilStatus = true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerIlceler.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != 0) {
                    seciliIlce = itemsIlceler.get(position);
                    spinnerOkullar.setEnabled(true);
                } else {
                    spinnerOkullar.setEnabled(false);
                }

                if (ilceStatus) {
                    itemsOkullar = setSpinnerOkullar(spinnerOkullar);
                } else {
                    ilceStatus = true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerOkullar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != 0) {
                    seciliOkul = itemsOkullar.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT", "OK");
            if (requestCode == MAP_BUTTON_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                if (latitude != 0 && longitude != 0 && !address.isEmpty() && !postalcode.isEmpty()) {
                    tvAddress.setText(address);
                    tvAddress.setError(null);

                    location = new MyLocation();
                    location.latitude = latitude;
                    location.longitude = longitude;
                    location.address = address;
                    location.postalcode = postalcode;
                }
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("RESULT", "CANCELLED");
        }
    }


    @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult"})
    private void getLocationPermissions(RxPermissions rxPermissions) {
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    // All requested permissions are granted
                    if (isServicesOK() && granted) {
                        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                                .withLocation(41.4036299, 2.1743558)
                                .withGeolocApiKey("https://www.googleapis.com/geolocation/v1/geolocate?key=" + getString(R.string.google_maps_API_KEY))
                                .shouldReturnOkOnBackPressed()
                                .withGooglePlacesEnabled()
                                .build(AddOrEditSchoolActivity.this);

                        startActivityForResult(locationPickerIntent, MAP_BUTTON_REQUEST_CODE);
                    } else {
                        Toast.makeText(this,
                                getString(R.string.media_picker_some_permission_is_denied), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(AddOrEditSchoolActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
//            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(context, available, ERROR_DIALOG_REQUEST);
//            dialog.show();
        } else {
            Toast.makeText(AddOrEditSchoolActivity.this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private ArrayList<String> setSpinnerIller(Spinner spinnerIller) {
        ArrayList<String> itemsIller = new ArrayList<>();
        itemsIller.add("İl Seçiniz");
        itemsIller.addAll(illerList);
        adapterIller = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsIller);
        spinnerIller.setAdapter(adapterIller);

        return itemsIller;
    }

    private ArrayList<String> setSpinnerIlceler(Spinner spinnerIlceler) {
        ArrayList<String> itemsIlceler = new ArrayList<>();
        itemsIlceler.add("İlçe Seçiniz");
        if (seciliIl.length() > 0) {
            OkullarBean okullarBean = schoolHashMap.get(seciliIl);
            for (OkullarBean.IlceveOkullariBean ilceveOkullariBean : okullarBean.ilceveOkullari) {
                itemsIlceler.add(ilceveOkullariBean.ilce);
            }
        }
        adapterIlceler = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsIlceler);
        spinnerIlceler.setAdapter(adapterIlceler);

        return itemsIlceler;
    }

    private ArrayList<String> setSpinnerOkullar(Spinner spinnerOkullar) {
        ArrayList<String> itemsOkullar = new ArrayList<>();
        itemsOkullar.add("Okul Seçiniz");
        if (seciliIlce.length() > 0) {
            OkullarBean okullarBean = schoolHashMap.get(seciliIl);
            for (OkullarBean.IlceveOkullariBean ilceveOkullariBean : okullarBean.ilceveOkullari) {
                if (ilceveOkullariBean.ilce.equals(seciliIlce)) {
                    itemsOkullar.addAll(ilceveOkullariBean.okullar);
                }
            }
        }

        adapterOkullar = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsOkullar);
        spinnerOkullar.setAdapter(adapterOkullar);

        return itemsOkullar;
    }

    private boolean validate(String il, String ilce, String okul) {
        boolean valid = true;

        if (il.equals("İl Seçiniz") || ilce.equals("İlçe Seçiniz") || okul.equals("Okul Seçiniz")) {
            valid = false;
        }

        if (bundle == null && location == null) {
            tvAddress.setError("Lütfen konum belirleyiniz");
            valid = false;
        } else {
            tvAddress.setError(null);
        }

        return valid;
    }

    private void onSaveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_SHORT).show();
    }

    private void init() {
        String json = Tools.getInstance().loadJSONFromAsset(this, "Okullar.json");
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("okullar");

            Type listType = new TypeToken<List<OkullarBean>>() {
            }.getType();

            List<OkullarBean> schoolList = new Gson().fromJson(jsonArray.toString(), listType);

            for (OkullarBean okullarBean : schoolList) {
                schoolHashMap.put(okullarBean.il, okullarBean);
                illerList.add(okullarBean.il);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
