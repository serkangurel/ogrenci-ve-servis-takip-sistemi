package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.RouteCardsActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.PARENT;

public class ChildInfoActivity extends AppCompatActivity {
    private static final String TAG = "ChildInfoActivity";

    private ConstraintLayout clChildInfo;
    private Child child;
    private ImageView imgBack, imgEditChild, imgDeleteChild;
    private TextView tvTitle, tvChildName, tvAge, tvBirthdate, tvSchoolNo, tvLocation, tvRouteName;
    private CircleImageView imgProfile;
    private Button btnSelectSeat, btnRoute;
    private TextView tvChairNo;

    private FirebaseFirestore db;
    private FirebaseUser userParent;
    private String callPlace;
    private Guide guide;
    private CircularProgressBar circleProgress;
    private Routes route;
    private Bus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_info);

        clChildInfo = findViewById(R.id.cl_childInfo);
        tvTitle = findViewById(R.id.tv_title);
        imgBack = findViewById(R.id.img_back);
        tvChildName = findViewById(R.id.tv_name);
        imgProfile = findViewById(R.id.img_profile);
        imgEditChild = findViewById(R.id.img_edit_child);
        imgDeleteChild = findViewById(R.id.img_delete_child);
        tvAge = findViewById(R.id.tv_phone);
        tvBirthdate = findViewById(R.id.tv_email);
        tvSchoolNo = findViewById(R.id.tv_schoolNo);
        tvLocation = findViewById(R.id.tv_location);
        tvRouteName = findViewById(R.id.tv_route);
        tvChairNo = findViewById(R.id.tv_chair_no);
        btnSelectSeat = findViewById(R.id.btn_SelectSeat);
        btnRoute = findViewById(R.id.btn_route);
        circleProgress = findViewById(R.id.circle_progress);

        db = FirebaseFirestore.getInstance();
        userParent = FirebaseAuth.getInstance().getCurrentUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            child = (Child) bundle.getSerializable(Constance.KEY_ACTIVITY);
            callPlace = bundle.getString(Constance.KEY_CALLPLACE);
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
        }

        tvTitle.setText(child.name);

        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(child.parentId)
                .collection("childs")
                .document(child.id)
                .addSnapshotListener((documentSnapshot, e) -> {
                    child = documentSnapshot.toObject(Child.class);
                    if (route != null) {
                        updateUI();
                    }
                });

        if (guide != null) {
            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(guide.firmaId)
                    .collection("busList")
                    .document(guide.id)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            bus = task.getResult().toObject(Bus.class);
                            if (child.routeId != null) {
                                db.collection("users")
                                        .document("company")
                                        .collection("companyList")
                                        .document(guide.firmaId)
                                        .collection("schoolList")
                                        .document(bus.okulId)
                                        .collection("busList")
                                        .document(guide.id)
                                        .collection("routeList")
                                        .document(child.routeId)
                                        .get()
                                        .addOnCompleteListener(task1 -> {
                                            if (task1.isSuccessful()) {
                                                route = task1.getResult().toObject(Routes.class);
                                                updateUI();
                                            }
                                        });

                            } else {
                                updateUI();
                            }
                        }
                    });
        }

        if (!callPlace.equals("Parent")) {
            imgEditChild.setVisibility(View.GONE);
            imgDeleteChild.setVisibility(View.GONE);
            btnSelectSeat.setVisibility(View.GONE);
            btnRoute.setVisibility(View.GONE);
        }

        String imageUrl = child.imageUrl;
        if (imageUrl != null) {
            Glide.with(ChildInfoActivity.this)
                    .load(imageUrl)
                    .into(imgProfile);
        } else {
            imgProfile.setImageResource(R.drawable.ic_child);
        }

        imgBack.setOnClickListener(view -> onBackPressed());
        imgDeleteChild.setOnClickListener(view -> onDeleteClick());
        imgEditChild.setOnClickListener(view -> onEditClick());
    }

    private void onButtonRouteClick(String chairNo) {
        if (child.routeId != null) {
            if (chairNo != null) {
                Intent intent = new Intent(ChildInfoActivity.this, RouteCardsActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY, route);
                intent.putExtra(Constance.KEY_CALLPLACE, "ChildInfo");
                intent.putExtra(Constance.KEY_CHILD, child);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Lütfen önce koltuk seçiniz!", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, "Henüz rotaya eklenmedi!", Toast.LENGTH_SHORT).show();
        }
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    private void updateUI() {
        closeProgressBar();
        clChildInfo.setVisibility(View.VISIBLE);
        tvTitle.setText(child.name);
        tvChildName.setText(child.name);
        tvAge.setText(child.age);
        tvBirthdate.setText(child.birthDate);
        tvSchoolNo.setText(child.schoolNo);
        tvLocation.setText(child.location.address);
        if (child.chairNo != null) {
            tvChairNo.setText(child.chairNo);
        }
        if (child.routeId != null && route != null) {
            tvRouteName.setText(route.ad);
        }

        btnSelectSeat.setOnClickListener(view -> onSeatSelectClick());
        btnRoute.setOnClickListener(view -> onButtonRouteClick(child.chairNo));
    }

    private void onEditClick() {
        Intent intent = new Intent(ChildInfoActivity.this, AddOrEditChildActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, child);
        startActivity(intent);
    }

    private void onDeleteClick() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Çocuk Sil");
        alert.setMessage("Silmek istediğinize emin misiniz?");
        alert.setPositiveButton("Evet", (dialog, which) -> {
            // continue with delete

            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(userParent.getUid())
                    .collection("childs")
                    .document(child.id)
                    .delete()
                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully deleted!"))
                    .addOnFailureListener(e -> Log.w(TAG, "Error deleting document", e));

            //Delete chairList
            db.collection("users")
                    .document("company")
                    .collection("companyList")
                    .document(guide.firmaId)
                    .collection("busList")
                    .document(guide.id)
                    .collection("chairList")
                    .document(child.id)
                    .delete()
                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully deleted!"))
                    .addOnFailureListener(e -> Log.w(TAG, "Error deleting document", e));

            dialog.dismiss();
            onBackPressed();
        });
        alert.setNegativeButton("Hayır", (dialog, which) -> {
            // close dialog
            dialog.dismiss();
        });
        alert.show();
    }

    private void onSeatSelectClick() {
        if (child.routeId != null) {
            Intent intent = new Intent(getApplicationContext(), ChooseChairActivity.class);
            intent.putExtra(Constance.KEY_CHILDID, child);
            intent.putExtra(Constance.KEY_ACTIVITY, bus.kapasite);
            intent.putExtra("bus", bus);
            intent.putExtra(Constance.KEY_ROUTE, route);
            intent.putExtra(Constance.KEY_GUIDE, guide);
            intent.putExtra(Constance.KEY_CALLPLACE, PARENT.toString());
            startActivity(intent);
        } else {
            Toast.makeText(this, "Koltuk seçilememektedir!", Toast.LENGTH_SHORT).show();
        }
    }

}