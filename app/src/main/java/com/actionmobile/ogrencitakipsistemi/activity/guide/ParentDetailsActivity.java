package com.actionmobile.ogrencitakipsistemi.activity.guide;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ParentInfoFragment;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.tbruyelle.rxpermissions2.RxPermissions;

public class ParentDetailsActivity extends AppCompatActivity {

    private Parents parent;
    private ImageView imgBack, imgCallPhone, imgSendMail;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_details);

        imgBack = findViewById(R.id.img_back);
        imgCallPhone = findViewById(R.id.img_call_phone);
        imgSendMail = findViewById(R.id.img_send_mail);
        tvTitle = findViewById(R.id.tv_parentName);
        RxPermissions rxPermissions = new RxPermissions(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parent = (Parents) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }

        tvTitle.setText(parent.name);
        imgCallPhone.setOnClickListener(view -> getCallPhonePermissions(rxPermissions));
        imgSendMail.setOnClickListener(view -> onSendMailClick());
        imgBack.setOnClickListener(view -> onBackPressed());

        ParentInfoFragment parentInfoFragment = ParentInfoFragment.newInstance(parent, "");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, parentInfoFragment);
        fragmentTransaction.commit();
    }

    private void onSendMailClick() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{parent.email});
        try {
            startActivity(Intent.createChooser(i, "Mail Gönder..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ParentDetailsActivity.this, "Yüklü e-posta istemcisi bulunamadı", Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressLint({"RxLeakedSubscription", "RxSubscribeOnError", "CheckResult", "MissingPermission"})
    private void getCallPhonePermissions(RxPermissions rxPermissions) {
        rxPermissions.request(Manifest.permission.CALL_PHONE)
                .subscribe(granted -> {
                    // All requested permissions are granted
                    if (granted) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setTitle("Numarayı Ara");
                        alert.setMessage(parent.phoneNumber + " numarasını aramak istediğinize emin misiniz?");
                        alert.setPositiveButton("Evet", (dialog, which) -> {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + parent.phoneNumber));
                            startActivity(intent);
                        });
                        alert.setNegativeButton("Hayır", (dialog, which) -> {
                            // close dialog
                            dialog.dismiss();
                        });
                        alert.show();

                    } else {
                        Toast.makeText(this, "Lütfen izinleri onaylayınız!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
