package com.actionmobile.ogrencitakipsistemi.activity.admin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CompanyDetailActivity extends AppCompatActivity {
    private TextView tvTitle,tvFirmaAd, tvYetkiliAd, tvYetkiliTelefon, tvYetkiliMail, tvFirmaAdres;
    ImageView imgBack, imgEdit, imgDelete;
    private Company company;
    private List<Company> companyList = new ArrayList<>();
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);

        tvTitle=findViewById(R.id.tv_title);
        tvFirmaAd = findViewById(R.id.tv_firma_adi);
        tvYetkiliAd = findViewById(R.id.tv_yetkili_adi);
        tvYetkiliTelefon = findViewById(R.id.tv_yetkili_telefon);
        tvYetkiliMail = findViewById(R.id.tv_yetkili_mail);
        tvFirmaAdres = findViewById(R.id.tv_firma_adres);

        imgBack = findViewById(R.id.img_back_company);
        imgEdit = findViewById(R.id.img_edit_company);
        imgDelete = findViewById(R.id.img_delete_company);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("firmalar");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GenericTypeIndicator<List<Company>> t = new GenericTypeIndicator<List<Company>>() {
                };
                companyList = dataSnapshot.getValue(t);
                if(companyList == null) {
                    companyList = new ArrayList<>();
                }
                setTextViews();
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            company = (Company) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        setTextViews();

        imgBack.setOnClickListener(view -> onBackPressed());
        imgEdit.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(),AddOrEditCompanyActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY,company);
                startActivity(intent);
        });
        imgDelete.setOnClickListener(view -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Firma Sil");
            alert.setMessage("Silmek istediğinize emin misiniz?");
            alert.setPositiveButton("Evet", (dialog, which) -> {
                // continue with delete

                for (Company companyItem : companyList) {
                    if (companyItem.id.equals(company.id)) {
                        companyList.remove(companyItem);
                        break;
                    }
                }
                myRef.setValue(companyList);
                dialog.dismiss();
                onBackPressed();
        });
            alert.setNegativeButton("Hayır", (dialog, which) -> {
                // close dialog
                dialog.dismiss();
            });
            alert.show();
    });

    }

    private void setTextViews() {
        for (Company companyItem : companyList) {
            if (companyItem.id.equals(company.id)) {
                company = companyItem;
            }
        }
        if (company != null) {
            tvTitle.setText(company.firmaIsmi);
            tvFirmaAd.setText(company.firmaIsmi);
            tvYetkiliAd.setText(company.yetkili.ad);
            tvYetkiliTelefon.setText(company.yetkili.telefon);
            tvYetkiliMail.setText(company.yetkili.email);
            tvFirmaAdres.setText(company.adres);
        }
    }

    public static class EmailSender {
    }
}
