package com.actionmobile.ogrencitakipsistemi.activity.parent;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.adapter.parent.ChairAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.model.parent.Chairs;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;

public class ChooseChairActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ChooseChairActivity";
    private FirebaseFirestore db;
    private List<Chairs> chairList = new ArrayList<>();
    private List<Parents> parentList = new ArrayList<>();
    private List<Child> childList = new ArrayList<>();
    private ChairAdapter chairAdapter;
    private Chairs chairs;
    private String capacity;
    private Child child;
    private Guide guide;
    private String callPlace, schoolId;
    private FirebaseUser userParent;
    private Routes route;
    private Bus bus;
    private CircularProgressBar circleProgress;
    private View ondortbir;
    private View onyedibir;
    private View onsekizbir;
    private View yirmiyedibir;

    Button oy1, oy2, oy3, oy4, oy5, oy6, oy7, oy8, oy9, oy10, oy11, oy12, oy13, oy14, oy15, oy16, oy17;
    Button od1, od2, od3, od4, od5, od6, od7, od8, od9, od10, od11, od12, od13, od14;
    Button os1, os2, os3, os4, os5, os6, os7, os8, os9, os10, os11, os12, os13, os14, os15, os16, os17, os18;
    Button yy1, yy2, yy3, yy4, yy5, yy6, yy7, yy8, yy9, yy10, yy11, yy12, yy13, yy14, yy15, yy16, yy17, yy18, yy19, yy20, yy21, yy22, yy23, yy24, yy25, yy26, yy27;

    public int koltukNo;
    public int sonTiklanan;
    public Integer dbChairNo;
    TextView tvOdChildName, tvOdChairNo, tvOyChildName, tvOyChairNo, tvOsChildName, tvOsChairNo, tvYyChildName, tvYyChairNo;
    private CircleImageView imgOdChild, imgOyChild, imgOsChild, imgYyChild;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_chair);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            capacity = bundle.getString(Constance.KEY_ACTIVITY);
            child = (Child) bundle.getSerializable(Constance.KEY_CHILDID);
            guide = (Guide) bundle.getSerializable(Constance.KEY_GUIDE);
            callPlace = (String) bundle.getSerializable(Constance.KEY_CALLPLACE);
            schoolId = (String) bundle.getSerializable("schoolId");
            route = (Routes) bundle.getSerializable(Constance.KEY_ROUTE);
            bus = (Bus) bundle.getSerializable("bus");
        }
        chairs = new Chairs();
        userParent = FirebaseAuth.getInstance().getCurrentUser();

        TextView tvChooseChair = findViewById(R.id.tv_choose_chair);
        if (callPlace.equals(GUIDE.toString())) {
            tvChooseChair.setText("Koltukları Görüntüle");
        } else {
            tvChooseChair.setText("Koltuk Seç");
        }
        db = FirebaseFirestore.getInstance();
        if (guide != null) {
            DocumentReference routeRef = FirebaseFirestore.getInstance().document(route.routeRefPath);
            routeRef.collection("routeAddedChildList").get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    QuerySnapshot queryDocumentSnapshots = task.getResult();
                    if (queryDocumentSnapshots != null) {
                        childList.clear();
                        List<DocumentSnapshot> childDocumentList = queryDocumentSnapshots.getDocuments();
                        if (!childDocumentList.isEmpty()) {
                            for (int i = 0; i < childDocumentList.size(); i++) {
                                int index = i;
                                DocumentSnapshot documentSnapshot = childDocumentList.get(i);
                                String childRefPath = (String) documentSnapshot.get("childRefPath");
                                DocumentReference childRef = FirebaseFirestore.getInstance().document(childRefPath);
                                Task<DocumentSnapshot> taskSnap = childRef.get().addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()) {
                                        Child child = task1.getResult().toObject(Child.class);
                                        if (child != null) {
                                            childList.add(child);
                                        }
                                        if (index == childDocumentList.size() - 1) {
                                            updateUI();
                                            DataUpdateChair();
                                        }
                                    }
                                });
                                while (!taskSnap.isComplete()) { //task tamamlanana kadar bekle
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {

                            Toast.makeText(ChooseChairActivity.this, "Rotada öğrenci bulunmamaktadır.", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(ChooseChairActivity.this, "Rota gösterilememektedir.", Toast.LENGTH_SHORT).show();
                }
            });
        }
        circleProgress = findViewById(R.id.circle_progress);
        ImageView imgBack = findViewById(R.id.img_back);
        Button btnSave = findViewById(R.id.btn_save);
        Button btnSave2 = findViewById(R.id.btn_savee);
        Button btnSave3 = findViewById(R.id.btn_saveee);
        Button btnSave4 = findViewById(R.id.btn_saveeee);
        tvOdChildName = findViewById(R.id.tv_od_child_name);
        tvOdChairNo = findViewById(R.id.tv_od_chair_no);
        imgOdChild = findViewById(R.id.img_od_child);
        tvOyChildName = findViewById(R.id.tv_oy_child_name);
        tvOyChairNo = findViewById(R.id.tv_oy_chair_no);
        imgOyChild = findViewById(R.id.img_oy_child);
        tvOsChildName = findViewById(R.id.tv_os_child_name);
        tvOsChairNo = findViewById(R.id.tv_os_chair_no);
        imgOsChild = findViewById(R.id.img_os_child);
        tvYyChildName = findViewById(R.id.tv_yy_child_name);
        tvYyChairNo = findViewById(R.id.tv_yy_chair_no);
        imgYyChild = findViewById(R.id.img_yy_child);
        ondortbir = findViewById(R.id.sv_ondort_bir);
        onyedibir = findViewById(R.id.sv_onyedi_bir);
        onsekizbir = findViewById(R.id.sv_onsekiz_bir);
        yirmiyedibir = findViewById(R.id.sv_yirmiyedi_bir);

        //17+1 koltukları
        oy1 = findViewById(R.id.chair_one);
        oy2 = findViewById(R.id.chair_two);
        oy3 = findViewById(R.id.chair_three);
        oy4 = findViewById(R.id.chair_four);
        oy5 = findViewById(R.id.chair_five);
        oy6 = findViewById(R.id.chair_six);
        oy7 = findViewById(R.id.chair_seven);
        oy8 = findViewById(R.id.chair_eight);
        oy9 = findViewById(R.id.chair_nine);
        oy10 = findViewById(R.id.chair_ten);
        oy11 = findViewById(R.id.chair_eleven);
        oy12 = findViewById(R.id.chair_twelve);
        oy13 = findViewById(R.id.chair_thirteen);
        oy14 = findViewById(R.id.chair_forteen);
        oy15 = findViewById(R.id.chair_fifteen);
        oy16 = findViewById(R.id.chair_sixteen);
        oy17 = findViewById(R.id.chair_seventeen);

        //14+1 koltukları
        od1 = findViewById(R.id.chair_onee);
        od2 = findViewById(R.id.chair_twoo);
        od3 = findViewById(R.id.chair_threee);
        od4 = findViewById(R.id.chair_fourr);
        od5 = findViewById(R.id.chair_fivee);
        od6 = findViewById(R.id.chair_sixx);
        od7 = findViewById(R.id.chair_sevenn);
        od8 = findViewById(R.id.chair_eightt);
        od9 = findViewById(R.id.chair_ninee);
        od10 = findViewById(R.id.chair_tenn);
        od11 = findViewById(R.id.chair_elevenn);
        od12 = findViewById(R.id.chair_twelvee);
        od13 = findViewById(R.id.chair_thirteenn);
        od14 = findViewById(R.id.chair_forteenn);

        //18+1 koltukları
        os1 = findViewById(R.id.chair_oneee);
        os2 = findViewById(R.id.chair_twooo);
        os3 = findViewById(R.id.chair_threeee);
        os4 = findViewById(R.id.chair_fourrr);
        os5 = findViewById(R.id.chair_fiveee);
        os6 = findViewById(R.id.chair_sixxx);
        os7 = findViewById(R.id.chair_sevennn);
        os8 = findViewById(R.id.chair_eighttt);
        os9 = findViewById(R.id.chair_nineee);
        os10 = findViewById(R.id.chair_tennn);
        os11 = findViewById(R.id.chair_elevennnn);
        os12 = findViewById(R.id.chair_twelveeee);
        os13 = findViewById(R.id.chair_thirteennnn);
        os14 = findViewById(R.id.chair_forteennnn);
        os15 = findViewById(R.id.chair_fifteennn);
        os16 = findViewById(R.id.chair_sixteennn);
        os17 = findViewById(R.id.chair_seventeennn);
        os18 = findViewById(R.id.chair_eightteennn);


        //27+1
        yy1 = findViewById(R.id.chair_oneeee);
        yy2 = findViewById(R.id.chair_twoooo);
        yy3 = findViewById(R.id.chair_threeeee);
        yy4 = findViewById(R.id.chair_fourrrr);
        yy5 = findViewById(R.id.chair_fiveeee);
        yy6 = findViewById(R.id.chair_sixxxx);
        yy7 = findViewById(R.id.chair_sevennnn);
        yy8 = findViewById(R.id.chair_eightttt);
        yy9 = findViewById(R.id.chair_nineeee);
        yy10 = findViewById(R.id.chair_tennnn);
        yy11 = findViewById(R.id.chair_elevennnnn);
        yy12 = findViewById(R.id.chair_twelveeeee);
        yy13 = findViewById(R.id.chair_thirteennnnn);
        yy14 = findViewById(R.id.chair_forteennnnn);
        yy15 = findViewById(R.id.chair_fifteennnn);
        yy16 = findViewById(R.id.chair_sixteennnn);
        yy17 = findViewById(R.id.chair_seventeennnn);
        yy18 = findViewById(R.id.chair_eightteennnnn);
        yy19 = findViewById(R.id.chair_nineteen);
        yy20 = findViewById(R.id.chair_twenty);
        yy21 = findViewById(R.id.chair_twentyone);
        yy22 = findViewById(R.id.chair_twentytwo);
        yy23 = findViewById(R.id.chair_twentythree);
        yy24 = findViewById(R.id.chair_twentyfour);
        yy25 = findViewById(R.id.chair_twentytfive);
        yy26 = findViewById(R.id.chair_twentysix);
        yy27 = findViewById(R.id.chair_twentyseven);

        imgBack.setOnClickListener(view -> onBackPressed());


        oy2.setOnClickListener(this);
        oy3.setOnClickListener(this);
        oy4.setOnClickListener(this);
        oy5.setOnClickListener(this);
        oy6.setOnClickListener(this);
        oy7.setOnClickListener(this);
        oy8.setOnClickListener(this);
        oy9.setOnClickListener(this);
        oy10.setOnClickListener(this);
        oy11.setOnClickListener(this);
        oy12.setOnClickListener(this);
        oy13.setOnClickListener(this);
        oy14.setOnClickListener(this);
        oy15.setOnClickListener(this);
        oy16.setOnClickListener(this);
        oy17.setOnClickListener(this);

        od2.setOnClickListener(this);
        od3.setOnClickListener(this);
        od4.setOnClickListener(this);
        od5.setOnClickListener(this);
        od6.setOnClickListener(this);
        od7.setOnClickListener(this);
        od8.setOnClickListener(this);
        od9.setOnClickListener(this);
        od10.setOnClickListener(this);
        od11.setOnClickListener(this);
        od12.setOnClickListener(this);
        od13.setOnClickListener(this);
        od14.setOnClickListener(this);

        os2.setOnClickListener(this);
        os3.setOnClickListener(this);
        os4.setOnClickListener(this);
        os5.setOnClickListener(this);
        os6.setOnClickListener(this);
        os7.setOnClickListener(this);
        os8.setOnClickListener(this);
        os9.setOnClickListener(this);
        os10.setOnClickListener(this);
        os11.setOnClickListener(this);
        os12.setOnClickListener(this);
        os13.setOnClickListener(this);
        os14.setOnClickListener(this);
        os15.setOnClickListener(this);
        os16.setOnClickListener(this);
        os17.setOnClickListener(this);
        os18.setOnClickListener(this);

        yy2.setOnClickListener(this);
        yy3.setOnClickListener(this);
        yy4.setOnClickListener(this);
        yy5.setOnClickListener(this);
        yy6.setOnClickListener(this);
        yy7.setOnClickListener(this);
        yy8.setOnClickListener(this);
        yy9.setOnClickListener(this);
        yy10.setOnClickListener(this);
        yy11.setOnClickListener(this);
        yy12.setOnClickListener(this);
        os13.setOnClickListener(this);
        yy13.setOnClickListener(this);
        yy14.setOnClickListener(this);
        yy15.setOnClickListener(this);
        yy16.setOnClickListener(this);
        yy17.setOnClickListener(this);
        yy18.setOnClickListener(this);
        yy19.setOnClickListener(this);
        yy20.setOnClickListener(this);
        yy21.setOnClickListener(this);
        yy22.setOnClickListener(this);
        yy23.setOnClickListener(this);
        yy24.setOnClickListener(this);
        yy25.setOnClickListener(this);
        yy26.setOnClickListener(this);
        yy27.setOnClickListener(this);

        btnSave.setOnClickListener(view -> AddChairtoFirebase());
        btnSave2.setOnClickListener(view -> AddChairtoFirebase());
        btnSave3.setOnClickListener(view -> AddChairtoFirebase());
        btnSave4.setOnClickListener(view -> AddChairtoFirebase());

        if (callPlace.equals(GUIDE.toString())) {
            btnSave.setVisibility(View.GONE);
            btnSave2.setVisibility(View.GONE);
            btnSave3.setVisibility(View.GONE);
            btnSave4.setVisibility(View.GONE);
        } else {
            tvOdChildName.setVisibility(View.GONE);
            tvOdChairNo.setVisibility(View.GONE);
            imgOdChild.setVisibility(View.GONE);
            tvOyChildName.setVisibility(View.GONE);
            tvOyChairNo.setVisibility(View.GONE);
            imgOyChild.setVisibility(View.GONE);
            tvOsChildName.setVisibility(View.GONE);
            tvOsChairNo.setVisibility(View.GONE);
            imgOsChild.setVisibility(View.GONE);
            tvYyChildName.setVisibility(View.GONE);
            tvYyChairNo.setVisibility(View.GONE);
            imgYyChild.setVisibility(View.GONE);
        }


    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    private void AddChairtoFirebase() {
        if (koltukNo != 0) {
            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(child.parentId)
                    .collection("childs")
                    .document(child.id)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            DocumentSnapshot childSnapShot = task.getResult();
                            Child child = childSnapShot.toObject(Child.class);
                            child.chairNo = String.valueOf(koltukNo);
                            task.getResult().getReference().set(child, SetOptions.merge())
                                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                    .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
                        }
                    });
        } else {
            Toast.makeText(this, "Lütfen Koltuk Seçiniz", Toast.LENGTH_SHORT).show();
        }
        onBackPressed();
    }

    private void updateUI() {
        closeProgressBar();
        switch (capacity) {
            case "14":
                ondortbir.setVisibility(View.VISIBLE);
                break;
            case "17":
                onyedibir.setVisibility(View.VISIBLE);
                break;
            case "18":
                onsekizbir.setVisibility(View.VISIBLE);
                break;
            case "27":
                yirmiyedibir.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void DataUpdateChair() {
        Drawable dr = getResources().getDrawable(R.drawable.button_selected);
        for (Child child1 : childList) {
            if (child1.chairNo != null) {
                if (capacity.equals("14")) {
                    for (int i = 0; i < 14; i++) {
                        if (child1.chairNo.equals("2")) {
                            od2.setBackgroundResource(R.drawable.button_selected);
                            od2.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_twoo;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od2.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("3")) {
                            od3.setBackgroundResource(R.drawable.button_selected);
                            od3.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_threee;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od3.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("4")) {
                            od4.setBackgroundResource(R.drawable.button_selected);
                            od4.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_fourr;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od4.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("5")) {
                            od5.setBackgroundResource(R.drawable.button_selected);
                            od5.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_fivee;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od5.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;

                            }
                        }
                        if (child1.chairNo.equals("6")) {
                            od6.setBackgroundResource(R.drawable.button_selected);
                            od6.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_sixx;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od6.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("7")) {
                            od7.setBackgroundResource(R.drawable.button_selected);
                            od7.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_sevenn;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od7.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("8")) {
                            od8.setBackgroundResource(R.drawable.button_selected);
                            od8.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_eightt;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od8.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("9")) {
                            od9.setBackgroundResource(R.drawable.button_selected);
                            od9.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_ninee;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od9.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("10")) {
                            od10.setBackgroundResource(R.drawable.button_selected);
                            od10.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_tenn;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od10.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("11")) {
                            od11.setBackgroundResource(R.drawable.button_selected);
                            od11.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_elevenn;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od11.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("12")) {
                            od12.setBackgroundResource(R.drawable.button_selected);
                            od12.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_twelvee;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od12.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("13")) {
                            od13.setBackgroundResource(R.drawable.button_selected);
                            od13.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_thirteenn;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od13.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                        if (child1.chairNo.equals("14")) {
                            od14.setBackgroundResource(R.drawable.button_selected);
                            od14.setBackground(dr);
                            if (!callPlace.equals(GUIDE.toString())) { // veli
                                if (child1.id.equals(child.id)) {
                                    sonTiklanan = R.id.chair_forteenn;
                                    onClick(findViewById(sonTiklanan));
                                } else {
                                    od14.setEnabled(false);
                                }

                            } else { // rehber
                                sonTiklanan = 0;
                            }
                        }
                    }

                }

            }

            if (capacity.equals("17")) {
                for (int i = 0; i < 17; i++) {
                    if (child1.chairNo.equals("2")) {
                        oy2.setBackgroundResource(R.drawable.button_selected);
                        oy2.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_two;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy2.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("3")) {
                        oy3.setBackgroundResource(R.drawable.button_selected);
                        oy3.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_three;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy3.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("4")) {
                        oy4.setBackgroundResource(R.drawable.button_selected);
                        oy4.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_four;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy4.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("5")) {
                        oy5.setBackgroundResource(R.drawable.button_selected);
                        oy5.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_five;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy5.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("6")) {
                        oy6.setBackgroundResource(R.drawable.button_selected);
                        oy6.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_six;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy6.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("7")) {
                        oy7.setBackgroundResource(R.drawable.button_selected);
                        oy7.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_seven;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy7.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("8")) {
                        oy8.setBackgroundResource(R.drawable.button_selected);
                        oy8.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eight;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy8.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("9")) {
                        oy9.setBackgroundResource(R.drawable.button_selected);
                        oy9.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_nine;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy9.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("10")) {
                        oy10.setBackgroundResource(R.drawable.button_selected);
                        oy10.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_ten;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy10.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("11")) {
                        oy11.setBackgroundResource(R.drawable.button_selected);
                        oy11.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eleven;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy11.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("12")) {
                        oy12.setBackgroundResource(R.drawable.button_selected);
                        oy12.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twelve;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy12.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("13")) {
                        oy13.setBackgroundResource(R.drawable.button_selected);
                        oy13.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_thirteen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy13.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("14")) {
                        oy14.setBackgroundResource(R.drawable.button_selected);
                        oy14.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_forteen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy14.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("15")) {
                        oy15.setBackgroundResource(R.drawable.button_selected);
                        oy15.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fifteen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy15.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("16")) {
                        oy16.setBackgroundResource(R.drawable.button_selected);
                        oy16.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sixteen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy16.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("17")) {
                        oy17.setBackgroundResource(R.drawable.button_selected);
                        oy17.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_seventeen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                oy17.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                }

            }

            if (capacity.equals("18")) {
                for (int i = 0; i < 18; i++) {
                    if (child1.chairNo.equals("2")) {
                        os2.setBackgroundResource(R.drawable.button_selected);
                        os2.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twooo;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os2.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("3")) {
                        os3.setBackgroundResource(R.drawable.button_selected);
                        os3.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_threeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os3.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("4")) {
                        os4.setBackgroundResource(R.drawable.button_selected);
                        os4.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fourrr;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os4.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("5")) {
                        os5.setBackgroundResource(R.drawable.button_selected);
                        os5.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fiveee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os5.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("6")) {
                        os6.setBackgroundResource(R.drawable.button_selected);
                        os6.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sixxx;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os6.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("7")) {
                        os7.setBackgroundResource(R.drawable.button_selected);
                        os7.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sevennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os7.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("8")) {
                        os8.setBackgroundResource(R.drawable.button_selected);
                        os8.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eighttt;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os8.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("9")) {
                        os9.setBackgroundResource(R.drawable.button_selected);
                        os9.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_nineee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os9.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("10")) {
                        os10.setBackgroundResource(R.drawable.button_selected);
                        os10.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_tennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os10.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("11")) {
                        os11.setBackgroundResource(R.drawable.button_selected);
                        os11.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_elevennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os11.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("12")) {
                        os12.setBackgroundResource(R.drawable.button_selected);
                        os12.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twelveeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os12.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("13")) {
                        os13.setBackgroundResource(R.drawable.button_selected);
                        os13.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_thirteennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os13.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("14")) {
                        os14.setBackgroundResource(R.drawable.button_selected);
                        os14.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_forteennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os14.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("15")) {
                        os15.setBackgroundResource(R.drawable.button_selected);
                        os15.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fifteennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os15.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("16")) {
                        os16.setBackgroundResource(R.drawable.button_selected);
                        os16.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sixteennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os16.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("17")) {
                        os17.setBackgroundResource(R.drawable.button_selected);
                        os17.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_seventeennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os17.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("18")) {
                        os18.setBackgroundResource(R.drawable.button_selected);
                        os18.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eightteennn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                os18.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }

                }

            }

            if (capacity.equals("27")) {
                for (int i = 0; i < 27; i++) {
                    if (child1.chairNo.equals("2")) {
                        yy2.setBackgroundResource(R.drawable.button_selected);
                        yy2.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twoooo;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy2.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("3")) {
                        yy3.setBackgroundResource(R.drawable.button_selected);
                        yy3.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_threeeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy3.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("4")) {
                        yy4.setBackgroundResource(R.drawable.button_selected);
                        yy4.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fourrrr;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy4.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("5")) {
                        yy5.setBackgroundResource(R.drawable.button_selected);
                        yy5.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fiveeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy5.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("6")) {
                        yy6.setBackgroundResource(R.drawable.button_selected);
                        yy6.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sixxxx;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy6.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("7")) {
                        yy7.setBackgroundResource(R.drawable.button_selected);
                        yy7.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sevennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy7.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("8")) {
                        yy8.setBackgroundResource(R.drawable.button_selected);
                        yy8.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eightttt;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy8.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("9")) {
                        yy9.setBackgroundResource(R.drawable.button_selected);
                        yy9.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_nineeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy9.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("10")) {
                        yy10.setBackgroundResource(R.drawable.button_selected);
                        yy10.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_tennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy10.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("11")) {
                        yy11.setBackgroundResource(R.drawable.button_selected);
                        yy11.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_elevennnnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy11.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("12")) {
                        yy12.setBackgroundResource(R.drawable.button_selected);
                        yy12.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twelveeeee;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy12.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("13")) {
                        yy13.setBackgroundResource(R.drawable.button_selected);
                        yy13.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_thirteennnnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy13.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("14")) {
                        yy14.setBackgroundResource(R.drawable.button_selected);
                        yy14.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_forteennnnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy14.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("15")) {
                        yy15.setBackgroundResource(R.drawable.button_selected);
                        yy15.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_fifteennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy15.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("16")) {
                        yy16.setBackgroundResource(R.drawable.button_selected);
                        yy16.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_sixteennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy16.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("17")) {
                        yy17.setBackgroundResource(R.drawable.button_selected);
                        yy17.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_seventeennnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy17.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("18")) {
                        yy18.setBackgroundResource(R.drawable.button_selected);
                        yy18.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_eightteennnnn;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy18.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("19")) {
                        yy19.setBackgroundResource(R.drawable.button_selected);
                        yy19.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_nineteen;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy19.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("20")) {
                        yy20.setBackgroundResource(R.drawable.button_selected);
                        yy20.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twenty;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy20.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("21")) {
                        yy21.setBackgroundResource(R.drawable.button_selected);
                        yy21.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentyone;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy21.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("22")) {
                        yy22.setBackgroundResource(R.drawable.button_selected);
                        yy22.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentythree;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy22.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("23")) {
                        yy23.setBackgroundResource(R.drawable.button_selected);
                        yy23.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentythree;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy23.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("24")) {
                        yy24.setBackgroundResource(R.drawable.button_selected);
                        yy24.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentyfour;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy24.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("25")) {
                        yy25.setBackgroundResource(R.drawable.button_selected);
                        yy25.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentytfive;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy25.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("26")) {
                        yy26.setBackgroundResource(R.drawable.button_selected);
                        yy26.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentysix;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy26.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                    if (child1.chairNo.equals("27")) {
                        yy27.setBackgroundResource(R.drawable.button_selected);
                        yy27.setBackground(dr);
                        if (!callPlace.equals(GUIDE.toString())) { // veli
                            if (child1.id.equals(child.id)) {
                                sonTiklanan = R.id.chair_twentyseven;
                                onClick(findViewById(sonTiklanan));
                            } else {
                                yy27.setEnabled(false);
                            }

                        } else { // rehber
                            sonTiklanan = 0;
                        }
                    }
                }

            }

        }
    }

    private void SetChildName(String dbChilId, String capacity, int chairNum) {
        tvOdChildName.setVisibility(View.VISIBLE);
        tvOdChairNo.setVisibility(View.VISIBLE);
        tvOyChildName.setVisibility(View.VISIBLE);
        tvOyChairNo.setVisibility(View.VISIBLE);
        tvOsChildName.setVisibility(View.VISIBLE);
        tvOsChairNo.setVisibility(View.VISIBLE);
        tvYyChildName.setVisibility(View.VISIBLE);
        tvYyChairNo.setVisibility(View.VISIBLE);
        for (Child child : childList) {
            if (child.id.equals(dbChilId)) {
                String imageUrl = child.imageUrl;
                //kapasite:17
                tvOyChildName.setText("İsim : " + child.name);
                tvOyChairNo.setText("Koltuk No : " + chairNum);
                if (imageUrl != null) {
                    Glide.with(ChooseChairActivity.this)
                            .load(imageUrl)
                            .into(imgOyChild);
                } else {
                    imgOyChild.setImageResource(R.drawable.ic_child);
                }
                //kapasite:14
                tvOdChildName.setText("İsim : " + child.name);
                tvOdChairNo.setText("Koltuk No : " + chairNum);
                if (imageUrl != null) {
                    Glide.with(ChooseChairActivity.this)
                            .load(imageUrl)
                            .into(imgOdChild);
                } else {
                    imgOdChild.setImageResource(R.drawable.ic_child);
                }
                //kapasite:18
                tvOsChildName.setText("İsim : " + child.name);
                tvOsChairNo.setText("Koltuk No : " + chairNum);
                if (imageUrl != null) {
                    Glide.with(ChooseChairActivity.this)
                            .load(imageUrl)
                            .into(imgOsChild);
                } else {
                    imgOsChild.setImageResource(R.drawable.ic_child);
                }
                //kapasite:27
                tvYyChildName.setText("İsim : " + child.name);
                tvYyChairNo.setText("Koltuk No : " + chairNum);
                if (imageUrl != null) {
                    Glide.with(ChooseChairActivity.this)
                            .load(imageUrl)
                            .into(imgYyChild);
                } else {
                    imgYyChild.setImageResource(R.drawable.ic_child);
                }
            }
        }

    }

    @Override
    public void onClick(View v) {

        Drawable dr = getResources().getDrawable(R.drawable.button_pressed);
        Drawable drDefault = getResources().getDrawable(R.drawable.button_default);
        Drawable drSelected = getResources().getDrawable(R.drawable.button_selected);

        switch (v.getId()) {
            //14+1
            case R.id.chair_twoo:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("2")) {
                            od2.setBackgroundResource(R.drawable.button_pressed);
                            od2.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twoo) {
                                od2.setBackgroundResource(R.drawable.button_pressed);
                                od2.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    od2.setBackgroundResource(R.drawable.button_pressed);
                    od2.setBackground(dr);
                    koltukNo = 2;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twoo) {
                        od2.setBackgroundResource(R.drawable.button_pressed);
                        od2.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_threee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("3")) {
                            od3.setBackgroundResource(R.drawable.button_pressed);
                            od3.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threee) {
                                od3.setBackgroundResource(R.drawable.button_pressed);
                                od3.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od3.setBackgroundResource(R.drawable.button_pressed);
                    od3.setBackground(dr);
                    koltukNo = 3;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threee) {
                        od3.setBackgroundResource(R.drawable.button_pressed);
                        od3.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fourr:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("4")) {
                            od4.setBackgroundResource(R.drawable.button_pressed);
                            od4.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourr) {
                                od4.setBackgroundResource(R.drawable.button_pressed);
                                od4.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od4.setBackgroundResource(R.drawable.button_pressed);
                    od4.setBackground(dr);
                    koltukNo = 4;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourr) {
                        od4.setBackgroundResource(R.drawable.button_pressed);
                        od4.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fivee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("5")) {
                            od5.setBackgroundResource(R.drawable.button_pressed);
                            od5.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fivee) {
                                od5.setBackgroundResource(R.drawable.button_pressed);
                                od5.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od5.setBackgroundResource(R.drawable.button_pressed);
                    od5.setBackground(dr);
                    koltukNo = 5;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fivee) {
                        od5.setBackgroundResource(R.drawable.button_pressed);
                        od5.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixx:
                if (callPlace.equals(GUIDE.toString())) {
                for (Child child : childList) {
                    if (child.chairNo.equals("6")) {
                        od6.setBackgroundResource(R.drawable.button_pressed);
                        od6.setBackground(dr);
                        SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                        if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixx) {
                            od6.setBackgroundResource(R.drawable.button_pressed);
                            od6.setBackground(dr);
                            sonTiklanan = v.getId();
                        } else {
                            Button button = findViewById(sonTiklanan);
                            button.setBackgroundResource(R.drawable.button_selected);
                            button.setBackground(drSelected);
                            sonTiklanan = v.getId();
                        }
                    }
                }
            } else {
                    od6.setBackgroundResource(R.drawable.button_pressed);
                    od6.setBackground(dr);
                    koltukNo = 6;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixx) {
                        od6.setBackgroundResource(R.drawable.button_pressed);
                        od6.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sevenn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("7")) {
                            od7.setBackgroundResource(R.drawable.button_pressed);
                            od7.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevenn) {
                                od7.setBackgroundResource(R.drawable.button_pressed);
                                od7.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od7.setBackgroundResource(R.drawable.button_pressed);
                    od7.setBackground(dr);
                    koltukNo = 7;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevenn) {
                        od7.setBackgroundResource(R.drawable.button_pressed);
                        od7.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eightt:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("8")) {
                            od8.setBackgroundResource(R.drawable.button_pressed);
                            od8.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightt) {
                                od8.setBackgroundResource(R.drawable.button_pressed);
                                od8.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od8.setBackgroundResource(R.drawable.button_pressed);
                    od8.setBackground(dr);
                    koltukNo = 8;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightt) {
                        od8.setBackgroundResource(R.drawable.button_pressed);
                        od8.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_ninee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("9")) {
                            od9.setBackgroundResource(R.drawable.button_pressed);
                            od9.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_ninee) {
                                od9.setBackgroundResource(R.drawable.button_pressed);
                                od9.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od9.setBackgroundResource(R.drawable.button_pressed);
                    od9.setBackground(dr);
                    koltukNo = 9;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_ninee) {
                        od9.setBackgroundResource(R.drawable.button_pressed);
                        od9.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_tenn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("10")) {
                            od10.setBackgroundResource(R.drawable.button_pressed);
                            od10.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tenn) {
                                od10.setBackgroundResource(R.drawable.button_pressed);
                                od10.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od10.setBackgroundResource(R.drawable.button_pressed);
                    od10.setBackground(dr);
                    koltukNo = 10;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tenn) {
                        od10.setBackgroundResource(R.drawable.button_pressed);
                        od10.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_elevenn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("11")) {
                            od11.setBackgroundResource(R.drawable.button_pressed);
                            od11.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevenn) {
                                od11.setBackgroundResource(R.drawable.button_pressed);
                                od11.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    od11.setBackgroundResource(R.drawable.button_pressed);
                    od11.setBackground(dr);
                    koltukNo = 11;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevenn) {
                        od11.setBackgroundResource(R.drawable.button_pressed);
                        od11.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twelvee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("12")) {
                            od12.setBackgroundResource(R.drawable.button_pressed);
                            od12.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelvee) {
                                od12.setBackgroundResource(R.drawable.button_pressed);
                                od12.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od12.setBackgroundResource(R.drawable.button_pressed);
                    od12.setBackground(dr);
                    koltukNo = 12;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelvee) {
                        od12.setBackgroundResource(R.drawable.button_pressed);
                        os12.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_thirteenn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("13")) {
                            od13.setBackgroundResource(R.drawable.button_pressed);
                            od13.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteenn) {
                                od13.setBackgroundResource(R.drawable.button_pressed);
                                od13.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od13.setBackgroundResource(R.drawable.button_pressed);
                    od13.setBackground(dr);
                    koltukNo = 13;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteenn) {
                        od13.setBackgroundResource(R.drawable.button_pressed);
                        od13.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_forteenn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("14")) {
                            od14.setBackgroundResource(R.drawable.button_pressed);
                            od14.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteenn) {
                                od14.setBackgroundResource(R.drawable.button_pressed);
                                od14.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    od14.setBackgroundResource(R.drawable.button_pressed);
                    od14.setBackground(dr);
                    koltukNo = 14;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteenn) {
                        od14.setBackgroundResource(R.drawable.button_pressed);
                        od14.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            //17+1

            case R.id.chair_two:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("2")) {
                            oy2.setBackgroundResource(R.drawable.button_pressed);
                            oy2.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_two) {
                                oy2.setBackgroundResource(R.drawable.button_pressed);
                                oy2.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy2.setBackgroundResource(R.drawable.button_pressed);
                    oy2.setBackground(dr);
                    koltukNo = 2;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_two) {
                        oy2.setBackgroundResource(R.drawable.button_pressed);
                        oy2.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                    break;
                }
            case R.id.chair_three:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("3")) {
                            oy3.setBackgroundResource(R.drawable.button_pressed);
                            oy3.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_three) {
                                oy3.setBackgroundResource(R.drawable.button_pressed);
                                oy3.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }else {
                    oy3.setBackgroundResource(R.drawable.button_pressed);
                    oy3.setBackground(dr);
                    koltukNo = 3;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_three) {
                        oy3.setBackgroundResource(R.drawable.button_pressed);
                        oy3.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_four:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("4")) {
                            oy4.setBackgroundResource(R.drawable.button_pressed);
                            oy4.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_four) {
                                oy4.setBackgroundResource(R.drawable.button_pressed);
                                oy4.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy4.setBackgroundResource(R.drawable.button_pressed);
                    oy4.setBackground(dr);
                    koltukNo = 4;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_four) {
                        oy4.setBackgroundResource(R.drawable.button_pressed);
                        oy4.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_five:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("5")) {
                            oy5.setBackgroundResource(R.drawable.button_pressed);
                            oy5.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_five) {
                                oy5.setBackgroundResource(R.drawable.button_pressed);
                                oy5.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy5.setBackgroundResource(R.drawable.button_pressed);
                    oy5.setBackground(dr);
                    koltukNo = 5;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_five) {
                        oy5.setBackgroundResource(R.drawable.button_pressed);
                        oy5.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_six:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("6")) {
                            oy6.setBackgroundResource(R.drawable.button_pressed);
                            oy6.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_six) {
                                oy6.setBackgroundResource(R.drawable.button_pressed);
                                oy6.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy6.setBackgroundResource(R.drawable.button_pressed);
                    oy6.setBackground(dr);
                    koltukNo = 6;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_six) {
                        oy6.setBackgroundResource(R.drawable.button_pressed);
                        oy6.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_seven:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("7")) {
                            oy7.setBackgroundResource(R.drawable.button_pressed);
                            oy7.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seven) {
                                oy7.setBackgroundResource(R.drawable.button_pressed);
                                oy7.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy7.setBackgroundResource(R.drawable.button_pressed);
                    oy7.setBackground(dr);
                    koltukNo = 7;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seven) {
                        oy7.setBackgroundResource(R.drawable.button_pressed);
                        oy7.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eight:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("8")) {
                            oy8.setBackgroundResource(R.drawable.button_pressed);
                            oy8.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eight) {
                                oy8.setBackgroundResource(R.drawable.button_pressed);
                                oy8.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }else {
                    oy8.setBackgroundResource(R.drawable.button_pressed);
                    oy8.setBackground(dr);
                    koltukNo = 8;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eight) {
                        oy8.setBackgroundResource(R.drawable.button_pressed);
                        oy8.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_nine:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("9")) {
                            oy9.setBackgroundResource(R.drawable.button_pressed);
                            oy9.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nine) {
                                oy9.setBackgroundResource(R.drawable.button_pressed);
                                oy9.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }else {
                    oy9.setBackgroundResource(R.drawable.button_pressed);
                    oy9.setBackground(dr);
                    koltukNo = 9;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nine) {
                        oy9.setBackgroundResource(R.drawable.button_pressed);
                        oy9.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_ten:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("10")) {
                            oy10.setBackgroundResource(R.drawable.button_pressed);
                            oy10.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_ten) {
                                oy10.setBackgroundResource(R.drawable.button_pressed);
                                oy10.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy10.setBackgroundResource(R.drawable.button_pressed);
                    oy10.setBackground(dr);
                    koltukNo = 10;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_ten) {
                        oy10.setBackgroundResource(R.drawable.button_pressed);
                        oy10.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eleven:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("11")) {
                            oy11.setBackgroundResource(R.drawable.button_pressed);
                            oy11.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_two) {
                                oy11.setBackgroundResource(R.drawable.button_pressed);
                                oy11.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy11.setBackgroundResource(R.drawable.button_pressed);
                    oy11.setBackground(dr);
                    koltukNo = 11;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eleven) {
                        oy11.setBackgroundResource(R.drawable.button_pressed);
                        oy11.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twelve:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("12")) {
                            oy12.setBackgroundResource(R.drawable.button_pressed);
                            oy12.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelve) {
                                oy12.setBackgroundResource(R.drawable.button_pressed);
                                oy12.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy12.setBackgroundResource(R.drawable.button_pressed);
                    oy12.setBackground(dr);
                    koltukNo = 12;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelve) {
                        oy12.setBackgroundResource(R.drawable.button_pressed);
                        oy12.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_thirteen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("13")) {
                            oy13.setBackgroundResource(R.drawable.button_pressed);
                            oy13.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteen) {
                                oy13.setBackgroundResource(R.drawable.button_pressed);
                                oy13.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy13.setBackgroundResource(R.drawable.button_pressed);
                    oy13.setBackground(dr);
                    koltukNo = 13;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteen) {
                        oy13.setBackgroundResource(R.drawable.button_pressed);
                        oy13.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_forteen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("14")) {
                            oy14.setBackgroundResource(R.drawable.button_pressed);
                            oy14.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteen) {
                                oy14.setBackgroundResource(R.drawable.button_pressed);
                                oy14.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy14.setBackgroundResource(R.drawable.button_pressed);
                    oy14.setBackground(dr);
                    koltukNo = 14;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteen) {
                        oy14.setBackgroundResource(R.drawable.button_pressed);
                        oy14.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fifteen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("15")) {
                            oy15.setBackgroundResource(R.drawable.button_pressed);
                            oy15.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteen) {
                                oy15.setBackgroundResource(R.drawable.button_pressed);
                                oy15.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy15.setBackgroundResource(R.drawable.button_pressed);
                    oy15.setBackground(dr);
                    koltukNo = 15;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteen) {
                        oy15.setBackgroundResource(R.drawable.button_pressed);
                        oy15.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixteen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("16")) {
                            oy16.setBackgroundResource(R.drawable.button_pressed);
                            oy16.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteen) {
                                oy16.setBackgroundResource(R.drawable.button_pressed);
                                oy16.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy16.setBackgroundResource(R.drawable.button_pressed);
                    oy16.setBackground(dr);
                    koltukNo = 16;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteen) {
                        oy16.setBackgroundResource(R.drawable.button_pressed);
                        oy16.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_seventeen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("17")) {
                            oy17.setBackgroundResource(R.drawable.button_pressed);
                            oy17.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeen) {
                                oy17.setBackgroundResource(R.drawable.button_pressed);
                                oy17.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    oy17.setBackgroundResource(R.drawable.button_pressed);
                    oy17.setBackground(dr);
                    koltukNo = 17;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeen) {
                        oy17.setBackgroundResource(R.drawable.button_pressed);
                        oy17.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;


            //18+1

            case R.id.chair_twooo:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("2")) {
                            os2.setBackgroundResource(R.drawable.button_pressed);
                            os2.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twooo) {
                                os2.setBackgroundResource(R.drawable.button_pressed);
                                os2.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os2.setBackgroundResource(R.drawable.button_pressed);
                    os2.setBackground(dr);
                    koltukNo = 2;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twooo) {
                        os2.setBackgroundResource(R.drawable.button_pressed);
                        os2.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_threeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("3")) {
                            os3.setBackgroundResource(R.drawable.button_pressed);
                            os3.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threeee) {
                                os3.setBackgroundResource(R.drawable.button_pressed);
                                os3.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os3.setBackgroundResource(R.drawable.button_pressed);
                    os3.setBackground(dr);
                    koltukNo = 3;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threeee) {
                        os3.setBackgroundResource(R.drawable.button_pressed);
                        os3.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fourrr:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("4")) {
                            os4.setBackgroundResource(R.drawable.button_pressed);
                            os4.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourrr) {
                                os4.setBackgroundResource(R.drawable.button_pressed);
                                os4.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os4.setBackgroundResource(R.drawable.button_pressed);
                    os4.setBackground(dr);
                    koltukNo = 4;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourrr) {
                        os4.setBackgroundResource(R.drawable.button_pressed);
                        os4.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fiveee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("5")) {
                            os5.setBackgroundResource(R.drawable.button_pressed);
                            os5.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fiveee) {
                                os5.setBackgroundResource(R.drawable.button_pressed);
                                os5.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }else {
                    os5.setBackgroundResource(R.drawable.button_pressed);
                    os5.setBackground(dr);
                    koltukNo = 5;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fiveee) {
                        os5.setBackgroundResource(R.drawable.button_pressed);
                        os5.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixxx:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("6")) {
                            os6.setBackgroundResource(R.drawable.button_pressed);
                            os6.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixxx) {
                                os6.setBackgroundResource(R.drawable.button_pressed);
                                os6.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os6.setBackgroundResource(R.drawable.button_pressed);
                    os6.setBackground(dr);
                    koltukNo = 6;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixxx) {
                        os6.setBackgroundResource(R.drawable.button_pressed);
                        os6.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sevennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("7")) {
                            os7.setBackgroundResource(R.drawable.button_pressed);
                            os7.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevennn) {
                                os7.setBackgroundResource(R.drawable.button_pressed);
                                os7.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os7.setBackgroundResource(R.drawable.button_pressed);
                    os7.setBackground(dr);
                    koltukNo = 7;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevennn) {
                        os7.setBackgroundResource(R.drawable.button_pressed);
                        os7.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eighttt:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("8")) {
                            os8.setBackgroundResource(R.drawable.button_pressed);
                            os8.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eighttt) {
                                os8.setBackgroundResource(R.drawable.button_pressed);
                                os8.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os8.setBackgroundResource(R.drawable.button_pressed);
                    os8.setBackground(dr);
                    koltukNo = 8;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eighttt) {
                        os8.setBackgroundResource(R.drawable.button_pressed);
                        os8.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_nineee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("9")) {
                            os9.setBackgroundResource(R.drawable.button_pressed);
                            os9.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineee) {
                                os9.setBackgroundResource(R.drawable.button_pressed);
                                os9.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os9.setBackgroundResource(R.drawable.button_pressed);
                    os9.setBackground(dr);
                    koltukNo = 9;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineee) {
                        os9.setBackgroundResource(R.drawable.button_pressed);
                        os9.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_tennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("10")) {
                            os10.setBackgroundResource(R.drawable.button_pressed);
                            os10.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tennn) {
                                os10.setBackgroundResource(R.drawable.button_pressed);
                                os10.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os10.setBackgroundResource(R.drawable.button_pressed);
                    os10.setBackground(dr);
                    koltukNo = 10;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tennn) {
                        os10.setBackgroundResource(R.drawable.button_pressed);
                        os10.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_elevennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("11")) {
                            os11.setBackgroundResource(R.drawable.button_pressed);
                            os11.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevennnn) {
                                os11.setBackgroundResource(R.drawable.button_pressed);
                                os11.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os11.setBackgroundResource(R.drawable.button_pressed);
                    os11.setBackground(dr);
                    koltukNo = 11;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevennnn) {
                        os11.setBackgroundResource(R.drawable.button_pressed);
                        os11.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twelveeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("12")) {
                            os12.setBackgroundResource(R.drawable.button_pressed);
                            os12.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelveeee) {
                                os12.setBackgroundResource(R.drawable.button_pressed);
                                os12.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os12.setBackgroundResource(R.drawable.button_pressed);
                    os12.setBackground(dr);
                    koltukNo = 12;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelveeee) {
                        os12.setBackgroundResource(R.drawable.button_pressed);
                        os12.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_thirteennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("13")) {
                            os13.setBackgroundResource(R.drawable.button_pressed);
                            os13.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteennnn) {
                                os13.setBackgroundResource(R.drawable.button_pressed);
                                os13.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os13.setBackgroundResource(R.drawable.button_pressed);
                    os13.setBackground(dr);
                    koltukNo = 13;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteennnn) {
                        os13.setBackgroundResource(R.drawable.button_pressed);
                        os13.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_forteennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("14")) {
                            os14.setBackgroundResource(R.drawable.button_pressed);
                            os14.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteennnn) {
                                os14.setBackgroundResource(R.drawable.button_pressed);
                                os14.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os14.setBackgroundResource(R.drawable.button_pressed);
                    os14.setBackground(dr);
                    koltukNo = 14;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteennnn) {
                        os14.setBackgroundResource(R.drawable.button_pressed);
                        os14.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fifteennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("15")) {
                            os15.setBackgroundResource(R.drawable.button_pressed);
                            os15.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteennn) {
                                os15.setBackgroundResource(R.drawable.button_pressed);
                                os15.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os15.setBackgroundResource(R.drawable.button_pressed);
                    os15.setBackground(dr);
                    koltukNo = 15;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteennn) {
                        os15.setBackgroundResource(R.drawable.button_pressed);
                        os15.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixteennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("16")) {
                            os16.setBackgroundResource(R.drawable.button_pressed);
                            os16.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteennn) {
                                os16.setBackgroundResource(R.drawable.button_pressed);
                                os16.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os16.setBackgroundResource(R.drawable.button_pressed);
                    os16.setBackground(dr);
                    koltukNo = 16;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteennn) {
                        os16.setBackgroundResource(R.drawable.button_pressed);
                        os16.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_seventeennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("17")) {
                            os17.setBackgroundResource(R.drawable.button_pressed);
                            os17.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeennn) {
                                os17.setBackgroundResource(R.drawable.button_pressed);
                                os17.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os17.setBackgroundResource(R.drawable.button_pressed);
                    os17.setBackground(dr);
                    koltukNo = 17;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeennn) {
                        os1.setBackgroundResource(R.drawable.button_pressed);
                        os1.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eightteennn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("18")) {
                            os18.setBackgroundResource(R.drawable.button_pressed);
                            os18.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightteennn) {
                                os18.setBackgroundResource(R.drawable.button_pressed);
                                os18.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    os18.setBackgroundResource(R.drawable.button_pressed);
                    os18.setBackground(dr);
                    koltukNo = 18;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightteennn) {
                        os1.setBackgroundResource(R.drawable.button_pressed);
                        os1.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            //27+1

            case R.id.chair_twoooo:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("2")) {
                            yy2.setBackgroundResource(R.drawable.button_pressed);
                            yy2.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twoooo) {
                                yy2.setBackgroundResource(R.drawable.button_pressed);
                                yy2.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    yy2.setBackgroundResource(R.drawable.button_pressed);
                    yy2.setBackground(dr);
                    koltukNo = 2;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twoooo) {
                        yy2.setBackgroundResource(R.drawable.button_pressed);
                        yy2.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_threeeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("3")) {
                            yy3.setBackgroundResource(R.drawable.button_pressed);
                            yy3.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threeeee) {
                                yy3.setBackgroundResource(R.drawable.button_pressed);
                                yy3.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy3.setBackgroundResource(R.drawable.button_pressed);
                    yy3.setBackground(dr);
                    koltukNo = 3;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_threeeee) {
                        yy3.setBackgroundResource(R.drawable.button_pressed);
                        yy3.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fourrrr:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("4")) {
                            yy4.setBackgroundResource(R.drawable.button_pressed);
                            yy4.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourrrr) {
                                yy4.setBackgroundResource(R.drawable.button_pressed);
                                yy4.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy4.setBackgroundResource(R.drawable.button_pressed);
                    yy4.setBackground(dr);
                    koltukNo = 4;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fourrrr) {
                        yy4.setBackgroundResource(R.drawable.button_pressed);
                        yy4.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fiveeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("5")) {
                            yy5.setBackgroundResource(R.drawable.button_pressed);
                            yy5.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fiveeee) {
                                yy5.setBackgroundResource(R.drawable.button_pressed);
                                yy5.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy5.setBackgroundResource(R.drawable.button_pressed);
                    yy5.setBackground(dr);
                    koltukNo = 5;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fiveeee) {
                        yy5.setBackgroundResource(R.drawable.button_pressed);
                        yy5.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixxxx:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("6")) {
                            yy6.setBackgroundResource(R.drawable.button_pressed);
                            yy6.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixxxx) {
                                yy6.setBackgroundResource(R.drawable.button_pressed);
                                yy6.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy6.setBackgroundResource(R.drawable.button_pressed);
                    yy6.setBackground(dr);
                    koltukNo = 6;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixxxx) {
                        yy6.setBackgroundResource(R.drawable.button_pressed);
                        yy6.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sevennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("7")) {
                            yy7.setBackgroundResource(R.drawable.button_pressed);
                            yy7.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevennnn) {
                                yy7.setBackgroundResource(R.drawable.button_pressed);
                                yy7.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy7.setBackgroundResource(R.drawable.button_pressed);
                    yy7.setBackground(dr);
                    koltukNo = 7;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sevennnn) {
                        yy7.setBackgroundResource(R.drawable.button_pressed);
                        yy7.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eightttt:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("8")) {
                            yy8.setBackgroundResource(R.drawable.button_pressed);
                            yy8.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightttt) {
                                yy8.setBackgroundResource(R.drawable.button_pressed);
                                yy8.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy8.setBackgroundResource(R.drawable.button_pressed);
                    yy8.setBackground(dr);
                    koltukNo = 8;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightttt) {
                        yy8.setBackgroundResource(R.drawable.button_pressed);
                        yy8.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_nineeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("9")) {
                            yy9.setBackgroundResource(R.drawable.button_pressed);
                            yy9.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineeee) {
                                yy9.setBackgroundResource(R.drawable.button_pressed);
                                yy9.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy9.setBackgroundResource(R.drawable.button_pressed);
                    yy9.setBackground(dr);
                    koltukNo = 9;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineeee) {
                        yy9.setBackgroundResource(R.drawable.button_pressed);
                        yy9.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_tennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("10")) {
                            yy10.setBackgroundResource(R.drawable.button_pressed);
                            yy10.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tennnn) {
                                yy10.setBackgroundResource(R.drawable.button_pressed);
                                yy10.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy10.setBackgroundResource(R.drawable.button_pressed);
                    yy10.setBackground(dr);
                    koltukNo = 10;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_tennnn) {
                        yy10.setBackgroundResource(R.drawable.button_pressed);
                        yy10.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_elevennnnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("11")) {
                            yy11.setBackgroundResource(R.drawable.button_pressed);
                            yy11.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevennnnn) {
                                yy11.setBackgroundResource(R.drawable.button_pressed);
                                yy11.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy11.setBackgroundResource(R.drawable.button_pressed);
                    yy11.setBackground(dr);
                    koltukNo = 11;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_elevennnnn) {
                        yy11.setBackgroundResource(R.drawable.button_pressed);
                        yy11.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twelveeeee:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("12")) {
                            yy12.setBackgroundResource(R.drawable.button_pressed);
                            yy12.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelveeeee) {
                                yy12.setBackgroundResource(R.drawable.button_pressed);
                                yy12.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy12.setBackgroundResource(R.drawable.button_pressed);
                    yy12.setBackground(dr);
                    koltukNo = 12;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twelveeeee) {
                        yy12.setBackgroundResource(R.drawable.button_pressed);
                        yy12.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_thirteennnnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("13")) {
                            yy13.setBackgroundResource(R.drawable.button_pressed);
                            yy13.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteennnnn) {
                                yy13.setBackgroundResource(R.drawable.button_pressed);
                                yy13.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy13.setBackgroundResource(R.drawable.button_pressed);
                    yy13.setBackground(dr);
                    koltukNo = 13;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_thirteennnnn) {
                        yy13.setBackgroundResource(R.drawable.button_pressed);
                        yy13.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_forteennnnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("2")) {
                            yy14.setBackgroundResource(R.drawable.button_pressed);
                            yy14.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteennnnn) {
                                yy14.setBackgroundResource(R.drawable.button_pressed);
                                yy14.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy14.setBackgroundResource(R.drawable.button_pressed);
                    yy14.setBackground(dr);
                    koltukNo = 14;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_forteennnnn) {
                        yy14.setBackgroundResource(R.drawable.button_pressed);
                        yy14.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_fifteennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("15")) {
                            yy15.setBackgroundResource(R.drawable.button_pressed);
                            yy15.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteennnn) {
                                yy15.setBackgroundResource(R.drawable.button_pressed);
                                yy15.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    yy15.setBackgroundResource(R.drawable.button_pressed);
                    yy15.setBackground(dr);
                    koltukNo = 15;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_fifteennnn) {
                        yy15.setBackgroundResource(R.drawable.button_pressed);
                        yy15.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_sixteennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("16")) {
                            yy16.setBackgroundResource(R.drawable.button_pressed);
                            yy16.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteennnn) {
                                yy16.setBackgroundResource(R.drawable.button_pressed);
                                yy16.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy16.setBackgroundResource(R.drawable.button_pressed);
                    yy16.setBackground(dr);
                    koltukNo = 16;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_sixteennnn) {
                        yy16.setBackgroundResource(R.drawable.button_pressed);
                        yy16.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_seventeennnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("17")) {
                            yy17.setBackgroundResource(R.drawable.button_pressed);
                            yy17.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeennnn) {
                                yy17.setBackgroundResource(R.drawable.button_pressed);
                                yy17.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy17.setBackgroundResource(R.drawable.button_pressed);
                    yy17.setBackground(dr);
                    koltukNo = 17;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_seventeennnn) {
                        yy17.setBackgroundResource(R.drawable.button_pressed);
                        yy17.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_eightteennnnn:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("18")) {
                            yy18.setBackgroundResource(R.drawable.button_pressed);
                            yy18.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightteennnnn) {
                                yy18.setBackgroundResource(R.drawable.button_pressed);
                                yy18.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy18.setBackgroundResource(R.drawable.button_pressed);
                    yy18.setBackground(dr);
                    koltukNo = 18;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_eightteennnnn) {
                        yy18.setBackgroundResource(R.drawable.button_pressed);
                        yy18.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_nineteen:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("19")) {
                            yy19.setBackgroundResource(R.drawable.button_pressed);
                            yy19.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineteen) {
                                yy19.setBackgroundResource(R.drawable.button_pressed);
                                yy19.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy19.setBackgroundResource(R.drawable.button_pressed);
                    yy19.setBackground(dr);
                    koltukNo = 19;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_nineteen) {
                        yy19.setBackgroundResource(R.drawable.button_pressed);
                        yy19.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twenty:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("20")) {
                            yy20.setBackgroundResource(R.drawable.button_pressed);
                            yy20.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twenty) {
                                yy20.setBackgroundResource(R.drawable.button_pressed);
                                yy20.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy20.setBackgroundResource(R.drawable.button_pressed);
                    yy20.setBackground(dr);
                    koltukNo = 20;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twenty) {
                        yy20.setBackgroundResource(R.drawable.button_pressed);
                        yy20.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentyone:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("21")) {
                            yy21.setBackgroundResource(R.drawable.button_pressed);
                            yy21.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyone) {
                                yy21.setBackgroundResource(R.drawable.button_pressed);
                                yy21.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy21.setBackgroundResource(R.drawable.button_pressed);
                    yy21.setBackground(dr);
                    koltukNo = 21;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyone) {
                        yy21.setBackgroundResource(R.drawable.button_pressed);
                        yy21.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentytwo:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("22")) {
                            yy22.setBackgroundResource(R.drawable.button_pressed);
                            yy22.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentytwo) {
                                yy22.setBackgroundResource(R.drawable.button_pressed);
                                yy22.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy22.setBackgroundResource(R.drawable.button_pressed);
                    yy22.setBackground(dr);
                    koltukNo = 22;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentytwo) {
                        yy22.setBackgroundResource(R.drawable.button_pressed);
                        yy22.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentythree:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("23")) {
                            yy23.setBackgroundResource(R.drawable.button_pressed);
                            yy23.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentythree) {
                                yy23.setBackgroundResource(R.drawable.button_pressed);
                                yy23.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy23.setBackgroundResource(R.drawable.button_pressed);
                    yy23.setBackground(dr);
                    koltukNo = 23;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentythree) {
                        yy23.setBackgroundResource(R.drawable.button_pressed);
                        yy23.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentyfour:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("24")) {
                            yy24.setBackgroundResource(R.drawable.button_pressed);
                            yy24.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyfour) {
                                yy24.setBackgroundResource(R.drawable.button_pressed);
                                yy24.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy24.setBackgroundResource(R.drawable.button_pressed);
                    yy24.setBackground(dr);
                    koltukNo = 24;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyfour) {
                        yy24.setBackgroundResource(R.drawable.button_pressed);
                        yy24.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentytfive:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("25")) {
                            yy25.setBackgroundResource(R.drawable.button_pressed);
                            yy25.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentytfive) {
                                yy25.setBackgroundResource(R.drawable.button_pressed);
                                yy25.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy25.setBackgroundResource(R.drawable.button_pressed);
                    yy25.setBackground(dr);
                    koltukNo = 25;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentytfive) {
                        yy25.setBackgroundResource(R.drawable.button_pressed);
                        yy25.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentysix:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("26")) {
                            yy26.setBackgroundResource(R.drawable.button_pressed);
                            yy26.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentysix) {
                                yy26.setBackgroundResource(R.drawable.button_pressed);
                                yy26.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                } else {
                    yy26.setBackgroundResource(R.drawable.button_pressed);
                    yy26.setBackground(dr);
                    koltukNo = 26;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentysix) {
                        yy26.setBackgroundResource(R.drawable.button_pressed);
                        yy26.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;
            case R.id.chair_twentyseven:
                if (callPlace.equals(GUIDE.toString())) {
                    for (Child child : childList) {
                        if (child.chairNo.equals("27")) {
                            yy27.setBackgroundResource(R.drawable.button_pressed);
                            yy27.setBackground(dr);
                            SetChildName(child.id, "", Integer.valueOf(child.chairNo));
                            if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyseven) {
                                yy27.setBackgroundResource(R.drawable.button_pressed);
                                yy27.setBackground(dr);
                                sonTiklanan = v.getId();
                            } else {
                                Button button = findViewById(sonTiklanan);
                                button.setBackgroundResource(R.drawable.button_selected);
                                button.setBackground(drSelected);
                                sonTiklanan = v.getId();
                            }
                        }
                    }
                }  else {
                    yy27.setBackgroundResource(R.drawable.button_pressed);
                    yy27.setBackground(dr);
                    koltukNo = 27;
                    if (sonTiklanan == 0 || sonTiklanan == R.id.chair_twentyseven) {
                        yy27.setBackgroundResource(R.drawable.button_pressed);
                        yy27.setBackground(dr);
                        sonTiklanan = v.getId();
                    } else {
                        Button button = findViewById(sonTiklanan);
                        button.setBackgroundResource(R.drawable.button_default);
                        button.setBackground(drDefault);
                        sonTiklanan = v.getId();
                    }
                }
                break;

            default:
                break;
        }
    }
}

