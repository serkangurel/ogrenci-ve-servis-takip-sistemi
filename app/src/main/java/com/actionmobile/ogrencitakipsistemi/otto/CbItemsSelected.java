package com.actionmobile.ogrencitakipsistemi.otto;

public class CbItemsSelected {
    private boolean isChecked;

    public CbItemsSelected(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public boolean isChecked() {
        return isChecked;
    }

}
