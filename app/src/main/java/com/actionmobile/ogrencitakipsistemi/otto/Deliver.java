package com.actionmobile.ogrencitakipsistemi.otto;

public class Deliver {
    private boolean isDeliverOn;

    public Deliver(boolean isDeliverOn) {
        this.isDeliverOn = isDeliverOn;
    }

    public boolean isDeliverOn() {
        return isDeliverOn;
    }
}
