package com.actionmobile.ogrencitakipsistemi.model.company;

import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.google.firebase.firestore.DocumentReference;

import java.io.Serializable;

public class Bus implements Serializable {
    public String id;
    public String plaka;
    public String kapasite;
    public String okulId;
    public String okulAdi;
    public String soforAdi;
    public Guide rehber;
}