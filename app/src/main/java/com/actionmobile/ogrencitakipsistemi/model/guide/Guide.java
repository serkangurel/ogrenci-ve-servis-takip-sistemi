package com.actionmobile.ogrencitakipsistemi.model.guide;

import java.io.Serializable;

public class Guide implements Serializable {
    public String id;
    public String firmaId;
    public String yetkiKodu;
    public String imageUrl;
    public String ad;
    public String email;
    public String telefon;
}
