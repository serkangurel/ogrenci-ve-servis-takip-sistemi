package com.actionmobile.ogrencitakipsistemi.model.guide;

import java.io.Serializable;

public class Parents implements Serializable {
    public String id;
    public String guideId;
    public String messageToken;
    public String name;
    public String imageUrl;
    public String authorityCode;
    public String phoneNumber;
    public String email;
    public String role;
}