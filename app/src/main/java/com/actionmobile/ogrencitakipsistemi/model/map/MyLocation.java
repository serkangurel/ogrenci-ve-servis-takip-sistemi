package com.actionmobile.ogrencitakipsistemi.model.map;

import java.io.Serializable;

public class MyLocation implements Serializable {
    public double latitude;
    public double longitude;
    public String address;
    public String postalcode;
}
