package com.actionmobile.ogrencitakipsistemi.model.company;

import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;

import java.io.Serializable;

public class Routes implements Serializable {

    public String id;
    public String ad;
    public RouteItem gidisGuzergahi;
    public RouteItem donusGuzergahi;
    public String kapasite;
    public String routeRefPath;

    public static class RouteItem implements Serializable {
        public String starTime;
        public String endTime;
        public MyLocation startPoint;
        public MyLocation destination;
    }
}