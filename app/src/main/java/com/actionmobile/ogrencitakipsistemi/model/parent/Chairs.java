package com.actionmobile.ogrencitakipsistemi.model.parent;

import com.google.firebase.firestore.DocumentReference;

import java.io.Serializable;

public class Chairs {
    public String id;
    public String capacity;
    public Chair chair;

    public static class Chair implements Serializable{
        public int chairNo;
        public String childId;
        public DocumentReference childRef;
        public boolean chairStatus;
    }
}
