package com.actionmobile.ogrencitakipsistemi.model.company;

import java.util.List;

public class Schools {

    public List<OkullarBean> okullar;

    public static class OkullarBean {
        public String il;
        public List<IlceveOkullariBean> ilceveOkullari;

        public static class IlceveOkullariBean {
            public String ilce;
            public List<String> okullar;
        }
    }
}
