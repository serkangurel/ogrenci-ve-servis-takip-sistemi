package com.actionmobile.ogrencitakipsistemi.model.admin;

import java.io.Serializable;

public class Company implements Serializable {

    public String id;
    public String firmaIsmi;
    public String adres;
    public Yetkili yetkili;

    public static class Yetkili implements Serializable {
        public String id;
        public String yetkiKodu;
        public String ad;
        public String email;
        public String telefon;
    }

}