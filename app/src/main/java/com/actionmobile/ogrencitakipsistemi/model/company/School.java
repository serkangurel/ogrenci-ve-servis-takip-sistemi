package com.actionmobile.ogrencitakipsistemi.model.company;

import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;

import java.io.Serializable;
import java.util.List;

public class School {

    public static class OkulBean implements Serializable {
        public String id;
        public String il;
        public String ilce;
        public String okul;
        public MyLocation location;
    }
}