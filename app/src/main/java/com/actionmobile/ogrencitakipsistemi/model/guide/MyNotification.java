package com.actionmobile.ogrencitakipsistemi.model.guide;

import java.io.Serializable;

public class MyNotification implements Serializable {
    public String id;
    public String dateInMilis;
    public String title;
    public String message;
    public String recipentId;
}