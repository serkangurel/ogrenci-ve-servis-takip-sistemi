package com.actionmobile.ogrencitakipsistemi.model.parent;

import com.actionmobile.ogrencitakipsistemi.model.map.MyLocation;

import java.io.Serializable;

public class Child implements Serializable {
    public String id;
    public String parentId;
    public String name;
    public String schoolNo;
    public String imageUrl;
    public String birthDate;
    public String age;
    public String childRefPath;
    public boolean isInBus;
    public String routeId;
    public String chairNo;
    public MyLocation location;
}