package com.actionmobile.ogrencitakipsistemi.fragments.guide;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.adapter.company.RouteAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.company.School;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.orhanobut.hawk.Hawk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class GuideFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "GuideFragment";

    private FirebaseFirestore db;
    private FirebaseUser userGuide;
    private String mParam1;
    private String mParam2;
    private String school_id;
    private Bus bus;

    private TextView tvInstruction;
    private RecyclerView rvRoutes;
    private RouteAdapter adapter;

    private List<Routes> routeList = new ArrayList<>();
    private Guide guide;
    private CircularProgressBar circleProgress;


    public GuideFragment() {
        // Required empty public constructor
    }

    public static GuideFragment newInstance(Guide guide, String param2) {
        GuideFragment fragment = new GuideFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, guide);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            guide = (Guide) getArguments().getSerializable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userGuide = mAuth.getCurrentUser();
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvRoutes = view.findViewById(R.id.rv_routes);
        circleProgress = view.findViewById(R.id.circle_progress);

        db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(guide.firmaId)
                .collection("schoolList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                School.OkulBean okul = documentSnapshot.toObject(School.OkulBean.class);
                                okulTara(okul);
                            }

                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });



    }

    private void okulTara(School.OkulBean okul) {
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(guide.firmaId)
                .collection("schoolList")
                .document(okul.id)
                .collection("busList")
                .document(userGuide.getUid())
                .collection("routeList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        routeList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            school_id = okul.id;
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                Routes rota = documentSnapshot.toObject(Routes.class);
                                routeList.add(rota);
                            }
                            for (int i = 0; i < routeList.size() - 1; i++) {
                                for (int j = 0; j < routeList.size() - i - 1; j++) {
                                    if (!checkTimings(routeList.get(j).donusGuzergahi.endTime, routeList.get(j + 1).donusGuzergahi.endTime)) {
                                        Routes route = routeList.get(j);
                                        routeList.set(j, routeList.get(j + 1));
                                        routeList.set(j + 1, route);
                                    }
                                }
                            }
                            rvRoutes.setLayoutManager(new LinearLayoutManager(getContext()));
                            adapter = new RouteAdapter(getContext(), guide, 1, okul.id, null);
                            rvRoutes.setAdapter(adapter);
                            adapter.setList(routeList);
                            updateUI();
                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });
    }

    public boolean checkTimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    private void updateUI() {
        closeProgressBar();
        if (routeList.size() == 0) {
            rvRoutes.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvRoutes.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }


}