package com.actionmobile.ogrencitakipsistemi.fragments.parent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.adapter.parent.NotificationAdapter;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;


public class notificationFragment extends Fragment {
    private static final String TAG = "notificationFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private CircularProgressBar circleProgress;

    private String mParam1;
    private String mParam2;
    private RecyclerView rvNotif;
    private TextView tvInstruction;
    private NotificationAdapter adapter;
    private List<MyNotification> notifList = new ArrayList<>();

    public notificationFragment() {
        // Required empty public constructor
    }

    public static notificationFragment newInstance(String param1, String param2) {
        notificationFragment fragment = new notificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvNotif = view.findViewById(R.id.rv_notif);
        circleProgress = view.findViewById(R.id.circle_progress);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser parent = mAuth.getCurrentUser();

        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.getUid())
                .collection("notificationList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.e(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        notifList.clear();
                        List<DocumentSnapshot> notificationDocumentList = queryDocumentSnapshots.getDocuments();
                        if (notificationDocumentList != null) {
                            for (DocumentSnapshot documentSnapshot : notificationDocumentList) {
                                MyNotification notif = documentSnapshot.toObject(MyNotification.class);
                                notifList.add(notif);
                            }
                            for (int j = 0; j < notifList.size(); j++) {
                                long enBüyük = 0;
                                MyNotification temp;
                                int k = 0;
                                for (int z = j; z < notifList.size(); z++) {
                                    long tarih = Long.parseLong(notifList.get(z).dateInMilis);
                                    if (tarih > enBüyük) {
                                        enBüyük = tarih;
                                        k = z;
                                    }

                                }
                                temp = notifList.get(j);
                                notifList.set(j, notifList.get(k));
                                notifList.set(k, temp);
                            }
                            if (notifList.size() > 10) {
                                int x = notifList.size();
                                for (int i = x - 11; i >= 0; i--)
                                    notifList.remove(i);
                            }

                            adapter.setList(notifList);
                            updateUI();
                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNotif.setLayoutManager(layoutManager);

        adapter = new NotificationAdapter(getContext());
        rvNotif.setAdapter(adapter);
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    private void updateUI() {
        closeProgressBar();
        if (notifList.size() == 0) {
            rvNotif.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvNotif.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }


}
