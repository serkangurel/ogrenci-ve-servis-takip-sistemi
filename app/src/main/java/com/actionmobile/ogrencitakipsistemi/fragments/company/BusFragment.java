package com.actionmobile.ogrencitakipsistemi.fragments.company;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.AddOrEditBusActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.company.BusAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.School;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import static com.actionmobile.ogrencitakipsistemi.model.company.School.*;


public class BusFragment extends Fragment {
    private static final String TAG = "BusFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser companyUser = FirebaseAuth.getInstance().getCurrentUser();
    private List<Bus> busList = new ArrayList<>();
    private TextView tvInstruction;
    private RecyclerView rvBus;
    private BusAdapter adapter;
    private CircularProgressBar circleProgress;
    private FloatingActionButton fab;

    public BusFragment() {
        // Required empty public constructor
    }

    public static BusFragment newInstance(String param1, String param2) {
        BusFragment fragment = new BusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab = view.findViewById(R.id.fab);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvBus = view.findViewById(R.id.rv_bus);
        circleProgress = view.findViewById(R.id.circle_progress);

        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(companyUser.getUid())
                .collection("busList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.e(TAG, "Listen failed.", e);
                        Toast.makeText(getContext(), "Servislere erişilememektedir", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        busList.clear();
                        List<DocumentSnapshot> busDocumentList = queryDocumentSnapshots.getDocuments();
                        if (busDocumentList != null) {
                            for (DocumentSnapshot documentSnapshot : busDocumentList) {
                                Bus bus = documentSnapshot.toObject(Bus.class);
                                busList.add(bus);
                            }
                            adapter.setList(busList);
                            updateUI();
                        }
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvBus.setLayoutManager(layoutManager);

        adapter = new BusAdapter(getContext(),null);
        rvBus.setAdapter(adapter);

    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(view -> startAddBusActivity());
        if (busList.size() == 0) {
            rvBus.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvBus.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    private void startAddBusActivity() {
        Intent intent = new Intent(getContext(), AddOrEditBusActivity.class);
        String tip = "BusFragment";
        intent.putExtra("tip", tip);
        startActivity(intent);
    }
}
