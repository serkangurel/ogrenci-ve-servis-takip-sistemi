package com.actionmobile.ogrencitakipsistemi.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.actionmobile.ogrencitakipsistemi.otto.BusProvider;

public class BaseFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

}
