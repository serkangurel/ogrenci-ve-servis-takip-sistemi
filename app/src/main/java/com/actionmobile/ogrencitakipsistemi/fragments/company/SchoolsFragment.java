package com.actionmobile.ogrencitakipsistemi.fragments.company;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.AddOrEditSchoolActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.company.SchoolsAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.School.OkulBean;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class SchoolsFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "SchoolsFragment";

    private String mParam1;
    private String mParam2;

    private List<OkulBean> schoolList = new ArrayList<>();
    private TextView tvInstruction;
    private RecyclerView rvSchool;
    private SchoolsAdapter adapter;
    private CircularProgressBar circleProgress;
    private FloatingActionButton fab;

    private FirebaseFirestore db;
    private FirebaseUser userCompany;

    public SchoolsFragment() {
        // Required empty public constructor
    }

    public static SchoolsFragment newInstance(String param1, String param2) {
        SchoolsFragment fragment = new SchoolsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schools, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab = view.findViewById(R.id.fab);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvSchool = view.findViewById(R.id.rv_school);
        circleProgress = view.findViewById(R.id.circle_progress);

        db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        userCompany = mAuth.getCurrentUser();


        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(userCompany.getUid())
                .collection("schoolList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        Toast.makeText(getContext(), "Okullara erişilememektedir", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        schoolList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                OkulBean school = documentSnapshot.toObject(OkulBean.class);
                                if (school != null) {
                                    schoolList.add(school);
                                }
                            }
                        }
                        updateUI();
                        adapter.setList(schoolList);
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSchool.setLayoutManager(layoutManager);

        adapter = new SchoolsAdapter(getContext());
        rvSchool.setAdapter(adapter);

    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(view -> startAddSchoolActivity());
        if (schoolList.size() == 0) {
            rvSchool.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvSchool.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    private void startAddSchoolActivity() {
        Intent intent = new Intent(getContext(), AddOrEditSchoolActivity.class);
        startActivity(intent);
    }
}
