package com.actionmobile.ogrencitakipsistemi.fragments.parent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

public class ParentInfoFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Parents parent;
    private String param2;

    public ParentInfoFragment() {
    }

    public static ParentInfoFragment newInstance(Parents parent, String param2) {
        ParentInfoFragment fragment = new ParentInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, parent);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            parent = (Parents) getArguments().getSerializable(ARG_PARAM1);
            param2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_parent_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CircleImageView imgProfile = view.findViewById(R.id.img_profile);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvPhone = view.findViewById(R.id.tv_phone);
        TextView tvEmail = view.findViewById(R.id.tv_email);

        String imageUrl = parent.imageUrl;
        if (imageUrl != null) {
            Glide.with(getContext())
                    .load(imageUrl)
                    .into(imgProfile);
        } else {
            imgProfile.setImageResource(R.drawable.ic_child);
        }

        tvName.setText(parent.name);
        tvPhone.setText(parent.phoneNumber);
        tvEmail.setText(parent.email);

    }
}
