package com.actionmobile.ogrencitakipsistemi.fragments.parent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;

import de.hdodenhof.circleimageview.CircleImageView;

public class GuideInfoFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private Guide guide;
    private CircleImageView imgProfile;
    private static final String TAG = "GuideInfoFragment";
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;

    public GuideInfoFragment() {
        // Required empty public constructor
    }

    public static GuideInfoFragment newInstance(Guide guide, String param2) {
        GuideInfoFragment fragment = new GuideInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, guide);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            guide = (Guide) getArguments().getSerializable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guide_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgProfile = view.findViewById(R.id.img_profile);
        tvName = view.findViewById(R.id.tv_name);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvEmail = view.findViewById(R.id.tv_email);

        setViews();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("guide")
                .collection("guideList")
                .document(guide.id)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    guide = documentSnapshot.toObject(Guide.class);
                    setViews();
                });

    }

    private void setViews() {
        String imageUrl = guide.imageUrl;
        if (imageUrl != null) {
            Glide.with(getContext())
                    .load(imageUrl)
                    .into(imgProfile);
        } else {
            imgProfile.setImageResource(R.drawable.ic_child);
        }

        tvName.setText(guide.ad);
        tvPhone.setText(guide.telefon);
        tvEmail.setText(guide.email);
    }
}
