package com.actionmobile.ogrencitakipsistemi.fragments.guide;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.guide.AddParentActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.guide.ParentsAdapter;
import com.actionmobile.ogrencitakipsistemi.fragments.BaseFragment;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.otto.CbItemsSelected;
import com.actionmobile.ogrencitakipsistemi.otto.Deliver;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class ParentsFragment extends BaseFragment {
    private static final String TAG = "ParentsFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private boolean isSendMessage;
    private String mParam2;

    private FloatingActionButton fab;
    private ParentsAdapter adapter;
    private TextView tvStatus;
    private RecyclerView rvParents;

    private FirebaseUser guide;
    private List<Parents> parentList = new ArrayList<>();

    private CircularProgressBar circleProgress;

    public ParentsFragment() {
    }

    public static ParentsFragment newInstance(boolean isSendMessage, String param2) {
        ParentsFragment fragment = new ParentsFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, isSendMessage);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isSendMessage = getArguments().getBoolean(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_parents, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvStatus = view.findViewById(R.id.tv_route);
        rvParents = view.findViewById(R.id.rv_parents);
        circleProgress = view.findViewById(R.id.circle_progress);
        fab = view.findViewById(R.id.fab);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        guide = mAuth.getCurrentUser();

        rvParents.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ParentsAdapter(getContext(), isSendMessage);
        rvParents.setAdapter(adapter);

        db.collection("users")
                .document("parent")
                .collection("parentList")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e);
                        return;
                    }
                    String source = queryDocumentSnapshots != null
                            && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                            ? "Local" : "Server";

                    if (queryDocumentSnapshots != null) {
                        parentList.clear();
                        List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                        if (!documentList.isEmpty()) {
                            for (DocumentSnapshot documentSnapshot : documentList) {
                                Parents parentSnapshot = documentSnapshot.toObject(Parents.class);
                                if (parentSnapshot != null && parentSnapshot.guideId.equals(guide.getUid())) {
                                    parentList.add(parentSnapshot);
                                }
                            }
                        }
                        adapter.setList(parentList);
                        updateUI();
                    } else {
                        Log.d(TAG, source + " data: null");
                    }
                });


    }

    private void sendMessage() {
        adapter.sendMessageToParents(getActivity());
    }

    @Subscribe
    public void CbItemsSelected(CbItemsSelected event) {
        adapter.setCBStatus(event.isChecked());
    }

    private void startAddParentActivity() {
        Intent intent = new Intent(getContext(), AddParentActivity.class);
        startActivity(intent);
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        fab.setVisibility(View.VISIBLE);
        if (isSendMessage) {
            fab.setImageResource(R.drawable.ic_menu_send);
            fab.setOnClickListener(view1 -> sendMessage());
        } else {
            fab.setOnClickListener(view1 -> startAddParentActivity());
        }
        if (parentList.size() == 0) {
            rvParents.setVisibility(View.GONE);
            tvStatus.setVisibility(View.VISIBLE);
        } else {
            rvParents.setVisibility(View.VISIBLE);
            tvStatus.setVisibility(View.GONE);
        }
    }
}
