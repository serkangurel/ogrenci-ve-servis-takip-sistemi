package com.actionmobile.ogrencitakipsistemi.fragments.parent;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.parent.AddOrEditChildActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.parent.ChildAdapter;
import com.actionmobile.ogrencitakipsistemi.fragments.BaseFragment;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.otto.BusProvider;
import com.actionmobile.ogrencitakipsistemi.otto.Deliver;
import com.actionmobile.ogrencitakipsistemi.otto.WaitUntilLoading;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public class ChildFragment extends BaseFragment {
    private static final String TAG = "ChildFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";

    private CircularProgressBar circleProgress;
    private String callPlace;
    private boolean isAddStudent;
    private FloatingActionButton fab;

    private TextView tvInstruction;
    private RecyclerView rvChilds;
    private ChildAdapter adapter;
    private String school_id;
    private Guide guide;
    private Routes route;
    private List<Child> childList = new ArrayList<>();
    private boolean isDeliverOn = false;

    public ChildFragment() {
        // Required empty public constructor
    }

    public static ChildFragment newInstance(String callPlace, boolean isAddStudent, Guide guide, Routes route, String school_id) {
        ChildFragment fragment = new ChildFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, callPlace);
        args.putBoolean(ARG_PARAM2, isAddStudent);
        args.putSerializable(ARG_PARAM3, guide);
        args.putSerializable(ARG_PARAM4, route);
        args.putSerializable(ARG_PARAM5, school_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            callPlace = getArguments().getString(ARG_PARAM1);
            isAddStudent = getArguments().getBoolean(ARG_PARAM2);
            guide = (Guide) getArguments().getSerializable(ARG_PARAM3);
            route = (Routes) getArguments().getSerializable(ARG_PARAM4);
            school_id = (String) getArguments().getSerializable(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_child, container, false);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab = view.findViewById(R.id.fab);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvChilds = view.findViewById(R.id.rv_child);
        circleProgress = view.findViewById(R.id.circle_progress);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        rvChilds.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ChildAdapter(getContext(), callPlace, isAddStudent, guide, route);
        rvChilds.setAdapter(adapter);

        switch (callPlace) {
            case "Parent":
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser parent = mAuth.getCurrentUser();

                db.collection("users")
                        .document("parent")
                        .collection("parentList")
                        .document(parent.getUid())
                        .collection("childs")
                        .addSnapshotListener((queryDocumentSnapshots, e) -> {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }
                            String source = queryDocumentSnapshots != null
                                    && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                    ? "Local" : "Server";

                            if (queryDocumentSnapshots != null) {
                                childList.clear();
                                List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                                if (!documentList.isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : documentList) {
                                        childList.add(documentSnapshot.toObject(Child.class));
                                    }
                                }
                                adapter.setList(childList);
                                updateUI();
                            } else {
                                Log.d(TAG, source + " data: null");
                            }
                        });
                break;
            case "Guide":
                FirebaseAuth mAuth1 = FirebaseAuth.getInstance();
                FirebaseUser guideUser = mAuth1.getCurrentUser();
                db.collection("users")
                        .document("parent")
                        .collection("parentList")
                        .addSnapshotListener((queryDocumentSnapshots, e) -> {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }
                            String source = queryDocumentSnapshots != null
                                    && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                    ? "Local" : "Server";

                            if (queryDocumentSnapshots != null) {
                                childList.clear();
                                List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                                if (!documentList.isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : documentList) {  //Tüm velileri dön
                                        Parents parentSnapshot = documentSnapshot.toObject(Parents.class);
                                        //veli servis rehberi tarafından eklenmemiş ise döngüye devam et
                                        if (parentSnapshot == null || !parentSnapshot.guideId.equals(guideUser.getUid())) {
                                            continue;
                                        }
                                        documentSnapshot.getReference().collection("childs").get()
                                                .addOnCompleteListener(task -> {
                                                    if (task.isSuccessful()) {
                                                        for (DocumentSnapshot document : task.getResult()) {
                                                            Child child = document.toObject(Child.class);
                                                            if (route != null) { //rotaya ekle
                                                                if (child.routeId == null || child.routeId.equals(route.id)) {
                                                                    childList.add(child);
                                                                }
                                                            } else { // öğrenci görüntüle
                                                                childList.add(child);
                                                            }

                                                        }
                                                        adapter.setList(childList);
                                                        updateUI();
                                                    } else {
                                                        Log.d(TAG, "Error getting subcollection.", task.getException());
                                                    }
                                                });
                                    }
                                }
                            } else {
                                Log.d(TAG, source + " data: null");
                            }
                        });

                break;
            case "Inspection":
                DocumentReference routeRef = FirebaseFirestore.getInstance().document(route.routeRefPath);
                routeRef.collection("routeAddedChildList")
                        .addSnapshotListener((queryDocumentSnapshots, e) -> {
                            if (e != null) {
                                Log.w(TAG, "Listen failed.", e);
                                return;
                            }

                            String source = queryDocumentSnapshots != null
                                    && queryDocumentSnapshots.getMetadata().hasPendingWrites()
                                    ? "Local" : "Server";

                            if (queryDocumentSnapshots != null) {
                                childList.clear();
                                List<DocumentSnapshot> documentList = queryDocumentSnapshots.getDocuments();
                                if (!documentList.isEmpty()) {
                                    for (DocumentSnapshot documentSnapshot : documentList) {   //Tüm öğrencileri dön
                                        String childRefPath = (String) documentSnapshot.get("childRefPath");
                                        DocumentReference childRef = FirebaseFirestore.getInstance().document(childRefPath);
                                        childRef.get().addOnSuccessListener(documentSnapshot1 -> {
                                            Child child = documentSnapshot1.toObject(Child.class);
                                            if (child != null) {
                                                childList.add(child);
                                            }
                                            adapter.setList(childList);
                                            updateUI();
                                        });
                                    }

                                } else {
                                    updateUI();
                                }
                            } else {
                                Log.d(TAG, source + " data: null");
                            }

                        });
                break;
        }

    }

    @Subscribe
    public void DeliverChilds(Deliver event) {
        isDeliverOn = event.isDeliverOn();
        adapter.deliverChilds(event.isDeliverOn());
    }

    private void updateInspection() {
        if (isDeliverOn) {
            adapter.updateDeliver();
            getActivity().onBackPressed();
            isDeliverOn = false;
        } else {
            if (childList.size() > 0) {
                adapter.updateInspectionDB(getActivity());
                getActivity().onBackPressed();
            }
        }

    }

    private void updateStudentStatus() {
        if (childList.size() > 0) {
            adapter.updateStudentDB();
            getActivity().onBackPressed();
        }
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }


    @SuppressLint("RestrictedApi")
    private void updateUI() {
        closeProgressBar();
        if (childList.size() == 0) {
            rvChilds.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
            if (callPlace.equals("Parent")) {
                tvInstruction.setText("+ butonuna tıklayarak çocuk ekle");
            } else {
                tvInstruction.setText("Öğrenci Bulunamadı");
            }
        } else {
            rvChilds.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }

        if (isAddStudent && !callPlace.equals("Inspection")) {
            fab.setVisibility(View.VISIBLE);
            fab.setImageResource(R.drawable.ic_done_white_24dp);
            fab.setOnClickListener(view1 -> updateStudentStatus());
        } else if (callPlace.equals("Inspection")) {
            fab.setVisibility(View.VISIBLE);
            fab.setImageResource(R.drawable.ic_done_white_24dp);
            fab.setOnClickListener(view1 -> updateInspection());
        } else {
            if (callPlace.equals("Parent")) {
                fab.setVisibility(View.VISIBLE);
                fab.setOnClickListener(view1 -> startAddChildActivity());
            }
        }
        BusProvider.getInstance().post(new WaitUntilLoading());
    }

    private void startAddChildActivity() {
        Intent intent = new Intent(getContext(), AddOrEditChildActivity.class);
        startActivity(intent);
    }

}