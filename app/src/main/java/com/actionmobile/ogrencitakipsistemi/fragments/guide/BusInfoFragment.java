package com.actionmobile.ogrencitakipsistemi.fragments.guide;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.parent.ChooseChairActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;

public class BusInfoFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "BusInfoFragment";

    private String mParam2;

    private FirebaseFirestore db;

    private CircularProgressBar circleProgress;
    private LinearLayout llBusInfo;
    private TextView tvOkul;
    private TextView tvPlaka;
    private TextView tvKapasite;
    private TextView tvSofor;
    private TextView tvRehberAdi;
    private TextView tvRehberMail;
    private TextView tvRehberTel;

    private Guide guide;

    public BusInfoFragment() {
        // Required empty public constructor
    }

    public static BusInfoFragment newInstance(Guide guide, String param2) {
        BusInfoFragment fragment = new BusInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, guide);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            guide = (Guide) getArguments().getSerializable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bus_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llBusInfo = view.findViewById(R.id.ll_busInfo);
        tvOkul = view.findViewById(R.id.tv_okul);
        tvPlaka = view.findViewById(R.id.tv_plaka);
        tvKapasite = view.findViewById(R.id.tv_kapasite);
        tvSofor = view.findViewById(R.id.tv_sofor);
        tvRehberAdi = view.findViewById(R.id.tv_rehberAdi);
        tvRehberMail = view.findViewById(R.id.tv_rehberMail);
        tvRehberTel = view.findViewById(R.id.tv_rehberTel);
        circleProgress = view.findViewById(R.id.circle_progress);

        db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(guide.firmaId)
                .collection("busList")
                .document(guide.id)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot busRef = task.getResult();
                        Bus bus = busRef.toObject(Bus.class);
                        updateUI(bus);
                    } else {
                        Toast.makeText(getContext(), "Servis bilgisine erişilememektedir",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void closeProgressBar() {
        if (circleProgress != null && circleProgress.isShown()) {
            circleProgress.setVisibility(View.GONE);
        }
    }

    private void updateUI(Bus bus) {
        closeProgressBar();
        llBusInfo.setVisibility(View.VISIBLE);

        tvOkul.setText(bus.okulAdi);
        tvPlaka.setText(bus.plaka);
        tvKapasite.setText(bus.kapasite);
        tvSofor.setText(bus.soforAdi);
        tvRehberAdi.setText(bus.rehber.ad);
        tvRehberMail.setText(bus.rehber.email);
        tvRehberTel.setText(bus.rehber.telefon);
    }
}

