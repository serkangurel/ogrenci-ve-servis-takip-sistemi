package com.actionmobile.ogrencitakipsistemi.adapter.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.BusActivity;
import com.actionmobile.ogrencitakipsistemi.activity.company.RoutesActivity;
import com.actionmobile.ogrencitakipsistemi.activity.parent.NotifInformationActivity;
import com.actionmobile.ogrencitakipsistemi.adapter.company.BusAdapter;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<MyNotification> notifList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public NotificationAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }


    public void setList(List<MyNotification> notifList) {
        this.notifList.clear();
        this.notifList.addAll(notifList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_notification, parent, false);
        return new NotificationAdapter.NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        com.actionmobile.ogrencitakipsistemi.adapter.parent.NotificationAdapter.NotificationViewHolder notificationViewHolder = (com.actionmobile.ogrencitakipsistemi.adapter.parent.NotificationAdapter.NotificationViewHolder) holder;
        long milis=Long.valueOf(notifList.get(position).dateInMilis);
        notificationViewHolder.tvBaslik.setText(notifList.get(position).title);

        notificationViewHolder.tvTarih.setText(Tools.getInstance().getDateFromMillisecond(milis));
        notificationViewHolder.tvAciklama.setText(notifList.get(position).message);
        notificationViewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, NotifInformationActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, notifList.get(position));
            context.startActivity(intent);
        });

    }


    @Override
    public int getItemCount() {
        return notifList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        TextView tvBaslik;
        TextView tvAciklama;
        TextView tvTarih;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            tvBaslik = itemView.findViewById(R.id.tv_baslik);
            tvAciklama = itemView.findViewById(R.id.tv_aciklama);
            tvTarih=itemView.findViewById(R.id.tv_tarih);
        }
    }
}