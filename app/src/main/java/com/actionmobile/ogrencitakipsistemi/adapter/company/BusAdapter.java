package com.actionmobile.ogrencitakipsistemi.adapter.company;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.BusActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.util.Constance;

import java.util.ArrayList;
import java.util.List;

public class BusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Bus> busList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private String school_id;

    public BusAdapter(Context context, String school_id) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.school_id = school_id;
    }

    public void setList(List<Bus> busList) {
        this.busList.clear();
        this.busList.addAll(busList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_bus, parent, false);
        return new BusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BusViewHolder busViewHolder = (BusViewHolder) holder;

        busViewHolder.tvRehberAdi.setText(busList.get(position).rehber.ad);
        busViewHolder.tvPlaka.setText(busList.get(position).plaka);
        busViewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, BusActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, busList.get(position));
            intent.putExtra("school", school_id);
            context.startActivity(intent);
        });

    }


    @Override
    public int getItemCount() {
        return busList.size();
    }

    public class BusViewHolder extends RecyclerView.ViewHolder {
        TextView tvPlaka;
        TextView tvRehberAdi;

        public BusViewHolder(View itemView) {
            super(itemView);
            tvPlaka = itemView.findViewById(R.id.tv_plaka);
            tvRehberAdi = itemView.findViewById(R.id.tv_rehber);
        }
    }
}
