package com.actionmobile.ogrencitakipsistemi.adapter.guide;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.guide.ParentDetailsActivity;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ParentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ParentsAdapter";

    private final List<Parents> parentsList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private boolean isSendMessage;
    private RecyclerView mRecyclerView;

    public ParentsAdapter(Context context, boolean isSendMessage) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.isSendMessage = isSendMessage;
    }

    public void setList(List<Parents> parentsList) {
        this.parentsList.clear();
        this.parentsList.addAll(parentsList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mRecyclerView = (RecyclerView) parent;
        View view = inflater.inflate(R.layout.item_parents, parent, false);
        return new ParentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ParentsViewHolder parentHolder = (ParentsViewHolder) holder;
        Parents parent = parentsList.get(position);
        parentHolder.parentName.setText(parent.name);
        parentHolder.phoneNumber.setText(parent.phoneNumber);
        String imageUrl = parent.imageUrl;
        if (imageUrl != null) {
            Glide.with(context)
                    .load(imageUrl)
                    .into(parentHolder.imgParent);
        } else {
            parentHolder.imgParent.setImageResource(R.drawable.ic_child);
        }

        if (isSendMessage) {
            parentHolder.cbStatus.setVisibility(View.VISIBLE);
        } else {
            parentHolder.itemView.setOnClickListener(view -> onItemClick(parent));
        }

    }

    public void sendMessageToParents(FragmentActivity activity) {
        ArrayList<Parents> selectedParentList = new ArrayList<>();
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            ParentsViewHolder holder = (ParentsViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            if (holder.cbStatus.isChecked()) {
                selectedParentList.add(parentsList.get(i));
            }
        }

        if (selectedParentList.size() == 0) {
            Toast.makeText(activity, "En az bir veli seçiniz!", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < selectedParentList.size(); i++) {
            if (i < selectedParentList.size() - 1) {
                stringBuilder.append(selectedParentList.get(i).name + ", ");
            } else {
                stringBuilder.append(selectedParentList.get(i).name);
            }
        }


        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Gönderilecek : " + stringBuilder.toString());

        EditText etTitle = new EditText(context);
        etTitle.setHint("Başlık");
        etTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
        layout.addView(etTitle);

        EditText etMessage = new EditText(context);
        etMessage.setHint("Mesaj");
        etMessage.setSingleLine(false);
        etMessage.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        etMessage.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        etMessage.setMaxLines(4);
        etMessage.setVerticalScrollBarEnabled(true);
        etMessage.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        layout.addView(etMessage);

        builder.setView(layout);

        // Set up the buttons
        builder.setPositiveButton("Gönder", (dialog, which) -> {
            String title = etTitle.getText().toString();
            String message = etMessage.getText().toString();
            if (title.length() < 3 || message.length() < 3) {
                Toast.makeText(context, "Mesaj gönderilemedi", Toast.LENGTH_SHORT).show();
                return;
            }
            sendNotification(selectedParentList, title, message);
            Toast.makeText(context, "Mesaj gönderildi", Toast.LENGTH_SHORT).show();

        });
        builder.setNegativeButton("İptal", (dialog, which) -> dialog.cancel());

        builder.setOnDismissListener(dialogInterface -> {
            activity.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        });

        builder.show();
    }

    private void sendNotification(ArrayList<Parents> selectedParentList, String title, String message) {
        for (Parents parent : selectedParentList) {
            MyNotification notification = new MyNotification();
            notification.id = UUID.randomUUID().toString();
            notification.dateInMilis = String.valueOf(System.currentTimeMillis());
            notification.title = title;
            notification.message = message;
            notification.recipentId = parent.id;
            sendToFirestoreDB(parent, notification);
        }
    }

    private void sendToFirestoreDB(Parents parent, MyNotification notification) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.id)
                .collection("notificationList")
                .document(notification.id)
                .set(notification)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    public void setCBStatus(boolean isChecked) {
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            ParentsViewHolder holder = (ParentsViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            holder.cbStatus.setChecked(isChecked);
        }
    }

    private void onItemClick(Parents parent) {
        Intent intent = new Intent(context, ParentDetailsActivity.class);
        intent.putExtra(Constance.KEY_ACTIVITY, parent);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return parentsList.size();
    }

    private class ParentsViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgParent;
        TextView parentName;
        TextView phoneNumber;
        CheckBox cbStatus;

        ParentsViewHolder(View view) {
            super(view);
            imgParent = view.findViewById(R.id.img_parent);
            parentName = view.findViewById(R.id.tv_parent_name);
            phoneNumber = view.findViewById(R.id.tv_phoneNumber);
            cbStatus = view.findViewById(R.id.cb_status);
        }
    }
}
