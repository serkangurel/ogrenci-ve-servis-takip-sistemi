package com.actionmobile.ogrencitakipsistemi.adapter.admin;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.admin.CompanyDetailActivity;
import com.actionmobile.ogrencitakipsistemi.activity.company.CompanyActivity;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.util.Constance;

import java.util.ArrayList;
import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Company> companyList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public CompanyAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<Company> companyList) {
        this.companyList.clear();
        this.companyList.addAll(companyList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_company, parent, false);
        return new CompanyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CompanyViewHolder viewHolder = (CompanyViewHolder) holder;
        viewHolder.firmaAdi.setText(companyList.get(position).firmaIsmi);
        viewHolder.yetkiliAdi.setText(companyList.get(position).yetkili.ad);
        viewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context , CompanyDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constance.KEY_ACTIVITY,companyList.get(position));
            context.startActivity(intent);

        });



    }

    @Override
    public int getItemCount() {
        return companyList.size();
    }

    private class CompanyViewHolder extends RecyclerView.ViewHolder {
        TextView firmaAdi;
        TextView yetkiliAdi;

        public CompanyViewHolder(View itemView) {
            super(itemView);
            firmaAdi = itemView.findViewById(R.id.tv_sirketAdi);
            yetkiliAdi = itemView.findViewById(R.id.tv_yetkiliAdi);
        }
    }
}
