package com.actionmobile.ogrencitakipsistemi.adapter.parent;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.model.parent.Chairs;

import java.util.ArrayList;
import java.util.List;


public class ChairAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChildAdapter";

    private final List<Chairs> chairsList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public ChairAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<Chairs> childList) {
        this.chairsList.clear();
        this.chairsList.addAll(childList);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    private class ChairsViewHolder extends RecyclerView.ViewHolder {
        Button chairNo;

        ChairsViewHolder(View view) {
            super(view);
            chairNo = view.findViewById(R.id.btn_save);
        }
    }
}

