package com.actionmobile.ogrencitakipsistemi.adapter.company;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.SchoolActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.School.OkulBean;
import com.actionmobile.ogrencitakipsistemi.util.Constance;

import java.util.ArrayList;
import java.util.List;

public class SchoolsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<OkulBean> schoolList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public SchoolsAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<OkulBean> schoolList) {
        this.schoolList.clear();
        this.schoolList.addAll(schoolList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_school, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        SchoolViewHolder schoolViewHolder = (SchoolViewHolder) holder;
            schoolViewHolder.tvOkul.setText(schoolList.get(position).okul);
            schoolViewHolder.tvIlce.setText(schoolList.get(position).ilce);
            schoolViewHolder.tvIl.setText(schoolList.get(position).il);
            schoolViewHolder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, SchoolActivity.class);
                intent.putExtra(Constance.KEY_ACTIVITY, schoolList.get(position));
                context.startActivity(intent);
        });
    }


    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    public class SchoolViewHolder extends RecyclerView.ViewHolder {
        TextView tvIl;
        TextView tvIlce;
        TextView tvOkul;

        public SchoolViewHolder(View itemView) {
            super(itemView);
            tvIl = itemView.findViewById(R.id.tv_il);
            tvIlce = itemView.findViewById(R.id.tv_ilce);
            tvOkul = itemView.findViewById(R.id.tv_okul);
        }
    }
}
