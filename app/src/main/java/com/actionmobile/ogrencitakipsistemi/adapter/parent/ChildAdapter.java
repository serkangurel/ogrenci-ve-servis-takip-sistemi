package com.actionmobile.ogrencitakipsistemi.adapter.parent;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.map.MapActivity;
import com.actionmobile.ogrencitakipsistemi.activity.parent.ChildInfoActivity;
import com.actionmobile.ogrencitakipsistemi.fragments.parent.ChildFragment;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.MyNotification;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.model.parent.Child;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.actionmobile.ogrencitakipsistemi.util.Tools;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChildAdapter";
    private static final int ERROR_DIALOG_REQUEST = 9001;

    private final List<Child> childList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private RecyclerView mRecyclerView;
    private boolean isAddStudent;
    private String callPlace;
    private int count;
    private int limit;

    private FirebaseFirestore db;
    private Guide guide;
    private Routes route;


    public ChildAdapter(Context context, String callPlace, boolean isAddStudent, Guide guide, Routes route) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.isAddStudent = isAddStudent;
        this.callPlace = callPlace;
        this.guide = guide;
        this.route = route;
        db = FirebaseFirestore.getInstance();
    }

    public void setList(List<Child> childList) {
        this.childList.clear();
        count = 0;
        if (isAddStudent && !callPlace.equals("Inspection")) {
            this.childList.add(new Child());
            this.childList.addAll(childList);
            for (int i = 1; i < this.childList.size(); i++) {
                if (this.childList.get(i).routeId != null) {
                    count++;
                }
            }
            limit = Integer.valueOf(route.kapasite);
        } else {
            this.childList.addAll(childList);
        }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mRecyclerView = (RecyclerView) parent;
        if (isAddStudent && !callPlace.equals("Inspection")) {
            if (viewType == 0) {
                View view = mInflater.inflate(R.layout.item_count, parent, false);
                return new CountViewHolder(view);
            } else {
                View view = mInflater.inflate(R.layout.item_child, parent, false);
                return new ChildViewHolder(view);
            }
        } else {
            View view = mInflater.inflate(R.layout.item_child, parent, false);
            return new ChildViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ChildViewHolder) {
            ChildViewHolder childHolder = (ChildViewHolder) holder;
            Child child = childList.get(position);
            childHolder.childName.setText(child.name);
            childHolder.schoolNo.setText(child.schoolNo);
            if (!isAddStudent) {
                childHolder.itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(context, ChildInfoActivity.class);
                    intent.putExtra(Constance.KEY_ACTIVITY, child);
                    intent.putExtra(Constance.KEY_GUIDE, guide);
                    intent.putExtra(Constance.KEY_CALLPLACE, callPlace);
                    context.startActivity(intent);
                });
            }

            String imageUrl = child.imageUrl;
            if (imageUrl != null) {
                Glide.with(context)
                        .load(imageUrl)
                        .into(childHolder.imgProfile);
            } else {
                childHolder.imgProfile.setImageResource(R.drawable.ic_child);
            }

            if (isAddStudent) {
                childHolder.imgStatus.setVisibility(View.GONE);
                childHolder.cbStatus.setVisibility(View.VISIBLE);

                if (!callPlace.equals("Inspection")) {

                    if (child.routeId != null) {
                        childHolder.cbStatus.setChecked(true);
                    }

                    CountViewHolder finalCountHolder = (CountViewHolder) mRecyclerView.findViewHolderForLayoutPosition(0);
                    childHolder.cbStatus.setOnCheckedChangeListener((compoundButton, isChecked) -> {
                        if (isChecked) {
                            if (count == limit) {
                                childHolder.cbStatus.setChecked(false);
                                return;
                            }
                            count++;
                            finalCountHolder.count.setText(String.valueOf(count));
                        } else {
                            count--;
                            finalCountHolder.count.setText(String.valueOf(count));
                        }
                    });
                } else {
                    if (child.isInBus) {
                        childHolder.cbStatus.setChecked(true);
                        childHolder.cbStatus.setEnabled(false);
                    } else {
                        //TODO: Teslim edilen öğrenciler için set enabled false yap
                    }
                }

            } else {
                childHolder.imgStatus.setVisibility(View.VISIBLE);
                childHolder.cbStatus.setVisibility(View.GONE);
                if (child.routeId != null) {
                    childHolder.imgStatus.setImageResource(R.drawable.ic_checked);
                } else {
                    childHolder.imgStatus.setImageResource(R.drawable.ic_error);
                }
            }
        } else {
            CountViewHolder countHolder = (CountViewHolder) holder;
            countHolder.count.setText(String.valueOf(count));
            countHolder.limit.setText(String.valueOf(limit));
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    public void updateStudentDB() {
        Toast.makeText(context, "Öğrenciler güncellendi!", Toast.LENGTH_SHORT).show();
        for (int i = 1; i < mRecyclerView.getChildCount(); i++) {
            Child child = childList.get(i);
            ChildViewHolder holder = (ChildViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            if (holder.cbStatus.isChecked()) {
                child.routeId = route.id;
            } else {
                child.routeId = null;
            }
            updateDB(child);
        }
    }

    public void updateInspectionDB(FragmentActivity activity) {
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            Child child = childList.get(i);
            ChildViewHolder holder = (ChildViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            if (!holder.cbStatus.isEnabled()) {
                continue;
            }
            child.isInBus = holder.cbStatus.isChecked();

            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(child.parentId)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Parents parent = task.getResult().toObject(Parents.class);
                            sendNotification(parent, child, false);
                            task.getResult().getReference()
                                    .collection("childs")
                                    .document(child.id)
                                    .set(child, SetOptions.merge())
                                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                    .addOnFailureListener(error -> Log.w(TAG, "Error writing document", error));
                        }
                    });
        }
        Toast.makeText(activity, "Yoklama Alındı", Toast.LENGTH_SHORT).show();
    }

    private void sendNotification(Parents parent, Child child, boolean isDeliver) {
        MyNotification notification = new MyNotification();
        notification.id = UUID.randomUUID().toString();
        notification.dateInMilis = String.valueOf(System.currentTimeMillis());
        notification.title = "Yoklama";
        if (isDeliver) {
            notification.message = child.name + " isimli çocuğunuz servisten inmiştir";
        } else {
            notification.message = child.name + " isimli çocuğunuz servise binmiştir";
        }
        notification.recipentId = parent.id;
        sendToFirestoreDB(parent, notification);
    }


    private void sendToFirestoreDB(Parents parent, MyNotification notification) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .document("parent")
                .collection("parentList")
                .document(parent.id)
                .collection("notificationList")
                .document(notification.id)
                .set(notification)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));
    }

    private void updateDB(Child child) {
        if (isAddStudent) {
            db.collection("users")
                    .document("parent")
                    .collection("parentList")
                    .document(child.parentId)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            task.getResult().getReference()
                                    .collection("childs")
                                    .document(child.id)
                                    .set(child, SetOptions.merge())
                                    .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                    .addOnFailureListener(error -> Log.w(TAG, "Error writing document", error));

                            addChildtoRouteDB(child);
                        }
                    });
        }

    }

    private void addChildtoRouteDB(Child child) {
        db.collection("users")
                .document("company")
                .collection("companyList")
                .document(guide.firmaId)
                .collection("schoolList")
                .addSnapshotListener((queryDocumentSnapshots1, e1) -> {
                    if (e1 != null) {
                        Log.w(TAG, "Listen failed.", e1);
                        return;
                    }

                    if (queryDocumentSnapshots1 != null) {
                        List<DocumentSnapshot> documentSchoolList = queryDocumentSnapshots1.getDocuments();
                        if (!documentSchoolList.isEmpty()) {
                            for (DocumentSnapshot snapshot : documentSchoolList) {
                                if (child.routeId != null) {
                                    HashMap<String,String> refMap = new HashMap<>();
                                    refMap.put("childRefPath", child.childRefPath);
                                    snapshot.getReference()
                                            .collection("busList")
                                            .document(guide.id)
                                            .collection("routeList")
                                            .document(route.id)
                                            .collection("routeAddedChildList")
                                            .document(child.id)
                                            .set(refMap)
                                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                            .addOnFailureListener(error -> Log.w(TAG, "Error writing document", error));

                                } else {
                                    child.isInBus = false;
                                    child.chairNo = null;
                                    db.collection("users")
                                            .document("parent")
                                            .collection("parentList")
                                            .document(child.parentId)
                                            .get()
                                            .addOnCompleteListener(task -> {
                                                if (task.isSuccessful()) {
                                                    task.getResult().getReference()
                                                            .collection("childs")
                                                            .document(child.id)
                                                            .set(child, SetOptions.merge())
                                                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                                            .addOnFailureListener(error -> Log.w(TAG, "Error writing document", error));
                                                }
                                            });

                                    snapshot.getReference()
                                            .collection("busList")
                                            .document(guide.id)
                                            .collection("routeList")
                                            .document(route.id)
                                            .collection("routeAddedChildList")
                                            .document(child.id)
                                            .delete()
                                            .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully deleted!"))
                                            .addOnFailureListener(error -> Log.w(TAG, "Error deleting document", error));
                                }

                            }
                        }
                    }

                });
    }

    public void deliverChilds(boolean isDeliverOn) {
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            ChildViewHolder holder = (ChildViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            if (isDeliverOn) {
                if (holder.cbStatus.isChecked()) {
                    holder.cbStatus.setEnabled(true);
                } else {
                    holder.cbStatus.setEnabled(false);
                }
            } else {
                if (holder.cbStatus.isChecked()) {
                    holder.cbStatus.setEnabled(false);
                } else {
                    holder.cbStatus.setEnabled(true);
                }
            }
        }
    }

    public void updateDeliver() {
        for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
            Child child = childList.get(i);
            ChildViewHolder holder = (ChildViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
            if (!holder.cbStatus.isEnabled()) {
                continue;
            }
            if (!holder.cbStatus.isChecked()) {
                child.isInBus = false;
                db.collection("users")
                        .document("parent")
                        .collection("parentList")
                        .document(child.parentId)
                        .get()
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Parents parent = task.getResult().toObject(Parents.class);
                                sendNotification(parent, child, true);
                                task.getResult().getReference()
                                        .collection("childs")
                                        .document(child.id)
                                        .set(child, SetOptions.merge())
                                        .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot successfully written!"))
                                        .addOnFailureListener(error -> Log.w(TAG, "Error writing document", error));
                            }
                        });
            }
        }
        Toast.makeText(context, "Öğrenci teslimi yapıldı", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return childList.size();
    }

    private class ChildViewHolder extends RecyclerView.ViewHolder {
        TextView childName;
        TextView schoolNo;
        CircleImageView imgProfile;
        ImageView imgStatus;
        CheckBox cbStatus;

        ChildViewHolder(View view) {
            super(view);
            childName = view.findViewById(R.id.tv_child_name);
            imgProfile = view.findViewById(R.id.img_child);
            schoolNo = view.findViewById(R.id.tv_schoolNo);
            imgStatus = view.findViewById(R.id.img_status);
            cbStatus = view.findViewById(R.id.cb_status);
        }
    }

    private class CountViewHolder extends RecyclerView.ViewHolder {
        TextView count;
        TextView limit;

        CountViewHolder(View view) {
            super(view);
            count = view.findViewById(R.id.tv_count);
            limit = view.findViewById(R.id.tv_limit);
        }
    }
}
