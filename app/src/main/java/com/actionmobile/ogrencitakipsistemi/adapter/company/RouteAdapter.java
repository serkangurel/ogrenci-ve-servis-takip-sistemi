package com.actionmobile.ogrencitakipsistemi.adapter.company;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.ogrencitakipsistemi.R;
import com.actionmobile.ogrencitakipsistemi.activity.company.RouteCardsActivity;
import com.actionmobile.ogrencitakipsistemi.model.company.Bus;
import com.actionmobile.ogrencitakipsistemi.model.company.Routes;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.util.Constance;

import java.util.ArrayList;
import java.util.List;

public class RouteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "RouteAdapter";
    private int guideFragment = 0;
    private final List<Routes> routeList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private Bus bus;
    private String school_id;
    private Guide guide;

    public RouteAdapter(Context context, Guide guide, int guideFragment, String school_id, Bus bus) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.guide = guide;
        this.guideFragment = guideFragment;
        this.school_id = school_id;
        this.bus = bus;
    }

    public void setList(List<Routes> routeList) {
        this.routeList.clear();
        this.routeList.addAll(routeList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_route, parent, false);
        return new RouteViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RouteViewHolder routeHolder = (RouteViewHolder) holder;
        String guzergahAd = routeList.get(position).ad;
        String gidisStart = routeList.get(position).gidisGuzergahi.starTime;
        String donusEnd = routeList.get(position).donusGuzergahi.endTime;
        routeHolder.tvGuzergahAd.setText(guzergahAd);
        routeHolder.tvKalkisSaati.setText(gidisStart);
        routeHolder.tvVarisSaati.setText(donusEnd);
        routeHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, RouteCardsActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, routeList.get(position));
            intent.putExtra("bus", bus);
            if (guideFragment == 1) {
                intent.putExtra("guideFragment", 1);
                intent.putExtra("guide", guide);
            }
            intent.putExtra("school", school_id);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return routeList.size();
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder {
        TextView tvGuzergahAd;
        TextView tvKalkisSaati;
        TextView tvVarisSaati;


        public RouteViewHolder(View itemView) {
            super(itemView);
            tvGuzergahAd = itemView.findViewById(R.id.tv_routeName);
            tvKalkisSaati = itemView.findViewById(R.id.tv_kalkisSaati);
            tvVarisSaati = itemView.findViewById(R.id.tv_varisSaati);
        }
    }
}
