package com.actionmobile.ogrencitakipsistemi.app;

import android.arch.lifecycle.LifecycleObserver;
import android.content.Intent;
import android.support.multidex.MultiDexApplication;

import com.actionmobile.ogrencitakipsistemi.activity.admin.AdminActivity;
import com.actionmobile.ogrencitakipsistemi.activity.company.CompanyActivity;
import com.actionmobile.ogrencitakipsistemi.activity.guide.GuideActivity;
import com.actionmobile.ogrencitakipsistemi.activity.parent.ParentActivity;
import com.actionmobile.ogrencitakipsistemi.model.admin.Company;
import com.actionmobile.ogrencitakipsistemi.model.guide.Guide;
import com.actionmobile.ogrencitakipsistemi.model.guide.Parents;
import com.actionmobile.ogrencitakipsistemi.util.Constance;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.orhanobut.hawk.Hawk;

import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.ADMIN;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.COMPANY;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.GUIDE;
import static com.actionmobile.ogrencitakipsistemi.util.Tools.UserRoles.PARENT;

public class OTSApp extends MultiDexApplication implements LifecycleObserver {

    private static final String TAG = "OTSApp";

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        boolean isLoggedIn = Hawk.get(Constance.HAWK_ISLOGGEDIN, false);
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null && isLoggedIn) {
            startCorrectActivity(currentUser);
        }
        //ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

//    @OnLifecycleEvent(Lifecycle.Event.ON_START)
//    public void onEnterForeground() {
//        Log.d(TAG, "Foreground");
//        boolean isLoggedIn = Hawk.get(Constance.HAWK_ISLOGGEDIN, false);
//        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
//        if (currentUser != null && isLoggedIn) {
//            startCorrectActivity(currentUser);
//        }
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//    public void onEnterBackground() {
//        Log.d(TAG, "Background");
//    }

    private void startCorrectActivity(FirebaseUser user) {
        if (user.getDisplayName().equals(ADMIN.toString())) {
            Intent intent = new Intent(getApplicationContext(), AdminActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (user.getDisplayName().equals(COMPANY.toString())) {
            Company company = Hawk.get(Constance.HAWK_ROLEOBJECT, new Company());
            Intent intent = new Intent(getApplicationContext(), CompanyActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constance.KEY_ACTIVITY, company);
            startActivity(intent);
        } else if (user.getDisplayName().equals(GUIDE.toString())) {
            Guide guide = Hawk.get(Constance.HAWK_ROLEOBJECT, new Guide());
            Intent intent = new Intent(getApplicationContext(), GuideActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constance.KEY_ACTIVITY, guide);
            startActivity(intent);
        } else if (user.getDisplayName().equals(PARENT.toString())) {
            Parents parent = Hawk.get(Constance.HAWK_ROLEOBJECT, new Parents());
            Guide guide = Hawk.get(Constance.HAWK_GUIDEINPARENT, new Guide());
            Intent intent = new Intent(getApplicationContext(), ParentActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constance.KEY_ACTIVITY, parent);
            intent.putExtra(Constance.KEY_GUIDE, guide);
            startActivity(intent);
        }
    }
}
