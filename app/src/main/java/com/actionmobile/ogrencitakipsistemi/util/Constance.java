package com.actionmobile.ogrencitakipsistemi.util;

public class Constance {

    public static final String KEY_ACTIVITY = "activity";

    public static final String KEY_CALLPLACE = "call_place";

    public static final String KEY_REMEMBER = "remember";

    public static final String KEY_CHILD_LIST = "child_list";

    public static final String KEY_LOCATION = "location";

    public static final String HAWK_ROLEOBJECT = "role_object";

    public static final String HAWK_GUIDEINPARENT = "hawk_guideInParent";

    public static final String HAWK_ISLOGGEDIN = "hawk_isLoggedIn";

    public static final String KEY_CHILD = "child";

    public static final String KEY_CHILDID = "childId";

    public static final String KEY_GUIDE = "guide";

    public static final String KEY_ROUTE = "route";


}