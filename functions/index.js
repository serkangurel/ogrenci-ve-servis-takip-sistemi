// [START all]
// [START import]
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
// [END import]
exports.sendNotification = functions.firestore
    .document('/users/parent/parentList/{parentId}/notificationList/{notificationId}')
    .onCreate((snap, context) => {
      // Get an object representing the document
      // e.g. {'name': 'Marie', 'age': 66}
      const messageDoc = snap.data();

      // access a particular field as you would any JS property
	  const id = messageDoc.id;
	  console.log("id: ", id);
      const title = messageDoc.title;
	  console.log("title: ", title);
	  const message = messageDoc.message;
	  console.log("message: ", message);
	  const recipentId = messageDoc.recipentId;
	  console.log("recipentId: ", recipentId);
	  
	  return admin.firestore().doc('/users/parent/parentList/' + recipentId).get().then(parentDoc => {
		  const token = parentDoc.get('messageToken');
		  console.log("token: ", token);
		  
		  const payload = {
				data: {
					data_type: "direct_message",
					title: title,
					message: message,
					message_id: id,
				}
			};
			
			return admin.messaging().sendToDevice(token, payload)
						.then(function(response) {
							console.log("Successfully sent message:", response);
							return null;
						  })
						  .catch(function(error) {
							console.log("Error sending message:", error);
						  });
	    });
	
    });